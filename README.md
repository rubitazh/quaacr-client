# Quaacr #

This is *quaacr* (read as *quaker*) project.

What is *quaacr?* This is Java lib, which allows you do Web UI test automation for a Blink engine based browsers 
(Google Chrome, Chromium, Microsoft Edge).

It uses Chrome Devtools protocol, so, it's a kind of reliable. I mean, at least it provides feedback.

It can be run on Java 11 or higher. Yes, I know, Java 8 would be cool, but sorry, I'm kind of dumb, and you will figure 
out why in a minute.

I'm a QA (now you know), so I wrote this tool as a QA and for QA. What does it mean? It means, that this tool is simple 
and it works. Sarcasm. Well, it works actually, but it's alpha, so, just keep that in mind.

**NB!** Once again, this is **alpha**, so don't use it unless you know what you are doing and you are feelin' like you 
can handle this like a pro. 
To calm you down, I can say, that I've used quaacr on several projects and I was pretty satisfied with the results. 
I really missed any kind of feedback from a bunch of popular web-ui test automation tools, so I tried to fill this gap 
with *quaacr*, and I think it kinda works. But I suppose you should not trust me.

OK, if you are still here, let's go down deep into the murky waters.

### Environment requirements ###

* If you **run** this **locally,** make sure *Google Chrome* (or any other web browser with Blink engine) is installed 
(I know it's obvious, but just in case, dude).

* If you want to **run** web UI tests in **Docker container**, you will need 
[*quaacr-doockr*](https://bitbucket.org/rubitazh/quaacr-doockr) runtime.

### Install ###

* First, clone this repo. Now, if you are **Maven** person, then do this:

* execute this line from quaacr root dir to install jar into your local Maven repo:

    ```
    mvn clean install
    ```
              
* add dependency in your Maven project:

        <dependency>
            <groupId>org.punch.quaacr</groupId>
            <artifactId>quaacr-client</artifactId>
            <version>0.0.65</version>
        </dependency>
        
    and that's it.

* If you are hardcore java compiler fan, just add `quaacr-client-0.0.65.jar` to your project, as lib.

* **No Gradle instructions**  for now, sorry (will do later).

### Start using ###

* Run Google Chrome (or any Blink engine based browser) locally:
```java
    public class GetStartedLocally {
    
        public static void main(String[] args) {
            ChromeBrowserContext cxt = Contexts.local(); //Local context
            Tab tab = cxt.defaultTab();                  //This is empty tab that opens when you start Chrome
            tab.open("https://your-page.org");
            tab.waitPageLoad();                          //Here we wait until page is loaded
            tab.locate()
                    .htmlNode(".span.news-link")         //Locate node by CSS selector (Xpath or just string also works)
                    .click();                            //Click on the node
        }
    }
```

* Run Google Chrome remotely in a Docker container:
```
    ChromeBrowserContext cxt = Contexts.remote()
```
>Do not forget to run [quaacr-doockr](https://bitbucket.org/rubitazh/quaacr-doockr) 
>Docker container before starting remote session.

* Play with config, if necessary:
```java
public class ConfigureBrowser {

    public static void main(String[] args) {
        ContextConfig cfg = new ContextConfig();                    //Browser context config. Mostly this is a bunch
                                                                    // of default timeouts, and probably you shouldn't
                                                                    // touch it until you get familiar with the tool,
                                                                    // but I will show it to you just so you know

        cfg.setAwaitEventTimeout(30_000L);                          //Time quaacr waits for a DevTools event to be
                                                                    // fired on any web-browser action. Default is
                                                                    // 30 seconds
        
        cfg.setAwaitTargetPageTimeout(5_000L);                      //Time quaacr waits for a web browser page (tab)
                                                                    // to be opened on start. Default is 5 seconds
        
        cfg.setConsumerQueueLimit(1024);                            //Sets a number of the latest DevTools events
                                                                    // quaacr currently listens to. Default is 1024
        
        cfg.setLocateNodePollTimeout(100L);                         //Time quaacr waits before executing a next HTML
                                                                    // node search attempt within a given time frame.
                                                                    // Default is 100 milliseconds

        cfg.setLocateNodeRitaSaidTenSecondsIsGoodTimeout(10_000L);  //Time quaacr waits for the element to be found via
                                                                    // a search query (XPath or CSS Locator). Default
                                                                    // is 10 seconds. In conjunction with the setter
                                                                    // above, quaacr will search for an element (let's
                                                                    // say, with the query "//a[@class='hey']") for
                                                                    // 10 seconds (this field), re-attempting the search
                                                                    // each 100 milliseconds (the field above)
        
        cfg.setWsResponseTimeout(30_000L);                          //Time quaacr waits for a WebSocket response to be
                                                                    // received from a browser. Default is 30 seconds

        RemoteContextBuilder builder = new RemoteContextBuilder();  //Create context builder
        builder.setContextConfig(cfg);                              //Sets context config
        //TODO: other builder setters
        
        ChromeBrowserContext cxt = Contexts.remote(builder);        //Pass context builder to context
        Tab tab = cxt.defaultTab();                                 //Pass control to the browser's initial tab
    }
}
```

* Pass browser options:
```java
public class Headless {

    public static void main(String[] args) {
        RemoteContextBuilder builder = new RemoteContextBuilder();  //Create context builder
        
        builder.setBrowserOpts(List.of(
                "--headless",                                       //Headless mode flag
                "--ignore-certificate-errors",                      //Ignore certs errors
                "--ignore-urlfetcher-cert-requests"));
    }
}
```

* The browser tab methods:
```java
public class WorkingWithBrowserTab {

    public static void main(String[] args) {
        Tab tab = Contexts.local().defaultTab();

        tab.bringToFront();                                  //Makes the tab active for a user

        tab.executeJSCode("console.log('Hi from quaacr.')"); //Executes JS code. Same as you would do it via
                                                             // a web browser console

        tab.getUrl();                                        //Returns page url
         
        Locate locate = tab.locate();                        //Returns Locate service, which searches for HTML node(s).
                                                             // If a node isn't found within given time (default is 
                                                             // 10 seconds), then an exception will be thrown
        
        Locate locate = tab.locateNoAwait();                 //Same as the method above, but search happens only once
                                                             // without any await time, and if nothing is found, then 
                                                             // null is returned (no exceptions are thrown)

        tab.printToPDF(Paths.get("/path/to/pdf"));           //Prints page content into PDF file

        tab.readFromClipboard();                             //Reads from the desktop clipboard anything copied
                                                             //during browser session
       
        tab.reload();                                        //Reloads the page

        tab.setBypassCSP(true);                              //Bypasses page Content Security Policy, if true

        tab.setDocumentContent("<div><a>quaacr</a></div>");  //Sets document for the tab

        tab.waitPageLoad();                                  //Waits until page is loaded. See javadoc for more info

        tab.waitPageLoadNetworkIdle();                       //Waits until page is loaded and all networks requests
                                                             // are completed. See javadoc for more info
        
        //... and some more. See the API and javadocs.
    }
}
```

* Various HTML node interactions:
```java
public class HtmlNodeInteractions {

    public static void main(String[] args) {
            BrowserContext cxt = Contexts.remote();
            Tab tab = cxt.defaultTab();
            tab.open("https://address");
            tab.waitPageLoad();
           
            //--- Inputs ---//
           
            tab.locate()
                    .input("xpath_or_selector")      //Searches for nodes which can accept text input (<input>, <textArea>
                    .focus()                         //Switch focus on input
                    .clear()                         //Clears input from any text
                    .populate("text")                //Populates into input symbol by symbol (like typing)
                    .insert("text")                  //Fills input with a single operation (like paste)
                    .hitEnter();                     //Passes pressed Enter button signal to input
        
            //--- Radio ---//
        
            InputNode.Radio radioButton = tab.locate()
                    .input("xpath_or_selector")
                    .radioButton();                  //Radio button
    
            radioButton.isChecked();                 //Whether radio is selected
            radioButton.check();                     //Checks radio
        
            //--- Select --//
        
            SelectNode selectNode = tab.locate()
                    .select("xpath_or_selector");    //Searches for <select> element
    
            selectNode.select("third_option");       //Selects given option (can be several options, like multi-select)
            selectNode.getValue();                   //Returns selected value, if exists
        
            //--- HTML node ---//
        
            HtmlNode htmlNode = tab.locate()         
                    .htmlNode("xpath_or_selector")  
                    .pointTo()                      //Mouseover action 
                    .pointTo(Keys.CTRL_LEFT)        //Mouseover with specified keyboard key pressed down
                    .click()                        //Mouse click
                    .click(Keys.ALT_LEFT)           //Mouse click with specified keyboard key pressed down   
                    .scrollIntoView();              //Scrolls into node's viewport

            //--- HTML nodes ---///
            
            List<HtmlNode> htmlNodes = tab.locate() //Same for all HtmlNode children, like InputNode, SelectNode,
                    .htmlNodes("xpath_or_selector"); // InputNode.Radio   
        
            //--- JS code execution ---//
        
            htmlNode.applyJSFunction(
                    "function f(){this.attr={}}", null, false);  //Applies given JS function to a node.  
                                                                 // Node can be addresses via 'this' keyword                                                                                    
    }
}
```

* HTML node's attributes:
```java
public class NodeAttributesInteraction {

    public static void main(String[] args) {
        //--- Check attribute is modified ---//

        HtmlNode htmlNode = tab.locate()
                .htmlNode("//a[@class='before_click']")
                .click();                                //After the click class value is changed to 'after_click'

        tab.attributeModified("class", "before_click");  //Will throw an exception, if class value not changed
        
        //--- Check aria is expanded ---//
        
        HtmlNode htmlNode = tab.locate()
                .htmlNode("//a[@class='aria_style']")
                .click();                             //Once node is clicked, aria is expanded

        htmlNode.ariaExpanded(true);                  //Will throw an exception, if aria not expanded
        
        //--- Set attribute ---//
        
        htmlNode.setAttributeValue("attr_name", "attr_value"); //Sets new attribute name and value                  
    }
}
```

* DOMNodeInserted / DOMNodeRemoved events handling:
```java
public class VerifyNewNodeWasInsertedAndThenRemoved {

    public static void main(String[] args) {
        tab.locate()
                .htmlNode("#id")  //Assume this click forces a new DIV with "title='hello'" attribute
                .click();         // to be inserted into DOM (like popup window, for instance)
      
        List<String> attrs = List.of("title", "hello");
        int nodeId = tab.domNodeInserted("div", attrs);  //Verify new div is inserted (popup window appears)
        tab.clickZero();                                 //Force popup window to be closed
        tab.domNodeRemoved(nodeId);                      //Verify new div was removed from DOM
    }
}
```

* Switch to a new tab:
```java
public class SwitchToNewTab {
    
    public static void main(String[] args) {           
        tab.locate()
                .htmlNode("//a[contains(text(), 'some-text')]")
                .click(); //new tab is opened on click

        String targetId = tab.expectNewTabOpen(); // get new tab id
        Tab newTab = cxt.switchToTarget(targetId, Tab.class); //get Tab object of the new opened tab
        newTab.waitPageLoadNetworkIdle(); //wait until new tab completely is loaded
    }
}
```

* Take a screenshot:
```java
public class TakeScreenshot {

    public static void main(String[] args) {           
        Path path = Paths.get(System.getProperty("user.home"), "quaacr_screenshot.png"); //path to screenshot 
        String selector = ".grid__fl1";
        tab.screenshot().capture(ImageFormat.png, selector).asFile(path); //takes screenshot of selected region (by selector)
        File png = path.toFile();
    }
}
```

* Listen to network requests:
```java
public class NetworkRequests {

    public static void main(String[] args) {
        Tab.NetworkView network = tab.getNetworkView();          //Network view must be evoked prior to any requests
                                                                 // execution you'd like to listen to. Is' similar
                                                                 // to opening DevTools Network tab: no requests are
                                                                 // recording until Network tab is activated 
        String reqId = network.waitRequestFinished(
                "/endpoint", HttpMethod.POST);                   //Waits until the request with specified endpoint 
                                                                 // is finished. Throws an exception on failure
        
        String responseBody = network.getResponseBody(reqId);    //Returns response body of request
        
        String postData = network.getRequestPostData(reqId);     //Returns response POST data (if it's a POST request)
        
        network.setCookies(List.of(Network.CookieParam.of(
                "name", "value", "https://some.url")));          //Sets a list of cookies for the current session
        
        network.clearCookies();                                  //Clears all session cookies

        List<Network.Cookie> cookies = network.getAllCookies();  //Returns all session cookies
        
        network.clearCache();                                    //Clears the cache
    }
}
```

* Web browser logs:
```java
public class BrowserLogs {

    public static void main(String[] args) {
        Tab.LogView logs = tab.getLogView();                     //Log view must be evoked prior to any actions you'd
                                                                 // like to gets the logs from

        List<Log.LogEntry> browserLogs = logs.getBrowserLogs();  //Returns all the logs emitted by a browser

        Log.LogEntry browserLog = logs.getNewestBrowserLog();    //Returns the newest browser log

        List<Runtime.ConsoleLog> consoleLogs 
                = logs.getConsoleLogs();                         //Returns all console logs

        Runtime.ConsoleLog oldestConsoleLog = 
                logs.getOldestConsoleLog();                      //Returns the oldest console log

        List<Runtime.ConsoleAPI> apiCalls =
                logs.getAllConsoleAPICalls();                    //Returns all console API calls

        logs.clear();                                            //Clears all the logs
    }
}
```

* Device emulation:
```java
public class DeviceEmulation {

    public static void main(String[] args) {
        LocalContextBuilder builder = new LocalContextBuilder();
        builder.setWindowSize(
                DeviceScreenSize.IPHONE.width(),                //Sets browser window size similar to the
                DeviceScreenSize.IPHONE.height());              // emulated device screen size. It's not
                                                                // necessary, but is recommended measure, as
                                                                // browser size might affect DOM structure
        BrowserContext cxt = Contexts.local(builder);
        Tab tab = cxt.defaultTab();
        tab.emulate().device(
           DeviceMetricsEmulation.IPHONE_6_7_8_PLUS);           //Enables device emulation
        tab.open("https://m.mobile.site");
        tab.waitPageLoad();
        tab.locate()
                .htmlNode("selector")
                .touch()                                        //Emulates 'touch' touchscreen user action
                .touch(Keys.CTRL_LEFT);                         //Emulates 'touch' touchscreen user action
                                                                // with the keyboard key pressed down

        tab.emulateReset();                                     //Resets device emulation for given tab
        tab.reload();                                           //Reloads the page. Not necessary, but
                                                                // it's recommended to reset DOM mobile view
    }
}
```

To be continued... (Keyboard, DialogHandler, Download)

### Logging ###

* To play with the logging, use [Logback](http://logback.qos.ch/) config (or any other log4j wrapper). It means, that you need to create `logback.xml` 
file in `src/main/resources` (Maven project way) with the following content (this is *debug* example, but don't be 
limited with this - search in Internet for answers):

         <configuration>
              <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
                  <encoder>
                      <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n</pattern>
                  </encoder>
              </appender>
          
              <root level="debug">
                  <appender-ref ref="STDOUT" />
              </root>
          </configuration>
          
### Run concurrently ###
No. It doesn't make any sense.

**Thank you**.