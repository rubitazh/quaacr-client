/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class FileUtils {

    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

    public static void deleteDirectory(Path path) {
        try {
            Files.walkFileTree(path, fileVisitor());

        } catch (NoSuchFileException e) {
            if (logger.isDebugEnabled()) {
                logger.info("All profile dirs already deleted.");
            }

        } catch (IOException e) {
            logger.warn("Cannot delete dir, maybe some bad carma involved. Let's try one more time. [path={}]", path);

            try {
                Files.walkFileTree(path, fileVisitor());

                logger.info("Hooray, all profile dirs deleted.");

            } catch (NoSuchFileException ex) {
                if (logger.isDebugEnabled()) {
                    logger.info("All profile dirs already deleted.");
                }

            } catch (IOException ex) {
                logger.error("Sorry, dude, still no luck with deleting that dir. Probably this is my fault, " +
                        "but check stacktrace, just in case.", e);
            }
        }
    }

    private static FileVisitor<? super Path> fileVisitor() {
        return new SimpleFileVisitor<>() {

            @Override
            public FileVisitResult postVisitDirectory(
                    Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(
                    Path file, BasicFileAttributes attrs)
                    throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
        };
    }
}
