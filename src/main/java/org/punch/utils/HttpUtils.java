/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.stream.Collectors;

public class HttpUtils {

    public static String readResponse(HttpURLConnection conn) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
            StringBuilder sb = new StringBuilder();
            List<String> lines = br.lines().collect(Collectors.toList());

            if (lines.isEmpty()) {
                throw new RuntimeException("Empty HTTP response from server.");
            }

            if (lines.size() == 1) {
                return lines.get(0); //there was a reason, but I don't remember it.
            }

            lines.forEach(l -> sb.append(l).append("\n"));
            return sb.toString();
        }
    }
}
