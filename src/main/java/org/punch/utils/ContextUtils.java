/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.utils;

import org.punch.context.ContextConfig;
import org.punch.context.SessionData;
import org.punch.context.entities.BackgroundPage;
import org.punch.context.entities.IFrame;
import org.punch.context.entities.Tab;
import org.punch.context.enums.TargetType;
import org.punch.context.ex.BrowserSessionException;
import org.punch.devtools.impl.BasicEventConsumer;
import org.punch.devtools.impl.DomainFactory;
import org.punch.devtools.interfaces.DevToolsClient;
import org.punch.devtools.protocol.target.Target;

import java.util.Objects;
import java.util.concurrent.ExecutorService;

/* Browser context utils. It's public cause it's used by tests. */
public class ContextUtils {

    private static final String ABOUT_BLANK = "about:blank";

    /* Creates background_page session instance for an existing background page. */
    public static BackgroundPage createBackgroundPage(ContextConfig config,
                                                      DevToolsClient client,
                                                      SessionData sessionData,
                                                      DomainFactory domainFactory) {

        var sessionId = sessionData.getSessionId();
        var eventConsumer = new BasicEventConsumer(config);
        client.addEventConsumer(sessionId, eventConsumer);
        return new BackgroundPage(config, sessionData, domainFactory, eventConsumer);
    }

    /* Creates  brand new about:blank browser page session. */
    public static SessionData createBlankPageSessionData(DomainFactory domainFactory) {
        var target = domainFactory.getDomain(Target.class);
        var targetId = target.createTarget(ABOUT_BLANK, null, null, null, false, false, false);
        var sessionId = target.attachToTarget(targetId, true);
        return new SessionData(targetId, sessionId);
    }

    /* Creates new iframe session instance for an existing iframe. */
    public static IFrame createIFrame(ContextConfig config,
                                      DevToolsClient client,
                                      SessionData sessionData,
                                      DomainFactory domainFactory) {

        var sessionId = sessionData.getSessionId();
        var eventConsumer = new BasicEventConsumer(config);
        client.addEventConsumer(sessionId, eventConsumer);
        return new IFrame(config, sessionData, domainFactory, eventConsumer);
    }

    /* Creates new page (tab) session instance for an existing page. */
    public static Tab createTab(ContextConfig config,
                                DevToolsClient client,
                                SessionData sessionData,
                                DomainFactory domainFactory,
                                ExecutorService threadPool,
                                boolean isNewTabNavigated) {

        var sessionId = sessionData.getSessionId();
        var eventConsumer = new BasicEventConsumer(config);
        client.addEventConsumer(sessionId, eventConsumer);
        return new Tab(config, sessionData, domainFactory, eventConsumer, threadPool, isNewTabNavigated);
    }

    /* Creates a page session for the very first browser page on start. */
    public static SessionData getBlankPageSessionData(DomainFactory domainFactory, long awaitTargetPageTimeout) {
        final long start = System.currentTimeMillis();
        var target = domainFactory.getDomain(Target.class);
        target.setDiscoverTargets(true);

        Target.TargetInfo targetInfo;
        do {
            targetInfo = target.getTargets().stream()
                    .filter(ti -> TargetType.TAB.value().equals(ti.getType()))
                    .findFirst()
                    .orElse(null);

            if ((System.currentTimeMillis() - start) >= awaitTargetPageTimeout) {
                throw new BrowserSessionException("Target of 'page' type not found by timeout. " +
                        "[timeout=" + awaitTargetPageTimeout + "]");
            }

        } while (Objects.isNull(targetInfo));

        var targetId = targetInfo.getTargetId();
        var sessionId = target.attachToTarget(targetId, true);
        return new SessionData(targetId, sessionId);
    }

    /* Returns session for given target. */
    public static SessionData getSessionDataByTargetId(DomainFactory domainFactory, String targetId) {
        var target = domainFactory.getDomain(Target.class);
        var sessionId = target.attachToTarget(targetId, true);
        return new SessionData(targetId, sessionId);
    }
}
