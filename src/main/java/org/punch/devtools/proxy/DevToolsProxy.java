/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.proxy;

import org.json.JSONObject;
import org.punch.devtools.ex.ProtocolResponseErrorException;
import org.punch.devtools.interfaces.DevToolsClient;
import org.punch.devtools.interfaces.JsonConverter;
import org.punch.devtools.protocol.annotations.ObjectParam;
import org.punch.devtools.protocol.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Proxy;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class DevToolsProxy {

    private static final Logger logger = LoggerFactory.getLogger(DevToolsProxy.class);

    /* JSON error related queries */
    static final String ERROR_MSG_Q = "/error/message";
    static final String ERROR_TEXT_Q = "/result/errorText";

    private final DevToolsClient client;
    private final JsonConverter converter;

    public DevToolsProxy(DevToolsClient client, JsonConverter converter) {
        this.client = client;
        this.converter = converter;
    }

    @SuppressWarnings("unchecked")
    public <T> T getProxy(Class<T> cls) {
        return (T) Proxy.newProxyInstance(
                DevToolsProxy.class.getClassLoader(),
                new Class[]{cls},
                new DevToolsInvocationHandler(cls.getSimpleName(), client, converter));
    }

    @SuppressWarnings("unchecked")
    public <T> T getProxy(Class<T> cls, String sessionId) {
        return (T) Proxy.newProxyInstance(
                DevToolsProxy.class.getClassLoader(),
                new Class[]{cls},
                new DevToolsInvocationHandler(cls.getSimpleName(), client, converter, sessionId));
    }

    static class DevToolsInvocationHandler implements InvocationHandler {

        private static final String VOID = "void";

        private final FixedList<Integer> usedIds = new FixedList<>(10);
        private final Random random = new Random();

        private final String domain;
        private final DevToolsClient client;
        private final JsonConverter converter;
        private String sessionId;

        public DevToolsInvocationHandler(String domain, DevToolsClient client, JsonConverter converter) {
            this.domain = domain;
            this.client = client;
            this.converter = converter;
        }

        public DevToolsInvocationHandler(String domain, DevToolsClient client,
                                         JsonConverter converter, String sessionId) {
            this(domain, client, converter);
            this.sessionId = sessionId;
        }

        @Override
        public Object invoke(Object proxy, Method mtd, Object[] args) {
            String mtdName = mtd.getName();
            String cdpMethod = domain.concat(".").concat(mtdName);

            if (mtdName.equals("equals")) {
                //TODO: impl
                return false;
            }

            if (mtdName.equals("hashCode")) {
                return System.identityHashCode(proxy);
            }

            if (mtdName.equals("toString")) {
                return cdpMethod;
            }

            JSONObject req = new JSONObject();

            int id;
            for (;;) {
                id = random.nextInt(Integer.MAX_VALUE);
                if (!usedIds.contains(id)) {
                    usedIds.add(id);
                    break;
                }
            }

            req.put("id", id);
            req.put("method", cdpMethod);

            if (Objects.nonNull(sessionId)) {
                req.put("sessionId", sessionId);
            }

            JSONObject params = null;
            if (Objects.nonNull(args)) {
                params = new JSONObject();
                Parameter[] prms = mtd.getParameters();

                for (int i = 0; i < prms.length; i++) {
                    Object arg = args[i];

                    if (Objects.nonNull(arg) && arg.getClass().isAnnotationPresent(ObjectParam.class)) {
                        arg = new JSONObject(arg);
                    }

                    params.put(prms[i].getAnnotation(Param.class).value(), arg);
                }

                req.put("params", params);
            }

            JSONObject resp = client.sendReq(id, req.toString());

            Object errTxt;
            if (resp.has("result") && Objects.nonNull((errTxt = resp.query(ERROR_TEXT_Q)))) {
                throw new ProtocolResponseErrorException("Method result error. [err=" + errTxt + ", method=" + cdpMethod + "]");
            }

            if (resp.has("error")) {
                final String BODY = "Method response error. [msg='%s', method=%s%s]";
                var err = resp.query(ERROR_MSG_Q);

                if (Objects.isNull(params)) {
                    throw new ProtocolResponseErrorException(String.format(BODY, err, cdpMethod, ""));
                }

                throw new ProtocolResponseErrorException(String.format(BODY, err, cdpMethod, ", params=" + params));
            }

            return mtd.getReturnType().getName().equals(VOID)
                    ? Void.TYPE : converter.convert(mtd, resp);
        }
    }

    private static class FixedList<T> {

        private final AtomicInteger counter = new AtomicInteger(0);
        private final int capacity;
        private final Object[] data;

        private FixedList(int capacity) {
            this.capacity = capacity;
            this.data = new Object[capacity];
        }

        private void add(T e) {
            data[counter.getAndIncrement() % capacity] = e;
        }

        private boolean contains(T e) {
            for (int i = 0; i < capacity; i++) {

                if (Objects.equals(data[i], e)) {
                    return true;
                }
            }

            return false;
        }
    }
}
