package org.punch.devtools.impl;

import org.json.JSONObject;

enum Poison {
    evt;

    private final JSONObject pill = new JSONObject("{" +
            "\"poison\": \"DIE_PILL\"," +
            "\"error\": {\"message\": \"Quaacr received terminate request via a client or due to a DevTools protocol " +
            "error (see debug for details). No new tasks will be started and all current tasks will be shutdown.\"}" +
            "}");

    JSONObject pill() {
        return pill;
    }
}
