/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.impl;

import org.punch.devtools.impl.singletons.BasicJsonConverter;
import org.punch.devtools.interfaces.DevToolsClient;
import org.punch.devtools.interfaces.JsonConverter;
import org.punch.devtools.proxy.DevToolsProxy;

public class DomainFactory {

    private final JsonConverter jsonConverter = BasicJsonConverter.INSTANCE;
    private final DevToolsClient client;

    public DomainFactory(DevToolsClient client) {
        this.client = client;
    }

    public <T> T getDomain(Class<T> cls) {
        return new DevToolsProxy(client, jsonConverter).getProxy(cls);
    }

    public <T> T getSessionDomain(String sessionId, Class<T> cls) {
        return new DevToolsProxy(client, jsonConverter).getProxy(cls, sessionId);
    }
}
