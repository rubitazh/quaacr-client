/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.impl.singletons;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONPointer;
import org.punch.devtools.interfaces.JsonConverter;
import org.punch.devtools.protocol.annotations.Result;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public enum BasicJsonConverter implements JsonConverter {
    INSTANCE;

    private static final String RESULT = "result";
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Object convert(Method mtd, JSONObject response) {
        Result resultMeta = mtd.getAnnotation(Result.class);
        Object resultJson = resultMeta.key().isEmpty()
                ? response.get(RESULT)
                : response.query(new JSONPointer("/" + RESULT + "/" + resultMeta.key()));
        try {
            if (Objects.equals(resultJson.getClass(), JSONArray.class)) {
                Class<?> arrayType = resultMeta.type();

                if (arrayType.equals(Void.class)) {
                    throw new IllegalCallerException("Array type not specified. [method=" + mtd.getName() + "]");
                }

                //String arrays must be handled separately
                // (Jackson throws exception on '/' symbol, for instance):
                if (arrayType.equals(String.class)) {
                    return mapper.readValue(resultJson.toString(), new TypeReference<List<String>>() {
                    });
                }

                JSONArray array = (JSONArray) resultJson;
                List<Object> lst = new ArrayList<>();

                for (Object o : array) {
                    Object jsn = mapper.readValue(o.toString(), arrayType);
                    lst.add(jsn);
                }

                return lst;
            }

            if (Objects.equals(resultJson.getClass(), JSONObject.class)) {
                return mapper.readValue(resultJson.toString(), mtd.getReturnType());
            }

            return resultJson;

        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public <T> T toBean(String obj, Class<T> cls) {
        try {
            return mapper.readValue(obj, cls);

        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
