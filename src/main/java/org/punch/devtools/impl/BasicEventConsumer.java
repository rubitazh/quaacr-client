/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.impl;

import org.json.JSONObject;
import org.punch.context.ContextConfig;
import org.punch.devtools.ex.PollEventException;
import org.punch.devtools.interfaces.EventConsumer;
import org.punch.devtools.interfaces.ProtocolEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static org.punch.devtools.protocol.dom.DOM.Event.setChildNodes;

/**
 * Default {@link EventConsumer} implementation.
 */
public class BasicEventConsumer implements EventConsumer {

    private static final Logger logger = LoggerFactory.getLogger(EventConsumer.class);

    private static final String WAIT_FIRED_EVT = "waitFiredEvent";
    private static final String GET_FIRED_EVT = "getFiredEvent";
    private static final String METHOD = "method";

    /**
     * List of the methods to be ignored. We place here events that are not currently used in quaacr
     * and could potentially consume a lot of heap space being added into a corresponding queue.
     */
    private static final List<String> BLACK_LIST = List.of(setChildNodes.method());

    /**
     * Event queues consumed by this class, keyed by event name. Pretty simple.
     */
    private final Map<String, BlockingQueue<JSONObject>> events = new ConcurrentHashMap<>();

    private final Long awaitEventTimeout;
    private final int queueLimit;

    public BasicEventConsumer(ContextConfig cfg) {
        this.awaitEventTimeout = cfg.getAwaitEventTimeout();
        this.queueLimit = cfg.getConsumerQueueLimit();
    }

    @Override
    public void clearAllQueues() {
        events.values().forEach(BlockingQueue::clear);
    }

    @Override
    public void consumeEvent(JSONObject e) {
        if (e.equals(Poison.evt.pill())) {
            events.values().forEach(q -> q.offer(e));
            return;
        }

        var method = e.getString(METHOD);

        if (BLACK_LIST.contains(method)) {
            return;
        }

        var queue = getByKey(method);
        queue.offer(e);
    }

    @Override
    public JSONObject getFiredEvent(ProtocolEvent pe, Function<JSONObject, Boolean> rule) throws PollEventException {
        var thread = getCurrentThread();
        long start = System.currentTimeMillis();
        var queue = getByKey(pe.method());

        while (true) {
            if (thread.isInterrupted()) {
                throwPollInterrupted(GET_FIRED_EVT, pe);
            }

            if (extTimeoutExceeded(start)) {
                throwPollTimeout(GET_FIRED_EVT, pe);
            }

            JSONObject o;

            try {
                o = pollNext(queue, pe);

                if (isPoisonPill(o)) {
                    throwPollInterrupted(GET_FIRED_EVT, pe);
                }

                if ((Objects.isNull(rule) || rule.apply(o))) {
                    return o;
                }

                queue.offer(o);

            } catch (InterruptedException e) {
                throwPollInterrupted(GET_FIRED_EVT, pe);
            }
        }
    }

    public JSONObject getFiredEvent(ProtocolEvent evt) throws PollEventException {
        return getFiredEvent(evt, null);
    }

    @Override
    public void waitFiredEvent(ProtocolEvent pe, Function<JSONObject, Boolean> rule) throws PollEventException {
        var thread = getCurrentThread();
        long start = System.currentTimeMillis();
        var queue = getByKey(pe.method());

        while (true) {
            if (thread.isInterrupted()) {
                throwPollInterrupted(WAIT_FIRED_EVT, pe);
            }

            if (extTimeoutExceeded(start)) {
                throwPollTimeout(WAIT_FIRED_EVT, pe);
            }

            try {
                var o = pollNext(queue, pe);

                if (isPoisonPill(o)) {
                    throwPollInterrupted(WAIT_FIRED_EVT, pe);
                }

                if (Objects.isNull(rule) || rule.apply(o)) {
                    break;
                }

                queue.offer(o);

            } catch (InterruptedException e) {
                throwPollInterrupted(WAIT_FIRED_EVT, pe);
            }
        }
    }

    @Override
    public List<JSONObject> pollAllEvents(ProtocolEvent pe) {
        var queue = getByKey(pe.method());

        if (queue.isEmpty()) {
            return null;
        }

        List<JSONObject> events = new ArrayList<>();
        int amount = queue.drainTo(events);

        if (logger.isDebugEnabled()) {
            logger.debug("Events successfully polled. [evt={}, amount={}]", pe.method(), amount);
        }

        return events;
    }

    private BlockingQueue<JSONObject> getByKey(String key) {
        var queue = events.get(key);

        if (Objects.isNull(queue)) {
            queue = new LinkedBlockingQueue<>();
            events.put(key, queue);
        }

        if (queue.size() > queueLimit) {
            var toBeRemoved = new ArrayList<>();
            //Remove the half of events from the queue (the oldest ones):
            queue.drainTo(toBeRemoved, (queueLimit >> 1));
            toBeRemoved.clear();
            toBeRemoved = null;

            if (logger.isDebugEnabled()) {
                logger.debug("Query cleared. [key={}]", key);
            }
        }

        return queue;
    }

    private Thread getCurrentThread() {
        return Thread.currentThread();
    }

    private JSONObject pollNext(BlockingQueue<JSONObject> queue, ProtocolEvent evt)
            throws InterruptedException, PollEventException {

        var o = queue.poll(awaitEventTimeout, TimeUnit.MILLISECONDS);

        if (Objects.isNull(o)) {
            throw new PollEventException("Poll event timeout. [evt=" + evt + "]");
        }

        return o;
    }

    private boolean isPoisonPill(JSONObject o) {
        if (o.equals(Poison.evt.pill())) {

            if (logger.isDebugEnabled()) {
                logger.debug("Poison pill consumed. Tell my parents I died as a warrior.");
            }

            return true;
        }

        return false;
    }

    private void throwPollInterrupted(String task, ProtocolEvent evt) throws PollEventException {
        throw new PollEventException(String.format("Poll interrupted. [task=%s, event=%s]", task, evt.method()));
    }

    private void throwPollTimeout(String task, ProtocolEvent evt) throws PollEventException {
        throw new PollEventException(String.format("Poll event timeout. [task=%s, event=%s]", task, evt.method()));
    }

    private boolean extTimeoutExceeded(long start) {
        return (System.currentTimeMillis() - start) > awaitEventTimeout;
    }
}
