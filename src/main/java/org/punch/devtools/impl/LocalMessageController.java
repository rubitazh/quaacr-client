/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.impl;

import org.glassfish.tyrus.client.ClientManager;
import org.punch.devtools.interfaces.MessageController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@ClientEndpoint
public class LocalMessageController implements MessageController {

    private static final Logger logger = LoggerFactory.getLogger(LocalMessageController.class);

    private final BlockingQueue<String> published = new LinkedBlockingQueue<>();
    private final String wsUrl;
    private Session session;

    public LocalMessageController(String wsUrl) {
        this.wsUrl = wsUrl;
    }

    @Override
    public void connect() throws URISyntaxException, DeploymentException, IOException {
        var uri = new URI(wsUrl);
        var clientManager = ClientManager.createClient();
        session = clientManager.connectToServer(this, uri);
    }

    @Override
    public void send(String msg) {
        session.getAsyncRemote().sendText(msg);
    }

    @Override
    public String fetch() throws InterruptedException {
        return published.take();
    }

    @Override
    public void close() {
        try {
            session.close();

        } catch (IOException e) {
            logger.error("WS session close error.", e);
        }
    }

    @OnOpen
    public void opOpen(Session session) {
        if (logger.isDebugEnabled()) {
            logger.debug("WS session started. [id={}]", session.getId());
        }
    }

    @OnMessage
    public void onMessage(String msg) {
        try {
            published.put(msg);

        } catch (InterruptedException e) {
            logger.warn("Publish task canceled.");
            close();
        }
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        if (logger.isDebugEnabled()) {
            logger.debug("WS session closed. [sessionId={}]", session.getId());
        }
    }
}
