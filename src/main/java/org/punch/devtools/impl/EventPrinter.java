/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.impl;

import org.punch.devtools.interfaces.ProtocolEvent;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * This class enables specified DevTools event messages to be passed to some output, so it can be visible
 * to a client. Frankly speaking, I don't think it will be used a lot, so I keep it short. If you don't know
 * anything about DevTools protocol messages, but you are a curious person, then check the following link:
 * <a href='https://chromedevtools.github.io/devtools-protocol/'>DevTools Protocol</a>.
 * <p>
 * If you have some experience and you know, what you are doing, then use this class with a
 * {@link org.punch.context.ContextBuilder} instance to set up DevTools events printing.
 */
public class EventPrinter {

    private final List<String> events;
    private final Consumer<String> printer;

    /**
     * Constructs DevTools event printer. For instance, if you need "Network.requestWillBeSent' event to be
     * printed to standard output for debugging (I don't see any other reason to do this), while quaacr tries
     * to do its best to make your tests pass, then you need to:
     * <pre>{@code
     *     ContextBuilder cb = new RemoteContextBuilder();
     *     EventPrinter ep = new EventPrinter(List.of(Network.Event.requestWillBeSent), System.out::println);
     *     cb.setEventPrinter(ep);
     *     BrowserContext cxt = Contexts.remote(cb);
     * }</pre>
     *
     * @param events  list of events to be sent to output.
     * @param printer something that consumes a string (event in your case) and sends it to an output.
     */
    public EventPrinter(List<ProtocolEvent> events, Consumer<String> printer) {
        this.events = toStrRepresentation(events);
        this.printer = printer;

    }

    void addCandidate(String method, String txt) {
        if (events.contains(method)) {
            printer.accept(txt);
        }
    }

    private List<String> toStrRepresentation(List<ProtocolEvent> events) {
        return events.stream()
                .map(ProtocolEvent::method)
                .collect(Collectors.toList());
    }
}
