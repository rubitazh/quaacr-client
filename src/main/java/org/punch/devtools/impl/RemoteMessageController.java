/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.impl;

import org.punch.devtools.interfaces.MessageController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

public class RemoteMessageController implements MessageController {

    private static final Logger logger = LoggerFactory.getLogger(RemoteMessageController.class);

    private static final int N_LEN_BYTES = 4;
    private static final int N_ID_BYTES = 36;

    private final BlockingQueue<String> published = new LinkedBlockingQueue<>();

    private final String clientId;
    private final ProxyAddress addr;
    private final ExecutorService threadPool;

    private Socket client;
    private InputStream is;
    private OutputStream os;

    public RemoteMessageController(String clientId, ProxyAddress addr, ExecutorService threadPool) {
        this.clientId = clientId;
        this.addr = addr;
        this.threadPool = threadPool;
    }

    @Override
    public void connect() throws IOException {
        client = new Socket(addr.getHost(), addr.getPort());
        client.setTcpNoDelay(true);
        is = new BufferedInputStream(client.getInputStream());
        os = new BufferedOutputStream(client.getOutputStream());

        if (logger.isDebugEnabled()) {
            logger.debug("Message controller started. [host={}, port={}]", addr.getHost(), addr.getPort());
        }

        startListenProxy();
    }

    public String fetch() throws InterruptedException {
        return published.take();
    }

    @Override
    public void send(String txt) {
        if (clientId.length() != N_ID_BYTES) {
            close();
            throw new IllegalArgumentException("Invalid client id length. " +
                    "[exp=" + N_ID_BYTES + ", act=" + clientId.length() + "]");
        }

        try {
            //write length:
            byte[] mbf = txt.getBytes(StandardCharsets.UTF_8);
            byte[] lbf = ByteBuffer.allocate(N_LEN_BYTES).putInt(mbf.length).array();
            os.write(lbf);
            //write id:
            os.write(clientId.getBytes(StandardCharsets.UTF_8));
            //write message:
            os.write(mbf);
            os.flush();

        } catch (IOException e) {
            handleIOEx(e);
        }
    }

    @Override
    public void close() {
        if (!client.isClosed()) {
            try {
                client.close();

            } catch (IOException e) {
                logger.error("Close proxy connection error.", e);
            }
        }
    }

    private void startListenProxy() {
        threadPool.execute(() -> {
            try {
                byte[] lbf, ibf, mbf;

                if (logger.isDebugEnabled()) {
                    logger.info("Start listen to TCP proxy.");
                }

                while (true) {
                    //read length:
                    lbf = read(is, N_LEN_BYTES);
                    int len = ByteBuffer.wrap(lbf).getInt();

                    //read id:
                    ibf = read(is, N_ID_BYTES);
                    var id = new String(ibf, StandardCharsets.UTF_8);

                    if (!id.equals(clientId)) {
                        logger.error("Unexpected message read. [expId={}, actId={}]", clientId, id);
                        close();
                        break;
                    }

                    //read msg:
                    mbf = read(is, len);
                    published.put(new String(mbf, StandardCharsets.UTF_8));
                }

            } catch (IOException | InterruptedException e) {
                handleIOEx(e);
            }
        });
    }

    private byte[] read(InputStream is, int len) throws IOException {
        byte[] bf = new byte[len];
        int offset = 0;

        while (true) {
            int read = is.read(bf, offset, len - offset);

            if (read == -1) {
                throw new IOException("Connection closed unexpectedly.");
            }

            if (read > 0) {
                if (offset + read == len) return bf;

                offset += read;
            }
        }
    }

    private void handleIOEx(Throwable e) {
        if (client.isClosed()) {

            if (logger.isDebugEnabled()) {
                logger.info("Connection to TCP proxy closed.");
            }

            return;
        }

        logger.error("TCP proxy connection error.", e);
        close();
    }
}