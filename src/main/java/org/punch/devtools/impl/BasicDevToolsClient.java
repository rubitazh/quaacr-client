/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.impl;

import org.json.JSONObject;
import org.punch.context.ContextConfig;
import org.punch.devtools.ex.DevToolsImplException;
import org.punch.devtools.interfaces.DevToolsClient;
import org.punch.devtools.interfaces.EventConsumer;
import org.punch.devtools.interfaces.MessageController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Basic {@link DevToolsClient} implementation.
 * @see DevToolsClient
 */
public class BasicDevToolsClient implements DevToolsClient {

    private static final Logger logger = LoggerFactory.getLogger(BasicDevToolsClient.class);

    private static final String ID = "id";
    private static final String METHOD = "method";
    private static final String SESSION_ID = "sessionId";

    private final Map<String, EventConsumer> consumers = new ConcurrentHashMap<>();
    private final Map<Integer, CompletableFuture<JSONObject>> promises = new ConcurrentHashMap<>();
    private final AtomicBoolean stopRequested = new AtomicBoolean(false);

    private final ContextConfig config;
    private final MessageController controller;
    private final ExecutorService threadPool;

    //Fetch messages from controller:
    private Future<?> fetchTask;

    //Protocol events printer (debugging):
    private EventPrinter eventPrinter;

    public BasicDevToolsClient(ContextConfig config, MessageController controller, ExecutorService threadPool) {
        this.config = config;
        this.controller = controller;
        this.threadPool = threadPool;
    }

    @Override
    public void start() throws Exception {
        controller.connect();

        fetchTask = threadPool.submit(() -> {
            while (!threadPool.isShutdown()) {
                String txt;
                try {
                    txt = controller.fetch();

                } catch (InterruptedException e) {

                    if (logger.isDebugEnabled()) {
                        logger.debug("Controller stopped to fetch messages.");
                    }

                    controller.close();
                    break;
                }

                JSONObject msg = new JSONObject(txt);

                //If message has id, it means that it's a request or a response type message:
                if (msg.has(ID)) {
                    int id = msg.getInt(ID);
                    var future = promises.get(id);
                    //In case method dispatch was interrupted:
                    if (Objects.nonNull(future)) {
                        future.complete(msg);
                    }
                    // If it's not request/response then it's an event:
                } else {
                    //Print if printer is set:
                    if (Objects.nonNull(eventPrinter)) {
                        eventPrinter.addCandidate(msg.getString(METHOD), txt);
                    }

                    if (msg.has(SESSION_ID)) {
                        var sessionId = msg.getString(SESSION_ID);
                        var consumer = consumers.get(sessionId);

                        if (Objects.isNull(consumer)) {
                            //TODO: throw exception
                            logger.error("Consumer not set. [sessionId={}]", sessionId);
                            break;
                        }

                        consumer.consumeEvent(msg);

                    } else {
                        consumers.values().forEach(c -> c.consumeEvent(msg));
                    }
                }
            }

            if (!threadPool.isShutdown()) {
                throw new DevToolsImplException("Connection to publisher lost.");
            }
        });
    }

    @Override
    public void addEventConsumer(String sessionId, EventConsumer consumer) {
        consumers.putIfAbsent(sessionId, consumer);
    }

    @Override
    public JSONObject sendReq(int reqId, String req) {
        if (stopRequested.get()) {

            if (logger.isDebugEnabled()) {
                logger.debug("Request won't be sent - quaacr is about to stop. [req={}]", req);
            }

            return Poison.evt.pill();
        }

        promises.putIfAbsent(reqId, new CompletableFuture<>());
        controller.send(req);
        return awaitResponse(reqId);
    }

    @Override
    public void stop() throws InterruptedException {
        if (stopRequested.get()) {

            if (logger.isDebugEnabled()) {
                logger.debug("Devtools client 'stop' already requested.");
            }

            return;
        }

        stopRequested.compareAndSet(false, true);
        //Wait until all current requests are completed:
        awaitResponseQueueEmpty();
        //Cancel all consumers poll events tasks:
        startConsumerDieBeat();
        //Cancel controller fetch messages task:
        fetchTask.cancel(true);
        //Close controller:
        controller.close();
        //Shutdown:
        threadPool.shutdown();
        threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
    }

    @Override
    public void printProtocolEvents(EventPrinter eventPrinter) {
        this.eventPrinter = eventPrinter;
    }

    private JSONObject awaitResponse(int id) {
        try {
            return promises.get(id).get(config.getWsResponseTimeout(), TimeUnit.MILLISECONDS);

        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException("Unexpected protocol response interrupt.", e);

        } catch (TimeoutException e) {
            throw new DevToolsImplException("Protocol response not received.", e);

        } finally {
            promises.remove(id);
        }
    }

    private void awaitResponseQueueEmpty() {
        final long timeout = config.getWsResponseTimeout();
        final long start = System.currentTimeMillis();

        for (;;) {
            if (promises.isEmpty()) {

                if (logger.isDebugEnabled()) {
                    logger.debug("Response queue empty - quaacr can be closed safely now.");
                }

                break;
            }

            //This is mostly for me: means something was implemented badly, and I need to fix it.
            if ((System.currentTimeMillis() - start) >= timeout) {
                throw new DevToolsImplException("Response never received on exit - unexpected quaacr exit.");
            }
        }
    }

    private void startConsumerDieBeat() {
        final long DIE_BEAT_INTERVAL = 1000L;

        if (logger.isDebugEnabled()) {
            logger.debug("Consumer die beat is about to start...");
        }

        new Thread(() -> {
            int pillCounter = 0;
            while (!threadPool.isTerminated()) {
                try {
                    consumers.values().forEach(c -> c.consumeEvent(Poison.evt.pill()));

                    if (logger.isDebugEnabled()) {
                        logger.debug("Consumer die beat is running. [pillNum={}]", ++pillCounter);
                    }

                    TimeUnit.MILLISECONDS.sleep(DIE_BEAT_INTERVAL);

                } catch (InterruptedException e) {
                    break;
                }
            }

            if (logger.isDebugEnabled()) {
                logger.debug("Consumer die beat stopped.");
            }

        }).start();
    }
}
