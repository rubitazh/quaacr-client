/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.ex;

public class DevToolsImplException extends RuntimeException {

    public DevToolsImplException() {
        super();
    }

    public DevToolsImplException(String msg) {
        super(msg);
    }

    public DevToolsImplException(String msg, Throwable e) {
        super(msg, e);
    }
}
