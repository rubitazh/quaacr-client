/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.ex;

public class ProtocolResponseErrorException extends RuntimeException {

    public ProtocolResponseErrorException(String msg) {
        super(msg);
    }

    public ProtocolResponseErrorException(String msg, Throwable e) {
        super(msg, e);
    }
}
