/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.enums.log;

public enum LogEntrySource {
    xml,
    javascript,
    network,
    storage,
    appcache,
    rendering,
    security,
    deprecation,
    worker,
    violation,
    intervention,
    recommendation,
    other
}
