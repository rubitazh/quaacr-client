/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.enums.browser;

public enum BrowserPermission {
    GEOLOCATION("geolocation"),
    NOTIFICATION("notifications"),
    PUSH("push"),
    MIDI("midi"),
    CAMERA("camera"),
    MICROPHONE("microphone"),
    CLIPBOARD_READ("clipboard-read"),
    CLIPBOARD_WRITE("clipboard-write");

    private final String value;

    BrowserPermission(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
