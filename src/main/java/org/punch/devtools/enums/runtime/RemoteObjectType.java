/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.enums.runtime;

public enum RemoteObjectType {
    OBJECT("object"),
    FUNCTION("function"),
    UNDEFINED("undefined"),
    STRING("string"),
    NUMBER("number"),
    BOOLEAN("boolean"),
    SYMBOL("symbol"),
    BIGINT("bigint"),
    WASM("wasm");

    private String value;

    RemoteObjectType(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
