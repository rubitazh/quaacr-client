/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.enums.runtime;

public enum ConsoleAPIType {
    LOG("log"),
    DEBUG("debug"),
    INFO("info"),
    ERROR("error"),
    WARNING("warning"),
    DIR("dir"),
    DIRXML("dirxml"),
    TABLE("table"),
    TRACE("trace"),
    CLEAR("clear"),
    START_GROUP("startGroup"),
    START_GROUP_COLLAPSED("startGroupCollapsed"),
    END_GROUP("endGroup"),
    ASSERT("assert"),
    PROFILE("profile"),
    PROFILE_END("profileEnd"),
    COUNT("count"),
    TIME_END("timeEnd");

    private String value;

    ConsoleAPIType(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
