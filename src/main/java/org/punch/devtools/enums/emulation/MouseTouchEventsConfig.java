/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.enums.emulation;

import org.punch.devtools.protocol.emulation.Emulation;

/**
 * Touch/gesture events configuration. Used in {@link Emulation#setEmitTouchEventsForMouse}
 * as 'configuration' parameter.
 */
public enum MouseTouchEventsConfig {
    desktop, mobile
}
