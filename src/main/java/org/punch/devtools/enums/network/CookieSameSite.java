/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.enums.network;

/* https://tools.ietf.org/html/draft-west-first-party-cookies-07 */
public enum CookieSameSite {
    Strict, Lax, None
}
