/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.enums.page;

public enum TransitionType {
    link,
    typed,
    address_bar,
    auto_bookmark,
    auto_subframe,
    manual_subframe,
    generated,
    auto_toplevel,
    form_submit,
    reload,
    keyword,
    keyword_generated,
    other
}
