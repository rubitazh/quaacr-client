/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.network;

import org.punch.devtools.enums.network.CookiePriority;
import org.punch.devtools.enums.network.CookieSameSite;
import org.punch.devtools.interfaces.ProtocolEvent;
import org.punch.devtools.protocol.annotations.ObjectParam;
import org.punch.devtools.protocol.annotations.Param;
import org.punch.devtools.protocol.annotations.ProtocolType;
import org.punch.devtools.protocol.annotations.Result;

import java.util.List;

public interface Network {

    void clearBrowserCache();

    void clearBrowserCookies();

    void enable(@Param("maxTotalBufferSize") Integer maxTotalBufferSize,
                @Param("maxResourceBufferSize") Integer maxResourceBufferSize,
                @Param("maxPostDataSize") Integer maxPostDataSize);

    void disable();

    @Result(key = "cookies", type = Cookie.class)
    List<Cookie> getAllCookies();

    @Result(key = "cookies", type = Cookie.class)
    List<Cookie> getCookies(@Param("urls") List<String> urls);

    @Result(key = "body")
    String getResponseBody(@Param("requestId") String requestId);

    @Result(key = "postData")
    String getRequestPostData(@Param("requestId") String requestId);

    void setCacheDisabled(@Param("cacheDisabled") boolean cacheDisabled);

    @Result(key = "success")
    boolean setCookie(@Param("name") String name,
                      @Param("value") String value,
                      @Param("url") String url,
                      @Param("domain") String domain,
                      @Param("path") String path,
                      @Param("secure") boolean secure,
                      @Param("httpOnly") boolean httpOnly,
                      @Param("sameSite") CookieSameSite sameSite,
                      @Param("expires") Number expires,
                      @Param("priority") CookiePriority priority);

    void setCookies(@Param("cookies") List<CookieParam> cookies);

    @ProtocolType
    class Cookie extends CookieBean {
    }

    @ProtocolType
    @ObjectParam
    class CookieParam extends CookieParamBean {
    }

    enum Event implements ProtocolEvent {
        dataReceived, eventSourceMessageReceived, loadingFailed, loadingFinished,
        requestServedFromCache, requestWillBeSent, responseReceived, webSocketClosed,
        webSocketCreated, webSocketFrameError, webSocketFrameReceived,
        webSocketFrameSent, webSocketHandshakeResponseReceived, webSocketWillSendHandshakeRequest;

        @Override
        public String method() {
            return Network.class.getSimpleName() + "." + toString();
        }
    }
}
