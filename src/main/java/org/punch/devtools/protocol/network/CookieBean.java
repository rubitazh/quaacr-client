/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.network;

public class CookieBean {

    private String name;
    private String value;
    private String domain;
    private String path;
    private Number expires;
    private Integer size;
    private boolean httpOnly;
    private boolean secure;
    private boolean session;
    private String sameSite;
    private String priority;
    private boolean sameParty;
    private String sourceScheme;
    private int sourcePort;

    CookieBean() {
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getDomain() {
        return domain;
    }

    public String getPath() {
        return path;
    }

    public Number getExpires() {
        return expires;
    }

    public Integer getSize() {
        return size;
    }

    public boolean isHttpOnly() {
        return httpOnly;
    }

    public boolean isSecure() {
        return secure;
    }

    public boolean isSession() {
        return session;
    }

    public String getSameSite() {
        return sameSite;
    }

    public String getPriority() {
        return priority;
    }

    public boolean isSameParty() {
        return sameParty;
    }

    public String getSourceScheme() {
        return sourceScheme;
    }

    public int getSourcePort() {
        return sourcePort;
    }
}
