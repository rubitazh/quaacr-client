/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.network;

import org.punch.devtools.enums.network.CookiePriority;
import org.punch.devtools.enums.network.CookieSameSite;

public class CookieParamBean {

    /* Mandatory */

    private String name;
    private String value;

    /* Optional */

    private String url;
    private String domain;
    private String path;
    private boolean secure;
    private boolean httpOnly;
    private CookieSameSite sameSite;
    private Number expires;
    private CookiePriority priority;

    public static Network.CookieParam of(String name, String value, String url) {
        return of(name, value, url, "", "/");
    }

    CookieParamBean() {
    }

    public static Network.CookieParam of(String name, String value, String url, String domain, String path) {
        return new Network.CookieParam()
                .builder(name, value, url)
                .setDomain(domain)
                .setPath(path)
                .build();
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getUrl() {
        return url;
    }

    public String getDomain() {
        return domain;
    }

    public String getPath() {
        return path;
    }

    public boolean getSecure() {
        return secure;
    }

    public boolean getHttpOnly() {
        return httpOnly;
    }

    public CookieSameSite getSameSite() {
        return sameSite;
    }

    public Number getExpires() {
        return expires;
    }

    public CookiePriority getPriority() {
        return priority;
    }

    public Builder builder(String name, String value, String url) {
        return new Network.CookieParam().new Builder(name, value, url);
    }

    /* Builder */
    public class Builder {

        public Builder(String name, String value, String url) {
            CookieParamBean.this.name = name;
            CookieParamBean.this.value = value;
            CookieParamBean.this.url = url;
        }

        public Builder setDomain(String domain) {
            CookieParamBean.this.domain = domain;
            return this;
        }

        public Builder setPath(String path) {
            CookieParamBean.this.path = path;
            return this;
        }

        public Builder setSecure(boolean secure) {
            CookieParamBean.this.secure = secure;
            return this;
        }

        public Builder setHttpOnly(boolean httpOnly) {
            CookieParamBean.this.httpOnly = httpOnly;
            return this;
        }

        public Builder setSameSite(CookieSameSite sameSite) {
            CookieParamBean.this.sameSite = sameSite;
            return this;
        }

        public Builder setExpires(Number expires) {
            CookieParamBean.this.expires = expires;
            return this;
        }

        public Builder setPriority(CookiePriority priority) {
            CookieParamBean.this.priority = priority;
            return this;
        }

        public Network.CookieParam build() {
            return (Network.CookieParam) CookieParamBean.this;
        }
    }
}
