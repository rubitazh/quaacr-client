/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.fetch;

public class HeaderEntryBean {

    private String name;
    private String value;

    HeaderEntryBean() {
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
