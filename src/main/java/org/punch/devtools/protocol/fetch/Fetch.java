/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.fetch;

import org.punch.devtools.interfaces.ProtocolEvent;
import org.punch.devtools.protocol.annotations.ObjectParam;
import org.punch.devtools.protocol.annotations.Param;
import org.punch.devtools.protocol.annotations.ProtocolType;

import java.util.List;

public interface Fetch {

    void enable(@Param("patterns") List<RequestPattern> patterns,
                @Param("handleAuthRequests") boolean handleAuthRequests);

    void disable();

    void continueRequest(@Param("requestId") String requestId,
                         @Param("url") String url,
                         @Param("method") String method,
                         @Param("postData") String postData,
                         @Param("headers") List<HeaderEntry> headers);

    void continueWithAuth(@Param("requestId") String requestId,
                          @Param("authChallengeResponse") AuthChallengeResponse authChallengeResponse);

    enum Event implements ProtocolEvent {
        authRequired, requestPaused;

        @Override
        public String method() {
            return Fetch.class.getSimpleName() + "." + toString();
        }
    }

    enum Response {
        Default, CancelAuth, ProvideCredentials
    }

    @ProtocolType
    class RequestPattern extends RequestPatternBean {
    }

    @ProtocolType
    class HeaderEntry extends HeaderEntryBean {
    }

    @ProtocolType
    @ObjectParam
    class AuthChallengeResponse extends AuthChallengeResponseBean {
    }
}
