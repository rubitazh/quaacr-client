/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.fetch;

public class AuthChallengeResponseBean {

    private Fetch.Response response;
    private String username;
    private String password;

    public static Fetch.AuthChallengeResponse of(Fetch.Response response, String username, String password) {
        var authChallengeResponse = new Fetch.AuthChallengeResponse();
        authChallengeResponse.setResponse(response);
        authChallengeResponse.setUsername(username);
        authChallengeResponse.setPassword(password);
        return authChallengeResponse;
    }

    AuthChallengeResponseBean() {
    }

    public Fetch.Response getResponse() {
        return response;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setResponse(Fetch.Response response) {
        this.response = response;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
