/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.fetch;

public class RequestPatternBean {

    private String urlPattern;
    private String resourceType;
    private String requestStage;

    public static Fetch.RequestPattern of(String urlPattern) {
        Fetch.RequestPattern requestPattern = new Fetch.RequestPattern();
        requestPattern.setUrlPattern(urlPattern);
        return requestPattern;
    }

    RequestPatternBean() {
    }

    public String getUrlPattern() {
        return urlPattern;
    }

    public String getResourceType() {
        return resourceType;
    }

    public String getRequestStage() {
        return requestStage;
    }

    public void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public void setRequestStage(String requestStage) {
        this.requestStage = requestStage;
    }
}
