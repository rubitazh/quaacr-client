/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.input;

import org.punch.devtools.protocol.annotations.Experimental;
import org.punch.devtools.protocol.annotations.Param;
import org.punch.devtools.protocol.annotations.ProtocolType;

import java.util.List;

public interface Input {

    void dispatchKeyEvent(@Param("type") KeyType type,
                          @Param("nativeVirtualKeyCode") Integer nativeVirtualKeyCode,
                          @Param("windowsVirtualKeyCode") Integer windowsVirtualKeyCode,
                          @Param("code") String code,
                          @Param("key") String key,
                          @Param("text") String text,
                          @Param("location") Integer location,
                          @Param("keyIdentifier") String keyIdentifier,
                          @Param("unmodifiedText") String unmodifiedText,
                          @Param("modifiers") Integer modifiers,
                          @Param("timestamp") Number timestamp,
                          @Param("isKeypad") Boolean isKeypad,
                          @Param("isSystemKey") Boolean isSystemKey,
                          @Param("autoRepeat") Boolean autoRepeat);

    void dispatchMouseEvent(@Param("type") MouseType type,
                            @Param("x") Number x,
                            @Param("y") Number y,
                            @Param("modifiers") Integer modifiers,
                            @Param("button") MouseButton button,
                            @Param("clickCount") Integer clickCount,
                            @Param("deltaX") Number deltaX,
                            @Param("deltaY") Number deltaY);

    void dispatchTouchEvent(@Param("type") TouchType type,
                            @Param("touchPoints") List<TouchPoint> touchPoints,
                            @Param("modifiers") Integer modifiers,
                            @Param("timestamp") Number timestamp);

    @Experimental
    void insertText(@Param("text") String text);

    @ProtocolType
    class TouchPoint extends TouchPointBean {
    }

    enum KeyType {
        keyDown("keyDown"), keyUp("keyUp"), rawKeyDown("rawKeyDown"), ch("char");

        private final String value;

        KeyType(String value) {
            this.value = value;
        }

        public String value() {
            return value;
        }
    }

    enum MouseType {
        mousePressed, mouseReleased, mouseMoved, mouseWheel
    }

    enum TouchType {
        touchStart, touchEnd, touchMove, touchCancel
    }

    enum MouseButton {
        none, left, middle, right, back, forward
    }
}
