package org.punch.devtools.protocol.input;

public class TouchPointBean {

    private Number x;
    private Number y;

    public static Input.TouchPoint of(Number x, Number y) {
        Input.TouchPoint touchPoint = new Input.TouchPoint();
        touchPoint.setX(x);
        touchPoint.setY(y);
        return touchPoint;
    }

    TouchPointBean() {
    }

    public Number getX() {
        return x;
    }

    void setX(Number x) {
        this.x = x;
    }

    public Number getY() {
        return y;
    }

    void setY(Number y) {
        this.y = y;
    }
}
