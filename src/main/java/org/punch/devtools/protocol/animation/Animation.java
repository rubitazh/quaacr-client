/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.animation;

import org.punch.devtools.interfaces.ProtocolEvent;
import org.punch.devtools.protocol.annotations.ProtocolType;
import org.punch.devtools.protocol.annotations.Param;

import java.util.List;

public interface Animation {

    void disable();

    void enable();

    void releaseAnimations(@Param("animations") List<String> animations);

    void setPaused(@Param("animations") List<String> animations, @Param("paused") boolean paused);

    @ProtocolType
    class AnimationEffect extends AnimationEffectBean {
    }

    @ProtocolType
    class AnimationObject extends AnimationObjectBean {
    }

    @ProtocolType
    class KeyframesRule extends KeyframesRuleBean {
    }

    @ProtocolType
    class KeyframeStyle extends KeyframeStyleBean {
    }

    enum Type {
        CSSTransition, CSSAnimation, WebAnimation
    }

    enum Event implements ProtocolEvent {
        animationCanceled, animationCreated, animationStarted;

        @Override
        public String method() {
            return Animation.class.getSimpleName() + "." + toString();
        }
    }
}
