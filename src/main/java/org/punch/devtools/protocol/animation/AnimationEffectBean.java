/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.animation;

public class AnimationEffectBean {

    private Number delay;
    private Number endDelay;
    private Number iterationStart;
    private Number iterations;
    private Number duration;
    private String direction;
    private String fill;
    private Integer backendNodeId;
    private Animation.KeyframesRule keyframesRule;
    private String easing;

    AnimationEffectBean() {
    }

    public Number getDelay() {
        return delay;
    }

    public Number getEndDelay() {
        return endDelay;
    }

    public Number getIterationStart() {
        return iterationStart;
    }

    public Number getIterations() {
        return iterations;
    }

    public Number getDuration() {
        return duration;
    }

    public String getDirection() {
        return direction;
    }

    public String getFill() {
        return fill;
    }

    public Integer getBackendNodeId() {
        return backendNodeId;
    }

    public Animation.KeyframesRule getKeyframesRule() {
        return keyframesRule;
    }

    public String getEasing() {
        return easing;
    }
}
