/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.animation;

public class AnimationObjectBean {

    private String id;
    private String name;
    private boolean pausedState;
    private String playState;
    private Number playbackRate;
    private Number startTime;
    private Number currentTime;
    private Animation.Type type;
    private Animation.AnimationEffect source;
    private String cssId;

    AnimationObjectBean() {
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isPausedState() {
        return pausedState;
    }

    public String getPlayState() {
        return playState;
    }

    public Number getPlaybackRate() {
        return playbackRate;
    }

    public Number getStartTime() {
        return startTime;
    }

    public Number getCurrentTime() {
        return currentTime;
    }

    public Animation.Type getType() {
        return type;
    }

    public Animation.AnimationEffect getSource() {
        return source;
    }

    public String getCssId() {
        return cssId;
    }
}
