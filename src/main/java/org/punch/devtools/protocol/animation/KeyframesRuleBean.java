/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.animation;

import java.util.List;

public class KeyframesRuleBean {

    private String name;
    private List<Animation.KeyframeStyle> keyframes;

    KeyframesRuleBean() {
    }

    public String getName() {
        return name;
    }

    public List<Animation.KeyframeStyle> getKeyframes() {
        return keyframes;
    }
}
