/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.dom;

public class BackendNodeBean {

    private int nodeType;
    private String nodeName;
    private int backendNodeId;

    BackendNodeBean() {
    }

    public int getNodeType() {
        return nodeType;
    }

    public String getNodeName() {
        return nodeName;
    }

    public int getBackendNodeId() {
        return backendNodeId;
    }
}
