/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.dom;

import org.punch.devtools.interfaces.ProtocolEvent;
import org.punch.devtools.protocol.annotations.*;
import org.punch.devtools.protocol.runtime.Runtime;

import java.util.List;

public interface DOM {

    void enable();

    void disable();

    @Result(key = "node")
    Node describeNode(@Param("nodeId") Integer nodeId,
                      @Param("backendNodeId") Integer backendNodeId,
                      @Param("objectId") String objectId,
                      @Param("depth") Integer depth,
                      @Param("pierce") Boolean pierce);

    void focus(@Param("nodeId") Integer nodeId,
               @Param("backendNodeId") Integer backendNodeId,
               @Param("objectId") String objectId);

    @Result(key = "model")
    BoxModel getBoxModel(@Param("nodeId") Integer nodeId,
                         @Param("backendNodeId") Integer backendNodeId,
                         @Param("objectId") String objectId);

    @Experimental
    @Result(key = "quads", type = List.class)
    List<List<Number>> getContentQuads(@Param("nodeId") Integer nodeId,
                                       @Param("backendNodeId") Integer backendNodeId,
                                       @Param("objectId") String objectId);

    @Result(key = "root")
    Node getDocument(@Param("depth") Integer depth, @Param("pierce") Boolean pierce);

    @Result
    NodeForLocation getNodeForLocation(@Param("x") int x, @Param("y") int y,
                                       @Param("includeUserAgentShadowDOM") Boolean includeUserAgentShadowDOM,
                                       @Param("ignorePointerEventsNone") Boolean ignorePoinerEventsNone);

    @Result(key = "outerHTML")
    String getOuterHTML(@Param("nodeId") Integer nodeId,
                        @Param("backendNodeId") Integer backendNodeId,
                        @Param("objectId") String objectId);

    @Experimental
    @Result(key = "nodeIds", type = Integer.class)
    List<Integer> getSearchResults(@Param("searchId") String searchId,
                                   @Param("fromIndex") Integer fromIndex,
                                   @Param("toIndex") Integer toIndex);

    @Result(key = "nodeId")
    int requestNode(@Param("objectId") String objectId);

    @Result(key = "object")
    Runtime.RemoteObject resolveNode(@Param("nodeId") Integer nodeId,
                                     @Param("backendNodeId") Integer backendNodeId,
                                     @Param("objectGroup") String objectGroup,
                                     @Param("executionContextId") Integer executionContextId);

    @Experimental
    @Result
    PerformSearch performSearch(@Param("query") String query,
                                @Param("includeUserAgentShadowDOM") Boolean includeUserAgentShadowDOM);

    @Result(key = "nodeIds", type = Integer.class)
    List<Integer> querySelectorAll(@Param("nodeId") int nodeId, @Param("selector") String selector);

    void removeNode(@Param("nodeId") Integer nodeId);

    @Experimental
    void scrollIntoViewIfNeeded(@Param("nodeId") Integer nodeId,
                                @Param("backendNodeId") Integer backendNodeId,
                                @Param("objectId") String objectId,
                                @Param("rect") Rect rect);

    void setAttributeValue(@Param("nodeId") Integer nodeId,
                           @Param("name") String name,
                           @Param("value") String value);

    @Experimental
    void setNodeStackTracesEnabled(@Param("enable") boolean enable);

    @ProtocolType
    class BackendNode extends BackendNodeBean {
    }

    @ProtocolType
    class BoxModel extends BoxModelBean {
    }

    @ProtocolType
    class Node extends NodeBean {
    }

    @CustomType
    class NodeForLocation extends NodeForLocationBean {
    }

    @CustomType
    class PerformSearch extends PerformSearchBean {
    }

    @ProtocolType
    @ObjectParam
    class Rect extends RectBean {
    }

    enum Event implements ProtocolEvent {
        attributeModified, attributeRemoved, characterDataModified, childNodeCountUpdated,
        childNodeInserted, childNodeRemoved, documentUpdated, setChildNodes;

        @Override
        public String method() {
            return DOM.class.getSimpleName() + "." + toString();
        }
    }
}
