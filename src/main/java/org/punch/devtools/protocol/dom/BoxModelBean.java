/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.dom;

import java.util.List;

public class BoxModelBean {

    private List<Integer> content;
    private List<Integer> padding;
    private List<Integer> border;
    private List<Integer> margin;
    private int width;
    private int height;
    private Object shapeOutside;

    BoxModelBean() {
    }

    public List<Integer> getContent() {
        return content;
    }

    public List<Integer> getPadding() {
        return padding;
    }

    public List<Integer> getBorder() {
        return border;
    }

    public List<Integer> getMargin() {
        return margin;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Object getShapeOutside() {
        return shapeOutside;
    }
}
