/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.dom;

public class PerformSearchBean {

    private String searchId;
    private int resultCount;

    PerformSearchBean() {
    }

    public String getSearchId() {
        return searchId;
    }

    public int getResultCount() {
        return resultCount;
    }
}
