/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.dom;

import org.punch.devtools.enums.dom.CompatibilityMode;

import java.util.List;
import java.util.Objects;

public class NodeBean {

    private int nodeId;
    private int parentId;
    private int backendNodeId;
    private int nodeType;
    private String nodeName;
    private String localName;
    private String nodeValue;
    private int childNodeCount;
    private List<DOM.Node> children;
    private List<String> attributes;
    private String documentURL;
    private String baseURL;
    private String publicId;
    private String systemId;
    private String internalSubset;
    private String xmlVersion;
    private String name;
    private String value;
    private String pseudoType;
    private String shadowRootType;
    private String frameId;
    private DOM.Node contentDocument;
    private List<DOM.Node> shadowRoots;
    private DOM.Node templateContent;
    private List<DOM.Node> pseudoElements;
    private DOM.Node importedDocument;
    private List<DOM.BackendNode> distributedNodes;
    private boolean isSVG;
    private CompatibilityMode compatibilityMode;

    NodeBean() {
    }

    public int getNodeId() {
        return nodeId;
    }

    public int getParentId() {
        return parentId;
    }

    public int getBackendNodeId() {
        return backendNodeId;
    }

    public int getNodeType() {
        return nodeType;
    }

    public String getNodeName() {
        return nodeName;
    }

    public String getLocalName() {
        return localName;
    }

    public String getNodeValue() {
        return nodeValue;
    }

    public int getChildNodeCount() {
        return childNodeCount;
    }

    public List<DOM.Node> getChildren() {
        return children;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public String getDocumentURL() {
        return documentURL;
    }

    public String getBaseURL() {
        return baseURL;
    }

    public String getPublicId() {
        return publicId;
    }

    public String getSystemId() {
        return systemId;
    }

    public String getInternalSubset() {
        return internalSubset;
    }

    public String getXmlVersion() {
        return xmlVersion;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getPseudoType() {
        return pseudoType;
    }

    public String getShadowRootType() {
        return shadowRootType;
    }

    public String getFrameId() {
        return frameId;
    }

    public DOM.Node getContentDocument() {
        return contentDocument;
    }

    public List<DOM.Node> getShadowRoots() {
        return shadowRoots;
    }

    public DOM.Node getTemplateContent() {
        return templateContent;
    }

    public List<DOM.Node> getPseudoElements() {
        return pseudoElements;
    }

    public DOM.Node getImportedDocument() {
        return importedDocument;
    }

    public List<DOM.BackendNode> getDistributedNodes() {
        return distributedNodes;
    }

    public boolean getIsSVG() {
        return isSVG;
    }

    public CompatibilityMode getCompatibilityMode() {
        return compatibilityMode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj.getClass() != this.getClass()) {
            return false;
        }

        NodeBean other = (NodeBean) obj;

        return this.nodeType == other.nodeType
                && this.childNodeCount == other.childNodeCount
                && Objects.equals(this.nodeName, other.nodeName)
                && Objects.equals(this.localName, other.localName)
                && Objects.equals(this.nodeValue, other.nodeValue)
                && Objects.equals(this.attributes, other.attributes)
                && (this.children == null
                ? other.children == null
                : other.children != null && this.children.containsAll(other.children));
    }

    @Override
    public int hashCode() {
        int hash = 47;
        hash = hash * 13
                + nodeType
                + childNodeCount
                + (nodeName != null ? nodeName.hashCode() : 0)
                + (localName != null ? localName.hashCode() : 0)
                + (nodeValue != null ? nodeValue.hashCode() : 0)
                + (attributes != null ? attributes.hashCode() : 0)
                + (children != null ? children.stream().mapToInt(NodeBean::hashCode).sum() : 0);

        return hash;
    }
}
