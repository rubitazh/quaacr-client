/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.dom;

public class RectBean {

    private Number x;
    private Number y;
    private Number width;
    private Number height;

    RectBean() {
    }

    public Number getX() {
        return x;
    }

    public Number getY() {
        return y;
    }

    public Number getWidth() {
        return width;
    }

    public Number getHeight() {
        return height;
    }

    public void setX(Number x) {
        this.x = x;
    }

    public void setY(Number y) {
        this.y = y;
    }

    public void setWidth(Number width) {
        this.width = width;
    }

    public void setHeight(Number height) {
        this.height = height;
    }
}
