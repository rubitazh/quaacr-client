package org.punch.devtools.protocol.io;

import org.punch.devtools.protocol.annotations.CustomType;
import org.punch.devtools.protocol.annotations.Param;
import org.punch.devtools.protocol.annotations.Result;

public interface IO {

    void close(@Param("handle") String handle);

    @Result
    Read read(@Param("handle") String handle, @Param("offset") Integer offset, @Param("size") Integer size);

    @CustomType
    class Read extends ReadBean {
    }
}
