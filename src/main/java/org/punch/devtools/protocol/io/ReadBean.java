package org.punch.devtools.protocol.io;

public class ReadBean {

    private boolean base64Encoded;
    private String data;
    private boolean eof;

    ReadBean() {
    }

    public boolean isBase64Encoded() {
        return base64Encoded;
    }

    public String getData() {
        return data;
    }

    public boolean isEof() {
        return eof;
    }
}
