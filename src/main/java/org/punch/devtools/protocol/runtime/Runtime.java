/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.runtime;

import org.punch.devtools.interfaces.ProtocolEvent;
import org.punch.devtools.protocol.annotations.*;

import java.util.List;

public interface Runtime {

    @Result
    Object awaitPromise(@Param("promiseObjectId") String promiseObjectId,
                        @Param("returnByValue") Boolean returnByValue,
                        @Param("generatePreview") Boolean generatePreview);

    void enable();

    void disable();

    @Result
    FunctionResult callFunctionOn(@Param("functionDeclaration") String functionDeclaration,
                                  @Param("objectId") String objectId,
                                  @Param("arguments") List<CallArgument> arguments,
                                  @Param("silent") Boolean silent,
                                  @Param("returnByValue") Boolean returnByValue,
                                  @Param("generatePreview") Boolean generatePreview,
                                  @Param("userGesture") Boolean userGesture,
                                  @Param("awaitPromise") Boolean awaitPromise,
                                  @Param("executionContextId") Integer executionContextId,
                                  @Param("objectGroup") String objectGroup);

    @Result
    Object compileScript(@Param("expression") String expression,
                         @Param("sourceURL") String sourceURL,
                         @Param("persistScript") Boolean persistScript,
                         @Param("executionContextId") Integer executionContextId);

    void discardConsoleEntries();

    @Result
    FunctionResult evaluate(@Param("expression") String expression,
                            @Param("objectGroup") String objectGroup,
                            @Param("contextId") Integer contextId,
                            @Param("includeCommandLineAPI") Boolean includeCommandLineAPI,
                            @Param("silent") Boolean silent,
                            @Param("generatePreview") Boolean generatePreview,
                            @Param("userGesture") Boolean userGesture,
                            @Param("awaitPromise") Boolean awaitPromise,
                            @Param("replMode") Boolean replMode);

    @Result(key = "result", type = PropertyDescriptor.class)
    List<PropertyDescriptor> getProperties(@Param("objectId") String objectId,
                               @Param("ownProperties") Boolean ownProperties,
                               @Param("accessorPropertiesOnly") Boolean accessorPropertiesOnly,
                               @Param("generatePreview") Boolean generatePreview);

    @Result
    void releaseObject(@Param("objectId") String objectId);

    @ProtocolType
    class CallArgument extends CallArgumentBean {
    }

    @ProtocolType
    class CallFrame extends CallFrameBean {
    }

    @ProtocolType
    @ObjectParam
    class PropertyDescriptor extends PropertyDescriptorBean {
    }

    @ProtocolType
    class RemoteObject extends RemoteObjectBean {
    }

    @ProtocolType
    class StackTrace extends StackTraceBean {
    }

    @ProtocolType
    class StackTraceId extends StackTraceIdBean {
    }

    @CustomType
    class ConsoleAPI extends ConsoleAPIBean {
    }

    @CustomType
    class ConsoleLog extends ConsoleLogBean {
    }

    @CustomType
    class FunctionResult extends FunctionResultBean {
    }

    enum Event implements ProtocolEvent {
        consoleAPICalled, exceptionThrown, executionContextCreated;

        @Override
        public String method() {
            return Runtime.class.getSimpleName() + "." + toString();
        }
    }
}
