package org.punch.devtools.protocol.runtime;

import org.punch.devtools.enums.runtime.ConsoleAPIType;

import java.util.List;

public class ConsoleLogBean {

    private ConsoleAPIType type;
    private List<String> messages;
    private Number timestamp;

    public static Runtime.ConsoleLog of(String type, List<String> messages, Number timestamp) {
        Runtime.ConsoleLog consoleLog = new Runtime.ConsoleLog();
        consoleLog.setType(ConsoleAPIType.valueOf(type.toUpperCase()));
        consoleLog.setMessages(messages);
        consoleLog.setTimestamp(timestamp);
        return consoleLog;
    }

    ConsoleLogBean() {
    }

    public ConsoleAPIType getType() {
        return type;
    }

    public List<String> getMessages() {
        return messages;
    }

    public Number getTimestamp() {
        return timestamp;
    }

    public void setType(ConsoleAPIType type) {
        this.type = type;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public void setTimestamp(Number timestamp) {
        this.timestamp = timestamp;
    }
}
