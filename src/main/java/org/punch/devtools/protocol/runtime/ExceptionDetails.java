/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.runtime;

public class ExceptionDetails {

    private Integer exceptionId;
    private String text;
    private Integer lineNumber;
    private Integer columnNumber;
    private String scriptId;
    private String url;
    private Runtime.StackTrace stackTrace;
    private Runtime.RemoteObject exception;
    private Integer executionContextId;

    public Integer getExceptionId() {
        return exceptionId;
    }

    public String getText() {
        return text;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public Integer getColumnNumber() {
        return columnNumber;
    }

    public String getScriptId() {
        return scriptId;
    }

    public String getUrl() {
        return url;
    }

    public Runtime.StackTrace getStackTrace() {
        return stackTrace;
    }

    public Runtime.RemoteObject getException() {
        return exception;
    }

    public Integer getExecutionContextId() {
        return executionContextId;
    }
}
