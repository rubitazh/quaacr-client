/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.runtime;

public class PropertyDescriptorBean {

    private String name;
    private Runtime.RemoteObject value;
    private boolean writable;
    private Runtime.RemoteObject get;
    private Runtime.RemoteObject set;
    private boolean configurable;
    private boolean enumerable;
    private boolean wasThrown;
    private boolean isOwn;
    private Runtime.RemoteObject symbol;

    PropertyDescriptorBean() {
    }

    public String getName() {
        return name;
    }

    public Runtime.RemoteObject getValue() {
        return value;
    }

    public boolean isWritable() {
        return writable;
    }

    public Runtime.RemoteObject getGet() {
        return get;
    }

    public Runtime.RemoteObject getSet() {
        return set;
    }

    public boolean isConfigurable() {
        return configurable;
    }

    public boolean isEnumerable() {
        return enumerable;
    }

    public boolean isWasThrown() {
        return wasThrown;
    }

    public boolean getIsOwn() {
        return isOwn;
    }

    public Runtime.RemoteObject getSymbol() {
        return symbol;
    }
}
