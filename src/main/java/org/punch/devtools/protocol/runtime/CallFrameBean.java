/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.runtime;

public class CallFrameBean {

    private String functionName;
    private String scriptId;
    private String url;
    private Integer lineNumber;
    private Integer columnNumber;

    CallFrameBean() {
    }

    public String getFunctionName() {
        return functionName;
    }

    public String getScriptId() {
        return scriptId;
    }

    public String getUrl() {
        return url;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public Integer getColumnNumber() {
        return columnNumber;
    }
}
