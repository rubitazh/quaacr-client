/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.runtime;

public class StackTraceIdBean {

    private String id;
    private String debuggerId;

    StackTraceIdBean() {
    }

    public String getId() {
        return id;
    }

    public String getDebuggerId() {
        return debuggerId;
    }
}