package org.punch.devtools.protocol.runtime;

import java.util.List;

public class ConsoleAPIBean {

    private String type;
    private List<Runtime.RemoteObject> args;
    private Integer executionContextId;
    private Number timestamp;
    private Runtime.StackTrace stackTrace;
    private String context;

    ConsoleAPIBean() {
    }

    public String getType() {
        return type;
    }

    public List<Runtime.RemoteObject> getArgs() {
        return args;
    }

    public Integer getExecutionContextId() {
        return executionContextId;
    }

    public Number getTimestamp() {
        return timestamp;
    }

    public Runtime.StackTrace getStackTrace() {
        return stackTrace;
    }

    public String getContext() {
        return context;
    }
}
