/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.runtime;

public class CallArgumentBean {

    private Object value;
    private String unserializableValue;
    private String objectId;

    public static Runtime.CallArgument of(Object value) {
        var callArgument = new Runtime.CallArgument();
        callArgument.setValue(value);
        return callArgument;
    }

    CallArgumentBean() {
    }

    public Object getValue() {
        return value;
    }

    public String getUnserializableValue() {
        return unserializableValue;
    }

    public String getObjectId() {
        return objectId;
    }

    protected void setValue(Object value) {
        this.value = value;
    }

    protected void setUnserializableValue(String unserializableValue) {
        this.unserializableValue = unserializableValue;
    }

    protected void setObjectId(String objectId) {
        this.objectId = objectId;
    }
}
