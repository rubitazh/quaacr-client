/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.runtime;

import java.util.List;

public class StackTraceBean {

    private String description;
    private List<Runtime.CallFrame> callFrames;
    private Runtime.StackTrace parent;
    private Runtime.StackTraceId parentId;

    StackTraceBean() {
    }

    public String getDescription() {
        return description;
    }

    public List<Runtime.CallFrame> getCallFrames() {
        return callFrames;
    }

    public Runtime.StackTrace getParent() {
        return parent;
    }

    public Runtime.StackTraceId getParentId() {
        return parentId;
    }
}
