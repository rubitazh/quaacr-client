/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.runtime;

public class FunctionResultBean {

    private Runtime.RemoteObject result;
    private ExceptionDetails exceptionDetails;

    FunctionResultBean() {
    }

    public Runtime.RemoteObject getResult() {
        return result;
    }

    public ExceptionDetails getExceptionDetails() {
        return exceptionDetails;
    }
}
