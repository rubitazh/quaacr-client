/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.runtime;

public class RemoteObjectBean {

    private String type;
    private String subtype;
    private String className;
    private String description;
    private String objectId;
    private Object value;
    private String unserializableValue;
    private Object preview;
    private Object customPreview;

    RemoteObjectBean() {
    }

    public String getType() {
        return type;
    }

    public String getSubtype() {
        return subtype;
    }

    public String getClassName() {
        return className;
    }

    public String getDescription() {
        return description;
    }

    public String getObjectId() {
        return objectId;
    }

    public Object getValue() {
        return value;
    }

    public String getUnserializableValue() {
        return unserializableValue;
    }

    public Object getPreview() {
        return preview;
    }

    public Object getCustomPreview() {
        return customPreview;
    }

    @Override
    public String toString() {
        return "{" +
                "\"type\":\"" + type + "\"," +
                "\"subtype\":\"" + subtype + "\"," +
                "\"classname\":\"" + className + "\"," +
                "\"description\":\"" + description + "\"," +
                "\"objectId\":\"" + objectId + "\"," +
                "\"value\":" + value + "\"," +
                "\"unserializableValue\":" + unserializableValue +
                "}";
    }
}
