package org.punch.devtools.protocol.domstorage;

public class StorageIdBean {

    private String securityOrigin;
    private boolean isLocalStorage;

    public static DOMStorage.StorageId of(String securityOrigin, boolean isLocalStorage) {
        DOMStorage.StorageId storageId = new DOMStorage.StorageId();
        storageId.setSecurityOrigin(securityOrigin);
        storageId.setLocalStorage(isLocalStorage);
        return storageId;
    }

    StorageIdBean() {
    }

    public String getSecurityOrigin() {
        return securityOrigin;
    }

    public boolean getIsLocalStorage() {
        return isLocalStorage;
    }

    protected void setSecurityOrigin(String securityOrigin) {
        this.securityOrigin = securityOrigin;
    }

    protected void setLocalStorage(boolean isLocalStorage) {
        this.isLocalStorage = isLocalStorage;
    }

    @Override
    public String toString() {
        return "{" +
                "\"securityOrigin\":\"" + securityOrigin + "\"," +
                "\"isLocalStorage\":" + isLocalStorage +
                "}";
    }
}
