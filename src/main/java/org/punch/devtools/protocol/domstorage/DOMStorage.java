package org.punch.devtools.protocol.domstorage;

import org.punch.devtools.interfaces.ProtocolEvent;
import org.punch.devtools.protocol.annotations.*;

import java.util.List;

@Experimental
public interface DOMStorage {

    void clear(@Param("storageId") String storageId);

    void disable();

    void enable();

    @Result(key = "entries", type = List.class)
    List<List<String>> getDOMStorageItems(@Param("storageId") StorageId storageId);

    void removeDOMStorageItem(@Param("storageId") StorageId storageId, @Param("key") String key);

    @ProtocolType
    @ObjectParam
    class StorageId extends StorageIdBean {
    }

    enum Event implements ProtocolEvent {
        domStorageItemAdded, domStorageItemRemoved, domStorageItemsCleared, domStorageItemUpdated;

        @Override
        public String method() {
            return DOMStorage.class.getSimpleName() + "." + toString();
        }
    }
}
