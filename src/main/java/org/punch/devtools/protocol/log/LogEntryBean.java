/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.log;

import org.punch.devtools.enums.log.LogEntryLevel;
import org.punch.devtools.enums.log.LogEntrySource;
import org.punch.devtools.protocol.runtime.Runtime;

import java.util.List;

public class LogEntryBean {

    private LogEntrySource source;
    private LogEntryLevel level;
    private String text;
    private Number timestamp;
    private String url;
    private Integer lineNumber;
    private Runtime.StackTrace stackTrace;
    private String networkRequestId;
    private String workerId;
    private List<Runtime.RemoteObject> args;

    LogEntryBean() {
    }

    public LogEntrySource getSource() {
        return source;
    }

    public LogEntryLevel getLevel() {
        return level;
    }

    public String getText() {
        return text;
    }

    public Number getTimestamp() {
        return timestamp;
    }

    public String getUrl() {
        return url;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public Runtime.StackTrace getStackTrace() {
        return stackTrace;
    }

    public String getNetworkRequestId() {
        return networkRequestId;
    }

    public String getWorkerId() {
        return workerId;
    }

    public List<Runtime.RemoteObject> getArgs() {
        return args;
    }
}