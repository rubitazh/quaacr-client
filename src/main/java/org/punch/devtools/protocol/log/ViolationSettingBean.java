package org.punch.devtools.protocol.log;

public class ViolationSettingBean {

    private String name;
    private int threshold;

    public static Log.ViolationSetting of(String name, int threshold) {
        Log.ViolationSetting setting = new Log.ViolationSetting();
        setting.setName(name);
        setting.setThreshold(threshold);
        return setting;
    }

    ViolationSettingBean() {
    }

    public String getName() {
        return name;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }
}
