/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.log;

import org.punch.devtools.interfaces.ProtocolEvent;
import org.punch.devtools.protocol.annotations.ProtocolType;
import org.punch.devtools.protocol.annotations.Param;

import java.util.List;

public interface Log {

    void enable();

    void disable();

    void clear();

    void startViolationsReport(@Param("config") List<ViolationSetting> config);

    void stopViolationsReport();

    @ProtocolType
    class LogEntry extends LogEntryBean {
    }

    @ProtocolType
    class ViolationSetting extends ViolationSettingBean {
    }

    enum Event implements ProtocolEvent {
        entryAdded;

        public String method() {
            return Log.class.getSimpleName() + "." + toString();
        }
    }
}
