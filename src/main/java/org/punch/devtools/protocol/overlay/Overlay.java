/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.overlay;

import org.punch.devtools.protocol.annotations.Param;

public interface Overlay {

    void enable();

    void disable();

    void setPausedInDebuggerMessage();

    void setShowViewportSizeOnResize(@Param("show") boolean show);
}
