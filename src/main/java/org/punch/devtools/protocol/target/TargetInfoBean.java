/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.target;

public class TargetInfoBean {

    private String targetId;
    private String type;
    private String title;
    private String url;
    private boolean attached;
    private String openerId;
    private boolean canAccessOpener;
    private String openerFrameId;
    private String browserContextId;

    TargetInfoBean() {
    }

    public String getTargetId() {
        return targetId;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public boolean isAttached() {
        return attached;
    }

    public String getOpenerId() {
        return openerId;
    }

    public boolean isCanAccessOpener() {
        return canAccessOpener;
    }

    public String getOpenerFrameId() {
        return openerFrameId;
    }

    public String getBrowserContextId() {
        return browserContextId;
    }
}
