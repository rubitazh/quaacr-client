/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.target;

import org.punch.devtools.interfaces.ProtocolEvent;
import org.punch.devtools.protocol.annotations.ProtocolType;
import org.punch.devtools.protocol.annotations.Experimental;
import org.punch.devtools.protocol.annotations.Param;
import org.punch.devtools.protocol.annotations.Result;

import java.util.List;

public interface Target {

    @Result(key = "sessionId")
    String attachToTarget(@Param("targetId") String targetId, @Param("flatten") Boolean flatten);

    @Result(key = "success")
    boolean closeTarget(@Param("targetId") String targetId);

    @Result(key = "browserContextId")
    String createBrowserContext(@Param("disposeOnDetach") Boolean disposeOnDetach,
                                @Param("proxyServer") String proxyServer,
                                @Param("proxyBypassList") String proxyBypassList);

    @Result(key = "targetId")
    String createTarget(@Param("url") String url,
                        @Param("width") Integer width,
                        @Param("height") Integer height,
                        @Param("browserContextId") String browserContextId,
                        @Param("enableBeginFrameControl") Boolean enableBeginFrameControl,
                        @Param("newWindow") Boolean newWindow,
                        @Param("background") Boolean background);

    @Experimental
    @Result(key = "targetInfo")
    TargetInfo getTargetInfo(@Param("targetId") String targetId);

    @Result(key = "targetInfos", type = TargetInfo.class)
    List<TargetInfo> getTargets();

    void setDiscoverTargets(@Param("discover") boolean discover);

    @ProtocolType
    class TargetInfo extends TargetInfoBean {
    }

    enum Event implements ProtocolEvent {
        attachedToTarget, targetInfoChanged, targetDestroyed, targetCreated;

        @Override
        public String method() {
            return Target.class.getSimpleName() + "." + toString();
        }
    }
}
