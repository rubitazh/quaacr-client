/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.browser;

public class TargetWindowBean {

    private int windowId;
    private Browser.Bounds bounds;

    TargetWindowBean() {
    }

    public int getWindowId() {
        return windowId;
    }

    public Browser.Bounds getBounds() {
        return bounds;
    }
}
