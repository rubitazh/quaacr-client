/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.browser;

import org.punch.devtools.enums.browser.BrowserPermission;

public class PermissionDescriptorBean {

    private String name;
    private Boolean sysex;
    private Boolean userVisibleOnly;
    private Boolean allowWithoutSanitization;

    public static Browser.PermissionDescriptor of(BrowserPermission permission, boolean allowWithoutSanitization) {
        Browser.PermissionDescriptor descriptor = new Browser.PermissionDescriptor();
        descriptor.setName(permission.value());
        descriptor.setAllowWithoutSanitization(allowWithoutSanitization);
        return descriptor;
    }

    PermissionDescriptorBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSysex() {
        return sysex;
    }

    void setSysex(Boolean sysex) {
        this.sysex = sysex;
    }

    public Boolean getUserVisibleOnly() {
        return userVisibleOnly;
    }

    void setUserVisibleOnly(Boolean userVisibleOnly) {
        this.userVisibleOnly = userVisibleOnly;
    }

    public Boolean getAllowWithoutSanitization() {
        return allowWithoutSanitization;
    }

    void setAllowWithoutSanitization(Boolean allowWithoutSanitization) {
        this.allowWithoutSanitization = allowWithoutSanitization;
    }
}
