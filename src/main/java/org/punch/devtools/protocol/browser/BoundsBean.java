/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.browser;

import org.punch.devtools.protocol.browser.Browser;

public class BoundsBean {

    private int left;
    private int top;
    private int width;
    private int height;
    private Browser.WindowState windowState;

    public int getLeft() {
        return left;
    }

    public int getTop() {
        return top;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Browser.WindowState getWindowState() {
        return windowState;
    }
}
