/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.browser;

public class BrowserVersionBean {

    private String protocolVersion;
    private String product;
    private String revision;
    private String userAgent;
    private String jsVersion;

    BrowserVersionBean() {
    }

    public String getProtocolVersion() {
        return protocolVersion;
    }

    public String getProduct() {
        return product;
    }

    public String getRevision() {
        return revision;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public String getJsVersion() {
        return jsVersion;
    }
}
