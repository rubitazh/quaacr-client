/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.browser;

import org.punch.devtools.enums.browser.PermissionSetting;
import org.punch.devtools.protocol.annotations.*;

import java.util.List;

public interface Browser {

    @Result
    BrowserVersion getVersion();

    @Experimental
    @Result(key = "arguments", type = String.class)
    List<String> getBrowserCommandLine();

    @Experimental
    @Result
    TargetWindow getWindowForTarget(@Param("targetId") String targetId);

    @Experimental
    void setDownloadBehavior(@Param("behavior") String behavior,
                             @Param("downloadPath") String downloadPath,
                             @Param("browserContextID") String browserContextID);

    @Experimental
    void setPermission(@Param("permission") PermissionDescriptor permission,
                       @Param("setting") PermissionSetting setting,
                       @Param("origin") String origin,
                       @Param("browserContextId") String browserContextId);

    @ProtocolType
    class Bounds extends BoundsBean {
    }

    @ProtocolType
    @ObjectParam
    class PermissionDescriptor extends PermissionDescriptorBean {
    }

    @CustomType
    class BrowserVersion extends BrowserVersionBean {
    }

    @CustomType
    class TargetWindow extends TargetWindowBean {
    }

    @ProtocolType
    enum WindowState {
        normal, minimized, maximized, fullscreen
    }
}
