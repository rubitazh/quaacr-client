/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.css;

import org.punch.devtools.protocol.annotations.Param;
import org.punch.devtools.protocol.annotations.Result;
import org.punch.devtools.protocol.css.CSSComputedStylePropertyBean;

import java.util.List;

public interface CSS {

    void disable();

    void enable();

    @Result(key = "computedStyle", type = CSSComputedStyleProperty.class)
    List<CSSComputedStyleProperty> getComputedStyleForNode(@Param("nodeId") int nodeId);

    class CSSComputedStyleProperty extends CSSComputedStylePropertyBean {
    }
}
