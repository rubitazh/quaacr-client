/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.emulation;

import org.punch.devtools.enums.emulation.ScreenOrientationType;

/**
 * Represents {@link Emulation.ScreenOrientation} object.
 */
public class ScreenOrientationBean {

    private String type;
    private int angle;

    /**
     * Returns unmodifiable ScreenOrientation instance.
     *
     * @param type  orientation type.
     * @param angle orientation angle.
     */
    public static Emulation.ScreenOrientation of(ScreenOrientationType type, int angle) {
        Emulation.ScreenOrientation screenOrientation = new Emulation.ScreenOrientation();
        screenOrientation.setType(type.toString());
        screenOrientation.setAngle(angle);
        return screenOrientation;
    }

    ScreenOrientationBean() {
    }

    public String getType() {
        return type;
    }

    void setType(String type) {
        this.type = type;
    }

    public int getAngle() {
        return angle;
    }

    void setAngle(int angle) {
        this.angle = angle;
    }
}
