/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.emulation;

/**
 * Represents {@link Emulation.MediaFeature} object.
 */
public class MediaFeatureBean {

    private String name;
    private String value;

    /**
     * Returns unmodifiable MediaFeature instance.
     *
     * @param name  media feature name.
     * @param value media feature value.
     */
    public static Emulation.MediaFeature of(String name, String value) {
        Emulation.MediaFeature mediaFeature = new Emulation.MediaFeature();
        mediaFeature.setName(name);
        mediaFeature.setValue(value);
        return mediaFeature;
    }

    MediaFeatureBean() {
    }

    public String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    void setValue(String value) {
        this.value = value;
    }
}
