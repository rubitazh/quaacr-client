/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.emulation;

/**
 * Represents {@link Emulation.UserAgentBrandVersion} object. Used to specify User Agent Client Hints
 * to emulate. See <a href='https://wicg.github.io/ua-client-hints'>ua-client-hints</a>.
 */
public class UserAgentBrandVersionBean {

    private String brand;
    private String version;

    /**
     * Returns unmodifiable UserAgentBrandVersion instance.
     *
     * @param brand   user agent brand.
     * @param version user agent type.
     */
    public static Emulation.UserAgentBrandVersion of(String brand, String version) {
        Emulation.UserAgentBrandVersion userAgentBrandVersion = new Emulation.UserAgentBrandVersion();
        userAgentBrandVersion.setBrand(brand);
        userAgentBrandVersion.setVersion(version);
        return userAgentBrandVersion;
    }

    UserAgentBrandVersionBean() {
    }

    public String getBrand() {
        return brand;
    }

    void setBrand(String brand) {
        this.brand = brand;
    }

    public String getVersion() {
        return version;
    }

    void setVersion(String version) {
        this.version = version;
    }
}
