/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.emulation;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

/**
 * Represents {@link Emulation.UserAgentMetadata} object. Used to specify User Agent Client Hints to emulate.
 * See <a href='https://wicg.github.io/ua-client-hints'>ua-client-hints</a>. Missing optional values will be
 * filled in by the target with what it would normally use.
 */
public class UserAgentMetadataBean {

    private String platform;
    private String platformVersion;
    private String architecture;
    private String model;
    private boolean mobile;
    private List<Emulation.UserAgentBrandVersion> brands;
    private String fullVersion;

    /**
     * Returns unmodifiable UserAgentMetadata instance.
     *
     * @param platform        metadata platform.
     * @param platformVersion metadata platform version.
     * @param architecture    metadata architecture.
     * @param model           metadata model.
     * @param mobile          acts as mobile metadata if {@code true}.
     * @param brands          metadata brands.
     * @param fullVersion     metadata full version.
     */
    public static Emulation.UserAgentMetadata of(@NotNull String platform,
                                                 @NotNull String platformVersion,
                                                 @NotNull String architecture,
                                                 @NotNull String model,
                                                 boolean mobile,
                                                 List<Emulation.UserAgentBrandVersion> brands,
                                                 String fullVersion) {

        Emulation.UserAgentMetadata userAgentMetadata = new Emulation.UserAgentMetadata();
        userAgentMetadata.setPlatform(platform);
        userAgentMetadata.setPlatformVersion(platformVersion);
        userAgentMetadata.setArchitecture(architecture);
        userAgentMetadata.setModel(model);
        userAgentMetadata.setMobile(mobile);
        if (Objects.nonNull(brands)) userAgentMetadata.setBrands(brands);
        if (Objects.nonNull(fullVersion)) userAgentMetadata.setFullVersion(fullVersion);

        return userAgentMetadata;
    }

    UserAgentMetadataBean() {
    }

    public List<Emulation.UserAgentBrandVersion> getBrands() {
        return brands;
    }

    public void setBrands(List<Emulation.UserAgentBrandVersion> brands) {
        this.brands = brands;
    }

    public String getFullVersion() {
        return fullVersion;
    }

    public void setFullVersion(String fullVersion) {
        this.fullVersion = fullVersion;
    }

    public String getPlatform() {
        return platform;
    }

    void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    void setPlatformVersion(String platformVersion) {
        this.platformVersion = platformVersion;
    }

    public String getArchitecture() {
        return architecture;
    }

    void setArchitecture(String architecture) {
        this.architecture = architecture;
    }

    public String getModel() {
        return model;
    }

    void setModel(String model) {
        this.model = model;
    }

    public boolean isMobile() {
        return mobile;
    }

    public void setMobile(boolean mobile) {
        this.mobile = mobile;
    }
}
