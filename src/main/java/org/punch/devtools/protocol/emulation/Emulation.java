/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.emulation;

import org.punch.devtools.enums.emulation.MouseTouchEventsConfig;
import org.punch.devtools.protocol.annotations.*;

public interface Emulation {

    @Result
    boolean canEmulate();

    void clearDeviceMetricsOverride();

    void clearGeolocationOverride();

    void setDeviceMetricsOverride(@Param("width") int width,
                                  @Param("height") int height,
                                  @Param("deviceScaleFactor") Number deviceScaleFactor,
                                  @Param("mobile") boolean mobile,
                                  @Param("scale") Number scale,
                                  @Param("screenWidth") Integer screenWidth,
                                  @Param("screenHeight") Integer screenHeight,
                                  @Param("positionX") Integer positionX,
                                  @Param("positionY") Integer positionY,
                                  @Param("dontSetVisibleSize") Boolean dontSetVisibleSize,
                                  @Param("screenOrientation") ScreenOrientation screenOrientation,
                                  @Param("displayFeature") DisplayFeature displayFeature);

    void setEmulatedMedia(@Param("media") String media, @Param("features") MediaFeature features);

    void setGeolocationOverride(@Param("latitude") Number latitude,
                                @Param("longitude") Number longitude,
                                @Param("accuracy") Number accuracy);

    void setScriptExecutionDisabled(@Param("value") boolean value);

    void setTouchEmulationEnabled(@Param("enabled") boolean enabled, @Param("maxTouchPoints") Integer maxTouchPoints);

    void setUserAgentOverride(@Param("userAgent") String userAgent,
                              @Param("acceptLanguage") String acceptLanguage,
                              @Param("platform") String platform,
                              @Param("userAgentMetadata") UserAgentMetadata userAgentMetadata);

    @Experimental
    void resetPageScaleFactor();

    @Experimental
    void setCPUThrottlingRate(@Param("rate") double rate);

    @Experimental
    void setDocumentCookieDisabled(@Param("disabled") boolean disabled);

    @Experimental
    void setEmitTouchEventsForMouse(@Param("enabled") boolean enabled,
                                    @Param("configuration") MouseTouchEventsConfig configuration);

    @Experimental
    void setEmulatedVisionDeficiency(@Param("type") String type); //TODO: add enum

    @Experimental
    void setFocusEmulationEnabled(@Param("enabled") boolean enabled);

    @Experimental
    void setIdleOverride(@Param("isUserActive") boolean isUserActive,
                         @Param("isScreenUnlocked") boolean isScreenUnlocked);

    @Experimental
    void setLocaleOverride(@Param("locale") String locale);

    @Experimental
    void setPageScaleFactor(@Param("pageScaleFactor") double pageScaleFactor);

    @Experimental
    void setScrollbarsHidden(@Param("hidden") boolean hidden);

    @Experimental
    void setTimezoneOverride(@Param("timezoneId") String timezoneId);

    @ProtocolType
    class DisplayFeature extends DisplayFeatureBean {
    }

    @ProtocolType
    class MediaFeature extends MediaFeatureBean {
    }

    @ObjectParam
    @ProtocolType
    class ScreenOrientation extends ScreenOrientationBean {
    }

    @ObjectParam
    @ProtocolType
    class UserAgentBrandVersion extends UserAgentBrandVersionBean {
    }

    @ObjectParam
    @ProtocolType
    class UserAgentMetadata extends UserAgentMetadataBean {
    }
}