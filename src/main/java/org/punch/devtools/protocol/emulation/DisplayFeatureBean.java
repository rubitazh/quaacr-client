/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.emulation;

/**
 * Represents {@link Emulation.DisplayFeature} object.
 */
public class DisplayFeatureBean {

    private String orientation;
    private int offset;
    private int maskLength;

    /**
     * Returns {@link Emulation.DisplayFeature} unmodifiable instance.
     *
     * @param orientation Orientation of a display feature in relation to screen.
     * @param offset      The offset from the screen origin in either the x -
     *                    vertical orientation, or y - horizontal orientation.
     * @param maskLength  A display feature may mask content such that it is not
     *                    physically displayed - this length along with the offset
     *                    describes this area. A display feature that only splits
     *                    content will have a 0 mask_length.
     */
    public static Emulation.DisplayFeature of(String orientation, int offset, int maskLength) {
        Emulation.DisplayFeature displayFeature = new Emulation.DisplayFeature();
        displayFeature.setOrientation(orientation);
        displayFeature.setOffset(offset);
        displayFeature.setMaskLength(maskLength);
        return displayFeature;
    }

    DisplayFeatureBean() {
    }

    public String getOrientation() {
        return orientation;
    }

    void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public int getOffset() {
        return offset;
    }

    void setOffset(int offset) {
        this.offset = offset;
    }

    public int getMaskLength() {
        return maskLength;
    }

    void setMaskLength(int maskLength) {
        this.maskLength = maskLength;
    }
}
