package org.punch.devtools.protocol.page;

import org.punch.devtools.protocol.dom.DOM;

public class LayoutMetricsBean {

    private Page.LayoutViewport cssLayoutViewport;
    private Page.VisualViewport cssVisualViewport;
    private DOM.Rect cssContentSize;

    /* Deprecated */

    private Page.LayoutViewport layoutViewport;
    private Page.VisualViewport visualViewport;
    private DOM.Rect contentSize;

    LayoutMetricsBean() {
    }

    public Page.LayoutViewport getCssLayoutViewport() {
        return cssLayoutViewport;
    }

    public Page.VisualViewport getCssVisualViewport() {
        return cssVisualViewport;
    }

    public DOM.Rect getCssContentSize() {
        return cssContentSize;
    }

    public Page.LayoutViewport getLayoutViewport() {
        return layoutViewport;
    }

    public Page.VisualViewport getVisualViewport() {
        return visualViewport;
    }

    public DOM.Rect getContentSize() {
        return contentSize;
    }
}
