package org.punch.devtools.protocol.page;

public class LayoutViewportBean {

    private int pageX;
    private int pageY;
    private int clientWidth;
    private int clientHeight;

    LayoutViewportBean() {
    }

    public int getPageX() {
        return pageX;
    }

    public int getPageY() {
        return pageY;
    }

    public int getClientWidth() {
        return clientWidth;
    }

    public int getClientHeight() {
        return clientHeight;
    }
}
