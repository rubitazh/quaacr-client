/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.page;

import org.punch.devtools.enums.page.GatedAPIFeatures;

import java.util.List;

public class FrameBean {

    private String id;
    private String parentId;
    private String loaderId;
    private String name;
    private String url;
    private String urlFragment;
    private String domainAndRegistry;
    private String securityOrigin;
    private String mimeType;
    private String unreachableUrl;
    private String adFrameType;
    private String secureContextType;
    private String crossOriginIsolatedContextType;
    private List<GatedAPIFeatures> gatedAPIFeatures;
    private List<Page.OriginTrial> originTrials;
    private Object adFrameStatus;

    FrameBean() {
    }

    public String getId() {
        return id;
    }

    public String getParentId() {
        return parentId;
    }

    public String getLoaderId() {
        return loaderId;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getUrlFragment() {
        return urlFragment;
    }

    public String getDomainAndRegistry() {
        return domainAndRegistry;
    }

    public String getSecurityOrigin() {
        return securityOrigin;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getUnreachableUrl() {
        return unreachableUrl;
    }

    public String getAdFrameType() {
        return adFrameType;
    }

    public String getSecureContextType() {
        return secureContextType;
    }

    public String getCrossOriginIsolatedContextType() {
        return crossOriginIsolatedContextType;
    }

    public List<GatedAPIFeatures> getGatedAPIFeatures() {
        return gatedAPIFeatures;
    }

    public List<Page.OriginTrial> getOriginTrials() {
        return originTrials;
    }

    public Object getAdFrameStatus() {
        return adFrameStatus;
    }
}
