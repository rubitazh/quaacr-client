/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.page;

import org.punch.devtools.enums.page.OriginTrialUsageRestriction;

public class OriginTrialTokenBean {

    private String origin;
    private boolean matchSubDomains;
    private String trialName;
    private Number expiryTime;
    private boolean isThirdParty;
    private OriginTrialUsageRestriction usageRestriction;

    OriginTrialTokenBean() {
    }

    public String getOrigin() {
        return origin;
    }

    public boolean isMatchSubDomains() {
        return matchSubDomains;
    }

    public String getTrialName() {
        return trialName;
    }

    public Number getExpiryTime() {
        return expiryTime;
    }

    public boolean getIsThirdParty() {
        return isThirdParty;
    }

    public OriginTrialUsageRestriction getUsageRestriction() {
        return usageRestriction;
    }
}
