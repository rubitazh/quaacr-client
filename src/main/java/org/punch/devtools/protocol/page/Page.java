/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.page;

import org.punch.devtools.enums.page.ImageFormat;
import org.punch.devtools.enums.page.ReferrerPolicy;
import org.punch.devtools.enums.page.TransitionType;
import org.punch.devtools.interfaces.ProtocolEvent;
import org.punch.devtools.protocol.annotations.*;

public interface Page {

    void bringToFront();

    @Result(key = "data")
    String captureScreenshot(@Param("format") ImageFormat format,
                             @Param("quality") Integer quality,
                             @Param("clip") Page.Viewport clip,
                             @Param("fromSurface") Boolean fromSurface);

    void disable();

    void enable();

    @Result(key = "frameTree")
    FrameTree getFrameTree();

    @Result
    LayoutMetrics getLayoutMetrics();

    @Result
    NavigationHistory getNavigationHistory();

    void handleJavaScriptDialog(@Param("accept") boolean accept, @Param("promptText") String promptText);

    @Result
    NavFrame navigate(@Param("url") String url,
                      @Param("referrer") String referrer,
                      @Param("transitionType") TransitionType transitionType,
                      @Param("frameId") String frameId,
                      @Param("referrerPolicy")ReferrerPolicy referrerPolicy);

    @Result(key = "stream")
    String printToPDF(@Param("landscape") Boolean landscape,
                      @Param("displayHeaderFooter") Boolean displayHeaderFooter,
                      @Param("printBackground") Boolean printBackground,
                      @Param("scale") Number scale,
                      @Param("paperWidth") Number paperWidth,
                      @Param("paperHeight") Number paperHeight,
                      @Param("marginTop") Number marginTop,
                      @Param("marginBottom") Number marginBottom,
                      @Param("marginLeft") Number marginLeft,
                      @Param("marginRight") Number marginRight,
                      @Param("pageRanges") String pageRanges,
                      @Param("ignoreInvalidPageRanges") Boolean ignoreInvalidPageRanges,
                      @Param("headerTemplate") String headerTemplate,
                      @Param("footerTemplate") String footerTemplate,
                      @Param("preferCSSPageSize") Boolean preferCSSPageSize,
                      @Param("transferMode") String transferMode);

    void reload(@Param("ignoreCache") Boolean ignoreCache,
                @Param("scriptToEvaluateOnLoad") String scriptToEvaluateOnLoad);

    @Experimental
    void setBypassCSP(@Param("enabled") boolean enabled);

    void setDocumentContent(@Param("frameId") String frameId, @Param("html") String html);

    @Experimental
    void setLifecycleEventsEnabled(@Param("enabled") boolean enabled);

    @Experimental
    void setWebLifecycleState(@Param("state") String state);

    @CustomType
    class LayoutMetrics extends LayoutMetricsBean {
    }

    @CustomType
    class LayoutViewport extends LayoutViewportBean {
    }

    @CustomType
    class NavFrame extends NavFrameBean {
    }

    @CustomType
    class NavigationHistory extends NavigationHistoryBean {
    }

    @CustomType
    class VisualViewport extends VisualViewportBean {
    }

    @ProtocolType
    class Frame extends FrameBean {
    }

    @ProtocolType
    class FrameTree extends FrameTreeBean {
    }

    @ProtocolType
    class NavigationEntry extends NavigationEntryBean {
    }

    @ProtocolType
    @ObjectParam
    class Viewport extends ViewportBean {
    }

    @ProtocolType
    class OriginTrial extends OriginTrialBean {
    }

    @ProtocolType
    class OriginTrialToken extends OriginTrialTokenBean {
    }

    @ProtocolType
    class OriginTrialTokenWithStatus extends OriginTrialTokenWithStatusBean {
    }

    enum Event implements ProtocolEvent {
        domContentEventFired, downloadProgress, downloadWillBegin, frameNavigated, javascriptDialogClosed,
        javascriptDialogOpening, lifecycleEvent, windowOpen, frameStartedLoading, frameStoppedLoading,
        navigatedWithinDocument;

        public String method() {
            return Page.class.getSimpleName() + "." + toString();
        }
    }
}
