/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.page;

import org.punch.devtools.enums.page.TransitionType;

public class NavigationEntryBean {

    private int id;
    private String url;
    private String userTypedURL;
    private String title;
    private TransitionType transitionType;

    NavigationEntryBean() {
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getUserTypedURL() {
        return userTypedURL;
    }

    public String getTitle() {
        return title;
    }

    public TransitionType getTransitionType() {
        return transitionType;
    }
}
