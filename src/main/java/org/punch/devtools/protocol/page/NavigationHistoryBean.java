/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.page;

import java.util.List;

public class NavigationHistoryBean {

    private int currentIndex;
    private List<Page.NavigationEntry> entries;

    NavigationHistoryBean() {
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public List<Page.NavigationEntry> getEntries() {
        return entries;
    }
}
