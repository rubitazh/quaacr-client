/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.page;

public class NavFrameBean {

    private String frameId;
    private String loaderId;

    NavFrameBean() {
    }

    public String getFrameId() {
        return frameId;
    }

    public String getLoaderId() {
        return loaderId;
    }
}
