/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.page;

import java.util.List;

public class FrameTreeBean {

    private Page.Frame frame;
    private List<Page.FrameTree> childFrames;

    FrameTreeBean() {
    }

    public Page.Frame getFrame() {
        return frame;
    }

    public List<Page.FrameTree> getChildFrames() {
        return childFrames;
    }
}
