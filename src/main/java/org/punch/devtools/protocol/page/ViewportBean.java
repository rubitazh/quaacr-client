/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.page;

import org.punch.devtools.protocol.page.Page;

public class ViewportBean {

    private Number x;
    private Number y;
    private Number width;
    private Number height;
    private Number scale;

    public static Page.Viewport of(Number x, Number y, Number width, Number height, Number scale) {
        var viewport = new Page.Viewport();
        viewport.setX(x);
        viewport.setY(y);
        viewport.setWidth(width);
        viewport.setHeight(height);
        viewport.setScale(scale);
        return viewport;
    }

    public Number getX() {
        return x;
    }

    public Number getY() {
        return y;
    }

    public Number getWidth() {
        return width;
    }

    public Number getHeight() {
        return height;
    }

    public Number getScale() {
        return scale;
    }

    public void setX(Number x) {
        this.x = x;
    }

    public void setY(Number y) {
        this.y = y;
    }

    public void setWidth(Number width) {
        this.width = width;
    }

    public void setHeight(Number height) {
        this.height = height;
    }

    public void setScale(Number scale) {
        this.scale = scale;
    }
}
