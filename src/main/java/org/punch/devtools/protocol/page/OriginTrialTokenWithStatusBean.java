/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.page;

import org.punch.devtools.enums.page.OriginTrialTokenStatus;

public class OriginTrialTokenWithStatusBean {

    private String rawTokenText;
    private Page.OriginTrialToken parsedToken;
    private OriginTrialTokenStatus status;

    OriginTrialTokenWithStatusBean() {
    }

    public String getRawTokenText() {
        return rawTokenText;
    }

    public Page.OriginTrialToken getParsedToken() {
        return parsedToken;
    }

    public OriginTrialTokenStatus getStatus() {
        return status;
    }
}
