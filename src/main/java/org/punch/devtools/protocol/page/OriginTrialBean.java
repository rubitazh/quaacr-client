/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.protocol.page;

import org.punch.devtools.enums.page.OriginTrialStatus;

import java.util.List;

public class OriginTrialBean {

    private String trialName;
    private OriginTrialStatus status;
    private List<Page.OriginTrialTokenWithStatus> tokensWithStatus;

    OriginTrialBean() {
    }

    public String getTrialName() {
        return trialName;
    }

    public OriginTrialStatus getStatus() {
        return status;
    }

    public List<Page.OriginTrialTokenWithStatus> getTokensWithStatus() {
        return tokensWithStatus;
    }
}
