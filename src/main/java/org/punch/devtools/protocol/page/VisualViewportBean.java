package org.punch.devtools.protocol.page;

public class VisualViewportBean {

    private Number offsetX;
    private Number offsetY;
    private Number pageX;
    private Number pageY;
    private Number clientWidth;
    private Number clientHeight;
    private Number scale;
    private Number zoom;

    public Number getOffsetX() {
        return offsetX;
    }

    public Number getOffsetY() {
        return offsetY;
    }

    public Number getPageX() {
        return pageX;
    }

    public Number getPageY() {
        return pageY;
    }

    public Number getClientWidth() {
        return clientWidth;
    }

    public Number getClientHeight() {
        return clientHeight;
    }

    public Number getScale() {
        return scale;
    }

    public Number getZoom() {
        return zoom;
    }
}
