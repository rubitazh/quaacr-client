/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.interfaces;

import org.json.JSONObject;

import java.lang.reflect.Method;

public interface JsonConverter {

    Object convert(Method mtd, JSONObject response);

    <T> T toBean(String obj, Class<T> cls);
}
