/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.interfaces;

/**
 * Responsible for Chromium based browser DevTools session management.
 */
public interface ChromeSession {

    /**
     * Returns browser session id.
     */
    default String getId() {
        return getWsUrl().substring(getWsUrl().lastIndexOf("/") + 1);
    }

    /**
     * Returns browser websocket url.
     */
    String getWsUrl();

    /**
     * Kills browser session.
     */
    void destroy();
}
