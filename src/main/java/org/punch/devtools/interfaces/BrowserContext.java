/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.interfaces;

import org.punch.context.entities.Session;
import org.punch.context.entities.Tab;
import org.punch.context.enums.TargetType;
import org.punch.devtools.enums.browser.DownloadBehavior;
import org.punch.devtools.protocol.target.Target;

import java.util.List;

/**
 * A set of generic actions for a web browser to be executed from a user perspective. It's intended to represent
 * something which a user can do when working with a single web browser process: add/close a tab, apply browser
 * specific settings etc.
 */
public interface BrowserContext {

    /**
     * Opens new tab in a browser.
     *
     * @return {@link Tab}.
     */
    Tab addTab();

    /**
     * Closes specified target.
     *
     * @param targetId {@link Session} targetId (frame).
     * @return true if closed successfully and false in all other cases.
     */
    boolean closeTarget(String targetId);

    /**
     * Must return the very first tab of a new web browser session.
     *
     * @return initial browser tab.
     */
    Tab defaultTab();

    /**
     * Returns {@link Target.TargetInfo} of requested target.
     *
     * @param targetId targetId (frame).
     * @return POJO for
     * <a href="https://chromedevtools.github.io/devtools-protocol/tot/Target/#type-TargetInfo">Target Info</a>
     */
    Target.TargetInfo getTargetInfo(String targetId);

    /**
     * Returns a list of IDs for all currently active targets.
     *
     * @param targetType {@link TargetType}.
     * @return list of targetIds filtered by type.
     */
    List<String> getTargetsByType(TargetType targetType);

    /**
     * Defines the policy of downloads.
     *
     * @param behavior allowed download behavior policy name.
     * @param path     where files are downloaded to.
     * @see DownloadBehavior
     */
    void setDownloadBehavior(DownloadBehavior behavior, String path);

    /**
     * Switches to other frame (target) by targetId.
     *
     * @param targetId browser's frame id.
     * @param cls      {@link Session} type.
     * @return frame session.
     * @throws RuntimeException if target type doesn't match {@link Session} type instance.
     */
    <T extends Session> T switchToTarget(String targetId, Class<T> cls);

    /**
     * Closes browser session with all its frames (or targets).
     */
    void terminate();
}
