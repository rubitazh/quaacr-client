/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.interfaces;

import org.json.JSONObject;
import org.punch.context.ContextConfig;
import org.punch.devtools.ex.PollEventException;

import java.util.List;
import java.util.function.Function;

/**
 * Handles events received from {@link DevToolsClient}. General implementation suggests that events are stored
 * as a hashtable of queues, identified by {@link ProtocolEvent} keys, and it must follow the workflow below:
 * <ul>
 *     <li>if required event is pulled, then it must be returned to a client and removed from queue;</li>
 *     <li>pulled event must be returned to the end of the queue in all other cases;</li>
 *     <li>size of a queue should be limited by a reasonable length value.</li>
 * </ul>
 * Event removal is required to mimic basic interface between a user and a web-browser, as once an action is
 * executed by a user, there is no way to return to the same web-browser state before the action was executed.
 * Yes, a user can do 'backwards' step, but the web-page state will be different.
 */
public interface EventConsumer {

    /**
     * Clears all existing {@link ProtocolEvent} queues.
     */
    void clearAllQueues();

    /**
     * Puts received a event into corresponding queue.
     *
     * @param e fired event to be registered by {@link EventConsumer}
     */
    void consumeEvent(JSONObject e);

    /**
     * Retrieves an event defined by {@link ProtocolEvent} type and {@link Function} condition rule a required event
     * must satisfy. Rule accepts {@link JSONObject} event, returns {@code true} if condition is satisfied or returns
     * {@code false}, if condition is failed.
     * If event has not been fired yet, the method waits for a {@link ContextConfig#getAwaitEventTimeout()} timeout.
     * Once the timeout is over, the method throws {@link PollEventException} exception.
     *
     * @param pe   {@link ProtocolEvent} event type;
     * @param rule function that defines a set of conditions which an event must satisfy
     *             to be considered as the required one.
     * @return fired event.
     * @throws PollEventException if no event satisfies provided conditions.
     */
    JSONObject getFiredEvent(ProtocolEvent pe, Function<JSONObject, Boolean> rule) throws PollEventException;

    /**
     * Waits for an event defined by {@link ProtocolEvent} type and {@link Function} condition rule to be fired.
     * The only difference between this method and {@link this#getFiredEvent} that it doesn't return anything.
     * Rest of the functionality is the same.
     *
     * @param pe   {@link ProtocolEvent} event type;
     * @param rule function that defines a set of conditions which an event must satisfy
     *             to be considered as the required one.
     * @throws PollEventException if no event satisfies provided conditions.
     */
    void waitFiredEvent(ProtocolEvent pe, Function<JSONObject, Boolean> rule) throws PollEventException;

    /**
     * Polls all events from a corresponding queue defined by {@link ProtocolEvent} event type key.
     *
     * @return List of fired events.
     */
    List<JSONObject> pollAllEvents(ProtocolEvent pe);
}
