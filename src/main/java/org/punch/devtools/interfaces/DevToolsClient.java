/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.devtools.interfaces;

import org.json.JSONObject;
import org.punch.context.entities.Session;
import org.punch.devtools.impl.EventPrinter;

/**
 * This class manages DevTools Protocol messages flow. It initiates a connection with DevTools Protocol
 * messages dispatcher (simply speaking, a Blink engine based web-browser running with an opened DevTools
 * webSocket connection) to be able to send DevTools Protocol messages (a JSON object) from a client and
 * listen to the messages sent as a response by a web browser back to the client.
 */
public interface DevToolsClient {

    /**
     * Starts DevTools client. If a WebSocket connection is established,
     * method completes normally. Otherwise an exception must be thrown.
     *
     * @throws Exception on DevTools Protocol WebSocket connection failure.
     */
    void start() throws Exception;

    /**
     * Adds {@link EventConsumer} for the given {@link Session} id.
     *
     * @param sessionId {@link Session} id.
     * @param consumer  {@link EventConsumer} instance.
     */
    void addEventConsumer(String sessionId, EventConsumer consumer);

    /**
     * Send a synchronous DevTools Protocol request.
     *
     * @param id  message id.
     * @param req request message.
     * @return DevTools Protocol response.
     */
    JSONObject sendReq(int id, String req);

    /**
     * Prints DevTools Protocol response messages to a specified
     * output, which is set via {@link EventPrinter} instance.
     *
     * @param printer DevTools events printer.
     */
    void printProtocolEvents(EventPrinter printer);

    /**
     * Stops DevTools client, which means that DevTools Protocol
     * WebSocket connection must be terminated.
     *
     * @throws InterruptedException on terminate connection failure.
     */
    void stop() throws InterruptedException;
}
