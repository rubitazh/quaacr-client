/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context;

public class SessionParamKeys {

    public static final String NO_DEFAULT_OPTS = "noDefaultOpts";
    public static final String PROFILE_NAME = "profileName";
    public static final String PROFILE_PATH = "profilePath";
    public static final String SESSION_LOAD_TIMEOUT = "sessionLoadTimeout";
    public static final String WINDOW_SIZE = "windowSize";
    public static final String WINDOW_SIZE_RATIO = "windowSizeRatio";
}