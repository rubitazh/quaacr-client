/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.enums;

/**
 * <a href='https://chromedevtools.github.io/devtools-protocol/tot/Target/#type-TargetInfo'>TargetInfo</a>
 * enumerated types.
 */
public enum TargetType {
    BACKGROUND_PAGE("background_page"),
    IFRAME("iframe"),
    OTHER("other"),
    TAB("page"); //Because otherwise it clashes with org.punch.devtools.protocol.Page interface.

    private final String value;

    TargetType(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
