/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.enums;

/** Pre-defined emulated devices screen size in pixels. */
public enum DeviceScreenSize {
    IPHONE(414, 736), ANDROID(280, 653);

    private final int width;
    private final int height;

    DeviceScreenSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /** Returns emulated device screen width in pixels. */
    public int width() {
        return width;
    }

    /** Returns emulated device screen height in pixels. */
    public int height() {
        return height;
    }
}
