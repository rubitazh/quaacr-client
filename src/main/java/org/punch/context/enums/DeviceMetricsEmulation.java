/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.enums;

import org.punch.context.interfaces.DeviceMetrics;

import static org.punch.context.constants.UserAgentStrings.*;
import static org.punch.devtools.enums.emulation.ScreenOrientationType.portraitPrimary;
import static org.punch.devtools.protocol.emulation.Emulation.ScreenOrientation;

/** Pre-defined {@link DeviceMetrics} provided by Blink engine based web browsers. */
public enum DeviceMetricsEmulation implements DeviceMetrics {

    /** Emulates IPhone 6.7,8 Plus device. */
    IPHONE_6_7_8_PLUS(3, 1f, DeviceScreenSize.IPHONE, ScreenOrientation.of(portraitPrimary, 0), IPHONE_USER_AGENT),

    /** Emulates Android 8 device. */
    ANDROID_8(3, 1f, DeviceScreenSize.ANDROID, ScreenOrientation.of(portraitPrimary, 0), ANDROID_USER_AGENT);

    private int deviceScaleFactor;
    private float scale;
    private int screenWidth;
    private int screenHeight;
    private ScreenOrientation screenOrientation;
    private String userAgentString;

    /**
     * Device metrics emulation constructor.
     *
     * @param deviceScaleFactor CSS scale factor.
     * @param scale             screen scale factor.
     * @param screenSize        screen size in pixels.
     * @param screenOrientation screen orientation.
     * @param userAgentString   user agent.
     */
    DeviceMetricsEmulation(int deviceScaleFactor,
                           float scale,
                           DeviceScreenSize screenSize,
                           ScreenOrientation screenOrientation,
                           String userAgentString) {

        this.deviceScaleFactor = deviceScaleFactor;
        this.scale = scale;
        this.screenWidth = screenSize.width();
        this.screenHeight = screenSize.height();
        this.screenOrientation = screenOrientation;
        this.userAgentString = userAgentString;
    }

    @Override
    public int deviceScaleFactor() {
        return deviceScaleFactor;
    }

    @Override
    public float scale() {
        return scale;
    }

    @Override
    public int screenWidth() {
        return screenWidth;
    }

    @Override
    public int screenHeight() {
        return screenHeight;
    }

    @Override
    public ScreenOrientation screenOrientation() {
        return screenOrientation;
    }

    @Override
    public String userAgentString() {
        return userAgentString;
    }
}
