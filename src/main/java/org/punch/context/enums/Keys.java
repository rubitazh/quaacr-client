/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.enums;

public enum Keys {

    /* Controls */

    BACKSPACE(8, "Backspace", "Backspace", "", 0),
    TAB(9, "Tab", "Tab", "", 0),
    Enter(13, "Enter", "Enter", "\r", 0),
    SHIFT_LEFT(16, "ShiftLeft", "Shift", "", 1),
    CTRL_LEFT(17, "ControlLeft", "Control", "", 1),
    ALT_LEFT(18, "AltLeft", "Alt", "", 1),
    CAPS_LOCK(20, "CapsLock", "CapsLock", "", 0),
    ESC(27, "Escape", "Escape", "", 0),
    LEFT(37, "ArrowLeft", "ArrowLeft", "", 0),
    UP(38, "ArrowUp", "ArrowUp", "", 0),
    RIGHT(39, "ArrowRight", "ArrowRight", "", 0),
    DOWN(40, "ArrowDown", "ArrowDown", "", 0),
    META_LEFT(91, "MetaLeft", "Meta", "", 1),

    /* Alphabet */

    KeyA(65, "KeyA", "a", "", 0),
    KeyC(67, "KeyC", "c", "", 0),
    KeyP(80, "KeyP", "p", "", 0),
    KeyS(83, "KeyS", "s", "", 0),
    KeyV(86, "KeyV", "v", "", 0),
    KeyX(88, "KeyX", "x", "", 0),
    KeyZ(90, "KeyZ", "z", "", 0);

    private final int keyCode;
    private final String code;
    private final String key;
    private final String text;
    private final int location;

    Keys(int keyCode, String code, String key, String text, int location) {
        this.keyCode = keyCode;
        this.code = code;
        this.key = key;
        this.text = text;
        this.location = location;
    }

    public int keyCode() {
        return keyCode;
    }

    public String code() {
        return code;
    }

    public String key() {
        return key;
    }

    public String text() {
        return text;
    }

    public int location() {
        return location;
    }
}
