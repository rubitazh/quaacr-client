/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.enums;

/** Pseudo-elements */
public enum PseudoElement {
    AFTER("::after"),
    BACKDROP("::backdrop"),
    BEFORE("::before"),
    CUE("::cue"),
    CUE_REGION("::cue-region"),
    FIRST_LETTER("::first-letter"),
    FIRST_LINE("::first-line"),
    FILE_SELECTOR_BUTTON("::file-selector-button"),
    GRAMMAR_ERROR("::grammar-error"),
    MARKER("::marker"),
    PART("::part()"),
    PLACEHOLDER("::placeholder"),
    SELECTION("::selection"),
    SLOTTED("::slotted()"),
    SPELLING_ERROR("::spelling-er<ror"),
    TARGET_TEXT("::target-text");

    private final String value;

    PseudoElement(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
