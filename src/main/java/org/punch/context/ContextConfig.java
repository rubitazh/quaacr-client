/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context;

import org.punch.context.entities.Locate;

/* This is browser context config. Pretty obvious. */
public class ContextConfig {

    /* The default timeout for an event to be fired. */
    private static final long AWAIT_EVENT_TIMEOUT = 30_000L;

    /* The default timeout for a Target domain 'page' tab to appear */
    private static final long AWAIT_TARGET_PAGE_TIMEOUT = 5_000;

    /**
     * Number of items {@link org.punch.devtools.interfaces.EventConsumer} queues can store.
     */
    private static final int DEFAULT_CONSUMER_QUEUE_LIMIT = 1 << 10;

    /**
     * The default timeout for a node to be located. This is total value for a node to be located.
     *
     * @see Locate#htmlNode(String).
     */
    private static final long LOCATE_NODE_TOTAL_TIMEOUT = 10_000L;

    /**
     * The default timeout for a node to be found, if it hasn't been located yet. So, overall timeout
     * to find node is {@link #LOCATE_NODE_TOTAL_TIMEOUT} value, and this value is the time quaacr
     * waits between requests to protocol to find given node.
     */
    private static final long LOCATE_NODE_POLL_TIMEOUT = 100L;

    /* The default timeout to wait for a response from a WebSocket server by a client. */
    private static final long WS_RESPONSE_TIMEOUT = 30_000L;

    /* Whether iframes and shadow roots should be traversed when search through DOM. */
    private static final boolean INCLUDE_SHADOW_DOM = false;

    /* Amount of time in millis to wait for a DevTools protocol event to be fired. */
    private Long awaitEventTimeout = AWAIT_EVENT_TIMEOUT;

    /* Amount of time in millis to wait for a browser tab to be opened from scratch. */
    private Long awaitTargetPageTimeout = AWAIT_TARGET_PAGE_TIMEOUT;

    /**
     * Number of events quaacr holds in a queue for each event name. If number of fired events exceeds
     * this number, the oldest events will be removed from the event's queue to keep the given size of
     * the queue. I would not recommend to change this value until you are sure, that there are events,
     * that are deleted before they are actually used by quaacr somehow.
     */
    private int consumerQueueLimit = DEFAULT_CONSUMER_QUEUE_LIMIT;

    /**
     * Time quaacr waits for an element to be located on a web page. Yes, the name is a bit long,
     * but this is the way it should be.
     */
    private Long locateNodeRitaSaidTenSecondsIsGoodTimeout = LOCATE_NODE_TOTAL_TIMEOUT;

    /**
     * Small timeout quaacr waits before locate element retry. So, If an element is not currently found
     * on a web page, quaacr waits this amounts of time, then it tries to find an element by given query
     * again, and it repeats the steps for {@link ContextConfig#locateNodeRitaSaidTenSecondsIsGoodTimeout}
     * amount of time.
     */
    private Long locateNodePollTimeout = LOCATE_NODE_POLL_TIMEOUT;

    /* For now this field is always false and cannot be changed. */
    private Boolean includeShadowDOM = INCLUDE_SHADOW_DOM;

    /* Amount of time quaacr waits for DevTools websocket response. I don't think you need to change it. */
    private Long wsResponseTimeout = WS_RESPONSE_TIMEOUT;

    public ContextConfig() {
        //default
    }

    /* Copy constructor */
    public ContextConfig(ContextConfig other) {
        this.awaitEventTimeout = other.awaitEventTimeout;
        this.awaitTargetPageTimeout = other.awaitTargetPageTimeout;
        this.consumerQueueLimit = other.consumerQueueLimit;
        this.locateNodeRitaSaidTenSecondsIsGoodTimeout = other.locateNodeRitaSaidTenSecondsIsGoodTimeout;
        this.locateNodePollTimeout = other.locateNodePollTimeout;
        this.includeShadowDOM = other.includeShadowDOM;
        this.wsResponseTimeout = other.wsResponseTimeout;
    }

    /* Getters */

    public Long getAwaitEventTimeout() {
        return awaitEventTimeout;
    }

    public Long getAwaitTargetPageTimeout() {
        return awaitTargetPageTimeout;
    }

    public int getConsumerQueueLimit() {
        return consumerQueueLimit;
    }

    public Long getLocateNodeRitaSaidTenSecondsIsGoodTimeout() {
        return locateNodeRitaSaidTenSecondsIsGoodTimeout;
    }

    public Long getLocateNodePollTimeout() {
        return locateNodePollTimeout;
    }

    public Long getWsResponseTimeout() {
        return wsResponseTimeout;
    }

    public Boolean getIncludeShadowDOM() {
        return includeShadowDOM;
    }

    /* Setters */

    public void setAwaitEventTimeout(Long awaitEventTimeout) {
        this.awaitEventTimeout = awaitEventTimeout;
    }

    public void setAwaitTargetPageTimeout(Long awaitTargetPageTimeout) {
        this.awaitTargetPageTimeout = awaitTargetPageTimeout;
    }

    public void setConsumerQueueLimit(int consumerQueueLimit) {
        this.consumerQueueLimit = consumerQueueLimit;
    }

    public void setLocateNodeRitaSaidTenSecondsIsGoodTimeout(Long locateNodeRitaSaidTenSecondsIsGoodTimeout) {
        this.locateNodeRitaSaidTenSecondsIsGoodTimeout = locateNodeRitaSaidTenSecondsIsGoodTimeout;
    }

    public void setLocateNodePollTimeout(Long locateNodePollTimeout) {
        this.locateNodePollTimeout = locateNodePollTimeout;
    }

    public void setWsResponseTimeout(Long wsResponseTimeout) {
        this.wsResponseTimeout = wsResponseTimeout;
    }

    private void setIncludeShadowDOM(Boolean includeShadowDOM) {
        this.includeShadowDOM = includeShadowDOM;
    }
}
