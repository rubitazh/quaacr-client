/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context;

import org.punch.devtools.impl.EventPrinter;
import org.punch.devtools.impl.ProxyAddress;
import org.punch.devtools.interfaces.ChromeSession;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.punch.context.SessionParamKeys.*;

/**
 * Builds remote browser session context, or, as people say today, containerized web browser (yes, Docker).
 * So, you need to have an up and running Docker container instance to make it all work together. You will
 * find the required container <a href='https://bitbucket.org/rubitazh/quaacr-doockr'>here</a>.
 */
public class RemoteContextBuilder extends ContextBuilder {

    /**
     * Host a container with a web browser is running.
     * By default local host is suggested.
     */
    private static final String REMOTE_HOST = "127.0.0.1";

    /**
     * HTTP schema for HTTP communication between quaacr
     * client and a container a browser is running on.
     */
    private static final String HTTP_SCHEMA = "http";

    /* Container host default HTTP port.*/
    private static final int HTTP_PORT = 5141;

    /**
     * Container host default TCP port for exchanging DevTools
     * messages between quaacr client and containerized browser.
     */
    private static final int TCP_PORT = 9999;

    /**
     * URL address a remove web server provides to a client for sending
     * control commands to a running browser instance. Default value is:
     * {@link RemoteContextBuilder#HTTP_SCHEMA} +
     * {@link RemoteContextBuilder#REMOTE_HOST} +
     * {@link RemoteContextBuilder#HTTP_PORT}.
     * If it doesn't work for you, set this field via
     * {@link RemoteContextBuilder#setBaseUrl(URL)}.
     * Just make sure, that your baseUrl host matches the host browser
     * container is currently running on, and the container is listening
     * to specified HTTP port.
     */
    private URL baseUrl;

    /**
     * List of Chromium flags to start a browser instance with.
     * Suppose, these could be of some use at the beginning:
     * <ul>
     *     <li>--headless (headless mode)</li>
     *     <li>--kiosk (kiosk mode)</li>
     *     <li>--proxy-server (sets a proxy)</li>
     *     <li>--ignore-certificate-errors (cert errors not a problem anymore)</li>
     * </ul>
     * You can find more switches <a href='https://bit.ly/2Q4HOt9'>here</a>.
     */
    private List<String> browserOpts;

    /**
     * This field is responsible for a bunch of quaacr timeout settings a
     * client can tweak, if needed. Check {@link ContextConfig} for details.
     */
    private ContextConfig contextConfig;

    /**
     * Defines which DevTools JSON messages will be exposed to a client
     * during its runtime. Simply speaking, if you need some low-level
     * DevTools protocol debugging, then use this field to set it up.
     * Check {@link EventPrinter} for details.
     */
    private EventPrinter eventPrinter;

    /**
     * By default, quaacr starts a browser instance with a set of flags
     * (or switches, see {@link RemoteContextBuilder#browserOpts} to make
     * a browser be more suitable for running some tests on it. If for
     * any reason you need to start a pristine browser instance without
     * any flags, then just this field is set to {@code true}. If you
     * want to know a list of default switches before turning them off,
     * check {@link RemoteContextBuilder#setNoDefaultOpts(boolean)}
     * Java doc.
     */
    private boolean noDefaultOpts;

    /**
     * Browser profile name. This is where a web browser will store the
     * session data. If you need an independent browser session on each
     * start, this field must be unique every time quaacr starts new
     * browser session. But this is what quaacr does by default, so you
     * are lucky. If you need the session to be the same (like there is
     * only one user), then make sure that field has the same value on
     * every start.
     */
    private String profileName;

    /**
     * This is the address quaacr client communicates with the server
     * which resides within Docker container, listens to the DevTools
     * requests sent by quaacr, passes these requests to a Chromium
     * instance and send DevTools response back to quaacr client.
     * Default value is: {@link RemoteContextBuilder#REMOTE_HOST} +
     * {@link RemoteContextBuilder#TCP_PORT}, which suggests that
     * Docker container runs locally on the same host quaacr client
     * does. If you need to change a host or a port, use this field
     * to set it up, but do not forget to change the container env
     * variables, so it could listen to on a new address.
     */
    private ProxyAddress proxyAddress;

    /**
     * Timeout for a new browser instance to be opened. Default value
     * is 30 seconds. If it's not enough, go ahead and set your own.
     */
    private long sessionLoadTimeout;

    /**
     * Initial browser window size. Not used by default. This field
     * is used with '--window-size' key to pass width and height values
     * to a browser, so it would make sense just to pass it, as a
     * browser parameter, but due to quaacr limitations (for the
     * {@link RemoteChromeSession} instance, it is set via this field.
     */
    private int[] windowSize;

    /**
     * Browser window size against Docker desktop size. By default
     * it is set as 1 (like 100%), so a browser window resolution
     * equals to a desktop one. To reduce it set this field value
     * a number between 0 and 1.
     */
    private float windowSizeRatio;

    /**
     * Sets base url.
     *
     * @param baseUrl HTTP url address.
     * @see RemoteContextBuilder#baseUrl
     */
    public RemoteContextBuilder setBaseUrl(URL baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    /**
     * Sets browser options (or switches).
     *
     * @param browserOpts list of browser options.
     * @see RemoteContextBuilder#browserOpts
     */
    public RemoteContextBuilder setBrowserOpts(List<String> browserOpts) {
        this.browserOpts = browserOpts;
        return this;
    }

    /**
     * Sets remote context config.
     *
     * @param contextConfig remote context config.
     * @see RemoteContextBuilder#contextConfig
     */
    public RemoteContextBuilder setContextConfig(ContextConfig contextConfig) {
        this.contextConfig = contextConfig;
        return this;
    }

    /**
     * Sets DevTools events printer.
     *
     * @param eventPrinter event printer.
     * @see RemoteContextBuilder#eventPrinter
     */
    public RemoteContextBuilder setEventPrinter(EventPrinter eventPrinter) {
        this.eventPrinter = eventPrinter;
        return this;
    }

    /**
     * Sets Chromium default options to be ignored (or not). See the list of remote context
     * default switches <a href='https://bit.ly/3djHlft'>here</a> starting from line 50 to 59.
     *
     * @param noDefaultOpts ignore default opts flag (false by default).
     * @see RemoteContextBuilder#noDefaultOpts
     */
    public RemoteContextBuilder setNoDefaultOpts(boolean noDefaultOpts) {
        this.noDefaultOpts = noDefaultOpts;
        return this;
    }

    /**
     * Sets browser profile name.
     *
     * @param profileName profile dir name.
     * @see RemoteContextBuilder#profileName
     */
    @Override
    public ContextBuilder setProfileName(String profileName) {
        this.profileName = profileName;
        return this;
    }

    /**
     * Sets quaacr proxy server address which resides within
     * <a href='https://bitbucket.org/rubitazh/quaacr-doockr'>quaacr-doockr</a>. Docker container.
     *
     * @param proxyAddress server address.
     * @see RemoteContextBuilder#proxyAddress
     */
    public RemoteContextBuilder setProxyAddress(ProxyAddress proxyAddress) {
        this.proxyAddress = proxyAddress;
        return this;
    }

    /**
     * Sets the timeout for a new browser instance to be opened.
     *
     * @param sessionLoadTimeout open new browser session timeout.
     * @see RemoteContextBuilder#sessionLoadTimeout
     */
    @Override
    public ContextBuilder setSessionLoadTimeout(long sessionLoadTimeout) {
        this.sessionLoadTimeout = sessionLoadTimeout;
        return this;
    }

    /**
     * Sets browser initial window size.
     *
     * @param width  browser window width.
     * @param height browser window height.
     */
    @Override
    public ContextBuilder setWindowSize(int width, int height) {
        this.windowSize = new int[]{width, height};
        return this;
    }

    /**
     * Sets browser window size ratio against container desktop resolution.
     *
     * @param windowSizeRatio valid range is (0, 1].
     * @see RemoteContextBuilder#windowSizeRatio
     */
    @Override
    public ContextBuilder setWindowSizeRatio(float windowSizeRatio) {
        this.windowSizeRatio = windowSizeRatio;
        return this;
    }

    /**
     * Builds Chromium browser context.
     *
     * @return new browser instance context.
     */
    ChromeBrowserContext build() {
        //Set base url:
        if (Objects.isNull(baseUrl)) {
            try {
                baseUrl = new URL(HTTP_SCHEMA, REMOTE_HOST, HTTP_PORT, "");

            } catch (MalformedURLException e) {
                throw new IllegalArgumentException("Http server URL not valid. It's really not possible, " +
                        "but if it happened, send me a note, and I will stop coding immediately.", e);
            }
        }

        //Set config:
        contextConfig = Objects.isNull(contextConfig) ? new ContextConfig() : contextConfig;
        ContextConfig configCopy = new ContextConfig(contextConfig);
        //Set session params:
        Map<String, Object> sessionParams = new HashMap<>();
        sessionParams.put(NO_DEFAULT_OPTS, noDefaultOpts);

        if (Objects.nonNull(profileName)) {
            sessionParams.put(PROFILE_NAME, profileName);
        }

        if (sessionLoadTimeout != 0L) {
            sessionParams.put(SESSION_LOAD_TIMEOUT, sessionLoadTimeout);
        }

        if (windowSize != null) {
            sessionParams.put(WINDOW_SIZE, windowSize);
        }

        if (windowSizeRatio != 0F) {
            if (windowSizeRatio < 0 || windowSizeRatio > 1) {
                throw new IllegalArgumentException("Invalid windowSizeRatio value. " +
                        "Should be between 0 and 1. [windowSizeRatio=" + windowSizeRatio + "]");
            }

            sessionParams.put(WINDOW_SIZE_RATIO, windowSizeRatio);
        }

        //Create session:
        ChromeSession session = RemoteChromeSession.newInstance(baseUrl, browserOpts, sessionParams);
        //Set proxy:
        if (Objects.isNull(proxyAddress)) {
            proxyAddress = new ProxyAddress(REMOTE_HOST, TCP_PORT);
        }

        return initContext(configCopy, session, eventPrinter, proxyAddress);
    }
}
