/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context;

import org.punch.devtools.impl.*;
import org.punch.devtools.interfaces.ChromeSession;
import org.punch.devtools.interfaces.DevToolsClient;
import org.punch.devtools.interfaces.MessageController;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Defines a browser session context: browser configuration, DevTools debugging, Chromium browser options etc.
 */
public abstract class ContextBuilder {

    /**
     * List of browser opts keys to be prohibited by {@link ContextBuilder}
     * due to quaacr limitations (mostly for {@link RemoteChromeSession}).
     * Represents a pair of 'option name' - 'suitable setter method'.
     */
    protected static final Map<String, String> excludedBrowserOptsKeys = Map.of(
            "-window-size", "ContextBuilder#setWindowSize(int, int)");

    //It's pre-defined. No, you cannot change it.
    private static final int FIXED_THREADS_NUM = 1 << 4;

    /**
     * Builds a browser context.
     *
     * @return fully configured browser context.
     */
    abstract ChromeBrowserContext build();

    /**
     * Sets Chromium switches.
     */
    public abstract ContextBuilder setBrowserOpts(List<String> opts);

    /**
     * Sets {@link ContextConfig}.
     */
    public abstract ContextBuilder setContextConfig(ContextConfig contextConfig);

    /**
     * Sets {@link EventPrinter}.
     */
    public abstract ContextBuilder setEventPrinter(EventPrinter eventPrinter);

    /**
     * Sets Chromium default options (switches) to be ignored or not.
     */
    public abstract ContextBuilder setNoDefaultOpts(boolean noDefaultOpts);

    /**
     * Sets browser profile dir name.
     */
    public abstract ContextBuilder setProfileName(String profile);

    /**
     * Sets timeout to wait for a browser to be opened.
     */
    public abstract ContextBuilder setSessionLoadTimeout(long sessionLoadTimeout);

    /**
     * Sets the initial browser window size.
     */
    public abstract ContextBuilder setWindowSize(int width, int height);

    /**
     * Sets browser window size ratio. 1 counts as 100% of the screen resolution.
     */
    public abstract ContextBuilder setWindowSizeRatio(float windowSizeRatio);

    /**
     * Initiates browse context (local or remote).
     *
     * @param config       browser context config.
     * @param session      browser session (local or remote).
     * @param eventPrinter DevTools events printer.
     * @param proxyAddress container proxy server address (remote session only).
     * @return new Chromium based browser instance context.
     */
    protected ChromeBrowserContext initContext(ContextConfig config,
                                               ChromeSession session,
                                               EventPrinter eventPrinter,
                                               ProxyAddress proxyAddress) {

        final ExecutorService threadPool = Executors.newFixedThreadPool(FIXED_THREADS_NUM);
        final MessageController controller;

        if (session.getClass().equals(RemoteChromeSession.class)) {

            if (Objects.isNull(proxyAddress)) {
                throw new NullPointerException("Remote session proxy address not set.");
            }

            controller = new RemoteMessageController(session.getId(), proxyAddress, threadPool);

        } else {
            controller = new LocalMessageController(session.getWsUrl());
        }

        try {
            final DevToolsClient client = new BasicDevToolsClient(config, controller, threadPool);
            final DomainFactory domainFactory = new DomainFactory(client);

            if (Objects.nonNull(eventPrinter)) {
                client.printProtocolEvents(eventPrinter);
            }

            client.start();

            return new ChromeBrowserContext(config, session, client, domainFactory, threadPool);

        } catch (Exception e) {
            session.destroy();
            throw new RuntimeException("Init browser context error.", e);
        }
    }
}
