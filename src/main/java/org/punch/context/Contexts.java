/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context;

/**
 * Factory for a prepared {@link ChromeBrowserContext} implementations. I don't like the word 'Factory' in Java context,
 * so I didn't use that word in this class's name. I'm writing it, cause I know it could upset some people, so I'm
 * kind'a sorry in advance. Pardon me.
 */
public class Contexts {

    /**
     * Starts local Chrome browser session. So, when you run this on a host, make sure that this host has Chrome
     * browser installed locally.
     *
     * @return local Chrome browser session.
     */
    public static ChromeBrowserContext local() {
        return new LocalContextBuilder().build();
    }

    /**
     * Same as the method above, but with the user defined configuration.
     *
     * @param builder context builder for local Chrome session.
     * @return local Chrome browser session.
     * @see LocalContextBuilder
     */
    public static ChromeBrowserContext local(LocalContextBuilder builder) {
        return builder.build();
    }

    /**
     * Starts Chrome browser session in a container. So, you need
     * <a href="https://bitbucket.org/rubitazh/quaacr-doockr/src/master/">quaacr-doockr</a>
     * container to be run prior to execute this method.
     *
     * @return remote Chrome browser session.
     */
    public static ChromeBrowserContext remote() {
        return new RemoteContextBuilder().build();
    }

    /**
     * Same as the method above, but with the user defined configuration.
     *
     * @param builder context builder for remote Chrome session.
     * @return remote Chrome browser session.
     * @see RemoteContextBuilder
     */
    public static ChromeBrowserContext remote(RemoteContextBuilder builder) {
        return builder.build();
    }
}
