/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.interfaces;

import org.punch.context.entities.Emulate;

import static org.punch.devtools.protocol.emulation.Emulation.ScreenOrientation;

/**
 * Provides emulated device metrics parameters for {@link Emulate#device(DeviceMetrics)} method.
 */
public interface DeviceMetrics {

    /**
     * Returns emmulated device pixel ratio.
     * See <a href='https://developer.mozilla.org/en-US/docs/Web/API/Window/devicePixelRatio'>
     * Device pixel ratio</a> for details.
     */
    int deviceScaleFactor();

    /** Returns emulated device scale to apply to resulting view image. */
    float scale();

    /** Returns emulated device screen width in pixels. */
    int screenWidth();

    /** Returns emulated device screen height. */
    int screenHeight();

    /** Returns emulated device screen orientation. */
    ScreenOrientation screenOrientation();

    /** Returns emulated device User Agent. */
    String userAgentString();
}
