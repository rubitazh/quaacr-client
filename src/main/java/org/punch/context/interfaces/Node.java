/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.interfaces;

import org.punch.context.entities.ExecutionResult;
import org.punch.context.entities.NodeInfo;
import org.punch.devtools.protocol.runtime.Runtime;

import java.util.List;

import static org.punch.devtools.protocol.runtime.Runtime.*;

/**
 * Basic HTML DOM element interface. Any HTML node must implement it.
 */
public interface Node {
    /**
     * Applies given JS function to this node. For instance:
     * <pre>
     *     function() {
     *         return this.value;
     *     }
     * </pre>
     * must return this node 'value' field value or null, if not assigned, as a part of
     * {@link Runtime.FunctionResult} object.
     *
     * @param jsFunc        JavaScript function to be applied;
     * @param args          call arguments of JavaScript function. All call arguments must belong to the same
     *                      JavaScript world as the target node;
     * @param returnByValue whether the result is expected to be a JSON object which should be sent by value.
     * @return function execution result (as a positive outcome or an exception).
     */
    ExecutionResult applyJSFunction(String jsFunc, List<CallArgument> args, boolean returnByValue);

    /**
     * Provides node info to a client.
     * See  <a href='https://chromedevtools.github.io/devtools-protocol/tot/DOM/#type-Node'>DevTools DOM Node</a>}
     *
     * @return node attributes and values.
     * @see NodeInfo
     */
    NodeInfo getNodeInfo();

    /**
     * Returns Outer HTML source of this element.
     *
     * @return this element's HTML source as string.
     */
    String getSource();
}
