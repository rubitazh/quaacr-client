/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.interfaces;

public interface Scrollable {

    /**
     * Positive deltaY value scrolls page down, negative one scrolls page up.
     *
     * @param deltaY Y axis scroll delta in pixels.
     */
    void scroll(int deltaY);
}
