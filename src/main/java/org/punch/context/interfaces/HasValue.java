package org.punch.context.interfaces;

public interface HasValue {

    String getValue();

    static String getNodeValueFunction() {
        return "function () {" +
                "  if (this.nodeName !== 'INPUT' " +
                "          && this.nodeName !== 'TEXTAREA' " +
                "          && this.nodeName !== 'SELECT' " +
                "          && this.nodeName !== 'OPTION') {" +
                "    throw new Error('Element without \\'value\\' property.')" +
                "  }" +
                "  return this.value;" +
                "}";
    }
}
