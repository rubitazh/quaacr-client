/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.ex;

public class BrowserSessionException extends RuntimeException {

    public BrowserSessionException() {
        super();
    }

    public BrowserSessionException(String msg) {
        super(msg);
    }

    public BrowserSessionException(String msg, Throwable e) {
        super(msg, e);
    }
}
