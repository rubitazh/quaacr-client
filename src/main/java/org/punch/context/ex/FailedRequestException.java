/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.ex;

public class FailedRequestException extends RuntimeException {

    public FailedRequestException() {
        super();
    }

    public FailedRequestException(String msg) {
        super(msg);
    }

    public FailedRequestException(String msg, Throwable e) {
        super(msg, e);
    }
}
