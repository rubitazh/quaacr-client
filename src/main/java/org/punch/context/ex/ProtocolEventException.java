package org.punch.context.ex;

public class ProtocolEventException extends RuntimeException {

    public ProtocolEventException() {
        super();
    }

    public ProtocolEventException(String msg) {
        super(msg);
    }

    public ProtocolEventException(String msg, Throwable e) {
        super(msg, e);
    }
}
