/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.ex;

public class HttpResponseErrorException extends RuntimeException {

    public HttpResponseErrorException(String msg) {
        super(msg);
    }

    public HttpResponseErrorException(String msg, Throwable e) {
        super(msg, e);
    }
}