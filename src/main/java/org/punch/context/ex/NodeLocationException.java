package org.punch.context.ex;

public class NodeLocationException extends RuntimeException {

    public NodeLocationException(String msg) {
        super(msg);
    }

    public NodeLocationException(String msg, Throwable e) {
        super(msg, e);
    }
}
