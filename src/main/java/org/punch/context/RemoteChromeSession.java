/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context;

import org.punch.context.ex.BrowserSessionException;
import org.punch.devtools.interfaces.ChromeSession;
import org.punch.utils.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static org.punch.context.SessionParamKeys.*;

/**
 * This class initiates Chromium based browser remote (containerized) DevTools session. So, first, the container
 * must be started. See <a href='https://bitbucket.org/rubitazh/quaacr-doockr'>Quaacr Doockr project</a>.
 * <p>
 * Responsible for initiating a web browser with listening to DevTools port within a container.
 */
public class RemoteChromeSession implements ChromeSession {

    private static final Logger logger = LoggerFactory.getLogger(RemoteChromeSession.class);

    private static final String CHROME_GET = "/chrome/get";
    private static final String CHROME_CLOSE = "/chrome/close";
    private static final String CHROME_CLEANUP = "/chrome/cleanup";

    private static final String PROFILE_NAME_PREFIX = "quaacr_remote";
    private static final long DEFAULT_SESSION_LOAD_TIMEOUT = 30_000L;
    private static final double DEFAULT_WINDOW_SIZE_RATIO = 1.0;

    private static final AtomicBoolean isCleanupOnShutdownSet = new AtomicBoolean(false);
    private static final Random random = new Random();

    private final URL baseUrl;
    private final String wsUrl;

    private RemoteChromeSession(URL baseUrl, String wsUrl) {
        this.baseUrl = baseUrl;
        this.wsUrl = wsUrl;
    }

    /**
     * Initiates new Chromium based browser remote DevTools session.
     *
     * @param baseUrl       URL which a client uses to communicate with a container via HTTP.
     * @param browserOpts   list of web browser params, like '--headless', for instance.
     * @param sessionParams HTTP session params which defines some of web browser configuration.
     */
    public static RemoteChromeSession newInstance(URL baseUrl,
                                                  List<String> browserOpts,
                                                  Map<String, Object> sessionParams) {

        //If cleanup hook is not set, then sends request to remote session host
        // to remove profile dir with all its content on shutdown:
        if (!isCleanupOnShutdownSet.get()) {
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    URL cleanupUrl = new URL(baseUrl, CHROME_CLEANUP);
                    HttpURLConnection conn = (HttpURLConnection) cleanupUrl.openConnection();
                    conn.setRequestMethod("GET");
                    String isCleanedUp = HttpUtils.readResponse(conn);

                    logger.info("Remote session cleanup finished. [status={}]", isCleanedUp);

                } catch (IOException e) {
                    throw new RuntimeException("Cannot set Remote Session cleanup shutdown hook.", e);
                }
            }));

            isCleanupOnShutdownSet.set(true);
        }

        HttpURLConnection conn = null;
        //Here we build query to be sent to remote browser session:
        StringBuilder query = new StringBuilder("?");
        //Set Chrome opts:
        if (Objects.nonNull(browserOpts)) {
            //Remove empty strings, otherwise it will cause
            // exceptions on remote browser opts parsing:
            browserOpts = browserOpts.stream()
                    .filter(s -> !s.isBlank())
                    .collect(Collectors.toList());

            if (!browserOpts.isEmpty()) {
                query.append("browserOpts=");

                for (int i = 0; i < browserOpts.size(); i++) {
                    query.append(browserOpts.get(i));
                    //Don't add comma for last param
                    if (i == browserOpts.size() - 1) {
                        query.append("&");
                        continue;
                    }

                    query.append(",");
                }
            }
        }
        //Set session params:
        query.append(NO_DEFAULT_OPTS)
                .append("=")
                .append(sessionParams.getOrDefault(NO_DEFAULT_OPTS, false))
                .append("&");

        query.append(PROFILE_NAME)
                .append("=")
                .append(sessionParams.getOrDefault(PROFILE_NAME, PROFILE_NAME_PREFIX + random.nextInt(Integer.MAX_VALUE)))
                .append("&");

        query.append(SESSION_LOAD_TIMEOUT)
                .append("=")
                .append(sessionParams.getOrDefault(SESSION_LOAD_TIMEOUT, DEFAULT_SESSION_LOAD_TIMEOUT))
                .append("&");

        int[] userDefinedWindowSize = (int[]) sessionParams.get(WINDOW_SIZE);
        if (userDefinedWindowSize != null) {
            query.append(WINDOW_SIZE)
                    .append("=")
                    .append(userDefinedWindowSize[0])
                    .append("x")
                    .append(userDefinedWindowSize[1])
                    .append("&");
        }

        query.append(WINDOW_SIZE_RATIO)
                .append("=")
                .append(sessionParams.getOrDefault(WINDOW_SIZE_RATIO, DEFAULT_WINDOW_SIZE_RATIO));

        //Send request to init remote session:
        try {
            URL getChromeUrl = new URL(baseUrl, CHROME_GET + query);
            conn = (HttpURLConnection) getChromeUrl.openConnection();
            conn.setRequestMethod("GET");
            //Read response:
            String wsUrl = HttpUtils.readResponse(conn);
            //Session:
            RemoteChromeSession session = new RemoteChromeSession(baseUrl, wsUrl);

            logger.info("Remote Chrome session started. [id={}]", session.getId());

            return session;

        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(String.format("Invalid remote session URL. " +
                    "[baseUrl=%s, path=%s, query=%s]", baseUrl, CHROME_GET, query), e);

        } catch (IOException e) {
            String errorMessage = "";

            if (Objects.nonNull(conn)) {
                try {
                    InputStream es = conn.getErrorStream();

                    if (Objects.nonNull(es)) {
                        byte[] errBytes = es.readAllBytes();

                        if (errBytes.length > 0) {
                            errorMessage += "\n\n\t" +
                                    "----------------------------SERVER_ERROR_HEAD----------------------------\n\n\t" +
                                    new String(errBytes, StandardCharsets.UTF_8) + "\n\n\t" +
                                    "----------------------------SERVER_ERROR_TAIL----------------------------\n\t";
                        }
                    }
                } catch (IOException ex) {
                    //swallow
                }
            }

            throw new BrowserSessionException(String.format("Start remote browser session error. %s", errorMessage), e);

        } finally {
            if (Objects.nonNull(conn)) {
                conn.disconnect();
            }
        }
    }

    @Override
    public String getWsUrl() {
        return wsUrl;
    }

    @Override
    public void destroy() {
        HttpURLConnection conn = null;
        String query = "?id=" + getId();

        try {
            URL closeChromeUrl = new URL(baseUrl, CHROME_CLOSE + query);

            conn = (HttpURLConnection) closeChromeUrl.openConnection();
            conn.setRequestMethod("GET");
            //Read response
            var wsId = HttpUtils.readResponse(conn);

            logger.info("Remote Chrome session closed. [id={}]", wsId);

        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(String.format("Invalid remote session URL. " +
                    "[url=%s, path=%s, query=%s]", baseUrl, CHROME_CLOSE, query), e);

        } catch (IOException e) {
            logger.error("Remote session HTTP connection error.", e);

        } finally {
            if (Objects.nonNull(conn)) {
                conn.disconnect();
            }
        }
    }
}
