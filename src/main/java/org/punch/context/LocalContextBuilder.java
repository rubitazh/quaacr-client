/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context;

import org.punch.devtools.impl.EventPrinter;
import org.punch.devtools.interfaces.ChromeSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.punch.context.SessionParamKeys.*;

/**
 * Builds local browser session context. It means that when quaacr starts, it expects a Chromium based browser
 * to be installed on the same host to default directory. It should be '/usr/bin' dir for Linux, '/Applications'
 * dir for  MasOS, 'C:/Program Files' dir for Windows. If the browser is located somewhere else, then use
 * {@link LocalContextBuilder#setExecPath(String)} to specify browser's executable.
 */
public class LocalContextBuilder extends ContextBuilder {

    /**
     * List of Chromium flags to start a browser instance with.
     * Suppose, these could be of some use at the beginning:
     * <ul>
     *     <li>--headless (headless mode)</li>
     *     <li>--kiosk (kiosk mode)</li>
     *     <li>--proxy-server (sets a proxy)</li>
     *     <li>--ignore-certificate-errors (cert errors not a problem anymore)</li>
     * </ul>
     * You can find more switches <a href='https://bit.ly/2Q4HOt9'>here</a>.
     */
    private List<String> browserOpts;

    /**
     * This field is responsible for a bunch of quaacr timeout settings a
     * client can tweak, if needed. Check {@link ContextConfig} for details.
     */
    private ContextConfig contextConfig;

    /**
     * Defines which DevTools JSON messages will be exposed to a client
     * during its runtime. Simply speaking, if you need some low-level
     * DevTools protocol debugging, then use this field to set it up.
     * Check {@link EventPrinter} for details.
     */
    private EventPrinter eventPrinter;

    /**
     * Path to Chromium browser executable for local session. So, if you
     * installed the browser into a specific directory, make sure you
     * specified this path to quaacr via this field.
     */
    private String execPath;

    /**
     * By default, quaacr starts a browser instance with a set of flags
     * (or switches, see {@link LocalContextBuilder#browserOpts} to make
     * a browser be more suitable for running some tests on it. If for
     * any reason you need to start a pristine browser instance without
     * any flags, then just this field is set to {@code true}. If you
     * want to know a list of default switches before turning them off,
     * check {@link LocalChromeSession} class from line 51 to 58.
     */
    private boolean noDefaultOpts;

    /**
     * Browser profile name. This is where a web browser will store the
     * session data. If you need an independent browser session on each
     * start, this field must be unique every time quaacr starts new
     * browser session. But this is what quaacr does by default, so you
     * are lucky. If you need the session to be the same (like there is
     * only one user), then make sure that field has the same value on
     * every start.
     */
    private String profileName;

    /**
     * Path to dir where all browser session profile dirs are located.
     * Default value is 'quaacr_profiles' dir under a 'home' dir of a
     * user which executes quaacr code.
     */
    private String profilePath;

    /**
     * Timeout for a new browser instance to be opened. Default value
     * is 30 seconds. If it's not enough, go ahead and set your own.
     */
    private long sessionLoadTimeout;

    /**
     * Initial browser window size. Not used by default. This field
     * is used with '--window-size' key to pass width and height values
     * to a browser, so it would make sense just to pass it, as a
     * browser parameter, but due to quaacr limitations (for the
     * {@link RemoteChromeSession} instance) it is set via this field.
     */
    private int[] windowSize;

    /**
     * Browser window size against local host desktop size. By default
     * it is set as 1 (like 100%), so a browser window resolution
     * equals to a desktop one. To reduce it set this field value
     * a number between 0 and 1.
     */
    private float windowSizeRatio;

    /**
     * Sets browser options (or switches).
     *
     * @param browserOpts list of browser options.
     * @see LocalContextBuilder#browserOpts
     */
    @Override
    public LocalContextBuilder setBrowserOpts(List<String> browserOpts) {
        browserOpts.forEach(bo -> {
            if (excludedBrowserOptsKeys.containsKey(bo)) {
                throw new RuntimeException("This browser option not allowed, use the setter instead. " +
                        "[option=" + bo + ", setter=" + excludedBrowserOptsKeys.get(bo) + "]");
            }
        });
        this.browserOpts = browserOpts;
        return this;
    }

    /**
     * Sets local context config.
     *
     * @param contextConfig local context config.
     * @see LocalContextBuilder#contextConfig
     */
    @Override
    public LocalContextBuilder setContextConfig(ContextConfig contextConfig) {
        this.contextConfig = contextConfig;
        return this;
    }

    /**
     * Sets DevTools events printer.
     *
     * @param eventPrinter event printer.
     * @see LocalContextBuilder#eventPrinter
     */
    @Override
    public LocalContextBuilder setEventPrinter(EventPrinter eventPrinter) {
        this.eventPrinter = eventPrinter;
        return this;
    }

    /**
     * Sets a path to web browser executable.
     *
     * @param execPath path to browser executable.
     * @see LocalContextBuilder#execPath
     */
    public LocalContextBuilder setExecPath(String execPath) {
        this.execPath = execPath;
        return this;
    }

    /**
     * Sets Chromium default options to be ignored (or not).
     *
     * @param noDefaultOpts ignore default opts flag (false by default).
     * @see LocalContextBuilder#noDefaultOpts
     */
    @Override
    public LocalContextBuilder setNoDefaultOpts(boolean noDefaultOpts) {
        this.noDefaultOpts = noDefaultOpts;
        return this;
    }

    /**
     * Sets browser profile name.
     *
     * @param profileName profile dir name.
     * @see LocalContextBuilder#profileName
     */
    @Override
    public ContextBuilder setProfileName(String profileName) {
        this.profileName = profileName;
        return this;
    }

    /**
     * Sets a path to web browser profiles.
     *
     * @param profilePath path to browser profiles.
     * @see LocalContextBuilder#profilePath
     */
    public ContextBuilder setProfilePath(String profilePath) {
        this.profilePath = profilePath;
        return this;
    }

    /**
     * Sets the timeout for a new browser instance to be opened.
     *
     * @param sessionLoadTimeout open new browser session timeout.
     * @see LocalContextBuilder#sessionLoadTimeout
     */
    @Override
    public ContextBuilder setSessionLoadTimeout(long sessionLoadTimeout) {
        this.sessionLoadTimeout = sessionLoadTimeout;
        return this;
    }

    /**
     * Sets browser initial window size.
     *
     * @param width  browser window width.
     * @param height browser window height.
     */
    @Override
    public ContextBuilder setWindowSize(int width, int height) {
        this.windowSize = new int[]{width, height};
        return this;
    }

    /**
     * Sets browser window size ratio against local host desktop resolution.
     *
     * @param windowSizeRatio valid range is (0, 1].
     * @see LocalContextBuilder#windowSizeRatio
     */
    @Override
    public ContextBuilder setWindowSizeRatio(float windowSizeRatio) {
        this.windowSizeRatio = windowSizeRatio;
        return this;
    }

    /**
     * Builds Chromium browser context.
     *
     * @return new browser instance context.
     */
    ChromeBrowserContext build() {
        //Set config:
        contextConfig = Objects.isNull(contextConfig) ? new ContextConfig() : contextConfig;
        ContextConfig configCopy = new ContextConfig(contextConfig);
        //Set session params:
        Map<String, Object> sessionParams = new HashMap<>();
        sessionParams.put(NO_DEFAULT_OPTS, noDefaultOpts);

        if (Objects.nonNull(profileName)) {
            sessionParams.put(PROFILE_NAME, profileName);
        }

        if (Objects.nonNull(profilePath)) {
            sessionParams.put(PROFILE_PATH, profilePath);
        }

        if (sessionLoadTimeout != 0L) {
            sessionParams.put(SESSION_LOAD_TIMEOUT, sessionLoadTimeout);
        }

        if (Objects.nonNull(windowSize)) {
            sessionParams.put(WINDOW_SIZE, windowSize);
        }

        if (windowSizeRatio != 0f) {
            if (windowSizeRatio < 0 || windowSizeRatio > 1) {
                throw new IllegalArgumentException("Invalid windowSizeRatio value. " +
                        "Should be between 0 and 1. [windowSizeRatio=" + windowSizeRatio + "]");
            }

            sessionParams.put(WINDOW_SIZE_RATIO, windowSizeRatio);
        }

        //Create session:
        ChromeSession session = LocalChromeSession.newInstance(execPath, browserOpts, sessionParams);

        return initContext(configCopy, session, eventPrinter, null);
    }
}