package org.punch.context;

public class SessionData {

    private final String targetId;
    private final String sessionId;

    public SessionData(String targetId, String sessionId) {
        this.targetId = targetId;
        this.sessionId = sessionId;
    }

    public String getTargetId() {
        return targetId;
    }

    public String getSessionId() {
        return sessionId;
    }
}
