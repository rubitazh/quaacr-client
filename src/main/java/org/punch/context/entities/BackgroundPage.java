/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.punch.context.ContextConfig;
import org.punch.context.SessionData;
import org.punch.devtools.impl.DomainFactory;
import org.punch.devtools.interfaces.EventConsumer;

/**
 * Represents a background service, like Web Worker, for instance..
 */
public class BackgroundPage extends Session {

    public BackgroundPage(ContextConfig config,
                          SessionData sessionData,
                          DomainFactory domainFactory,
                          EventConsumer eventConsumer) {

        super(config, sessionData, domainFactory, eventConsumer);
    }
}
