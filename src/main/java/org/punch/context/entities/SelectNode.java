/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.punch.context.ContextConfig;
import org.punch.context.ex.SearchNodeException;
import org.punch.context.interfaces.HasValue;
import org.punch.context.ex.ScriptExecutionException;
import org.punch.devtools.interfaces.EventConsumer;
import org.punch.devtools.protocol.runtime.Runtime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SelectNode extends HtmlNode implements HasValue {

    SelectNode(Session session, ContextConfig config, EventConsumer consumer, String query, int index, int foundTotal) {
        super(session, config, consumer, query, index, foundTotal);
    }

    /* We check that returned value is of type List before casting. */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public void select(String... opts) {
        List<Runtime.CallArgument> args = Arrays.stream(opts)
                .map(Runtime.CallArgument::of)
                .collect(Collectors.toList());

        scrollIntoView();
        String objectId = getObjectId();
        Runtime.FunctionResult functionResult = runtimeJS.callFunctionOn(
                select(),
                objectId,
                args,
                null, true,
                null, null,
                null, null,
                null);

        runtimeJS.checkFunctionResultErrors(functionResult);
        runtimeJS.releaseObject(objectId);

        Object value = functionResult.getResult().getValue();

        if (Objects.isNull(value) || !value.getClass().equals(ArrayList.class)) {
            throw new ScriptExecutionException("Unexpected select response. array expected, " +
                    "but other response arrived. [value=" + value + "]");
        }

        List<Object> optsList = (List) value;
        List<Object> argsList = args.stream().map(Runtime.CallArgument::getValue).collect(Collectors.toList());

        if (!optsList.containsAll(argsList)) {
            //TODO: meaningless exception
            throw new SearchNodeException("Select option not presented. " +
                    "[requested=" + argsList + ", available=" + optsList + "]");
        }
    }

    @Override
    public String getValue() {
        String objectId = getObjectId();
        Runtime.FunctionResult functionResult = runtimeJS.callFunctionOn(
                HasValue.getNodeValueFunction(),
                getObjectId(),
                null, null,
                null, null,
                null, null,
                null, null);

        runtimeJS.checkFunctionResultErrors(functionResult);
        runtimeJS.releaseObject(objectId);

        return (String) functionResult.getResult().getValue();
    }

    private static String select() {
        return "function(...args) {" +
                "  if (this.nodeName !== 'SELECT') {" +
                "    throw new Error('Element not SELECT.')" +
                "  }" +
                "  const options = this.options;" +
                "  const values = [];" +
                "  this.value = undefined;" +
                "  for (const option of options) {" +
                "    values.push(option.value);" +
                "    option.selected = args.includes(option.value);" +
                "    if (option.selected && !this.multiple) break;" +
                "  }" +
                "  this.dispatchEvent(new Event('input', { 'bubbles': true }));" +
                "  this.dispatchEvent(new Event('change', { 'bubbles': true }));" +
                "  return values;" +
                "}";
    }
}
