/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.punch.context.interfaces.DeviceMetrics;
import org.punch.devtools.enums.emulation.MouseTouchEventsConfig;
import org.punch.devtools.protocol.emulation.Emulation;

import static org.punch.devtools.protocol.emulation.Emulation.DisplayFeature;

/**
 * This class emulates a web browser mobile device behaviour according to provided {@link DeviceMetrics}.
 *
 * @see Tab#emulate()
 * @see DeviceMetrics
 */
public class Emulate {

    private final Emulation emulation;

    /**
     * Default constructor.
     *
     * @param emulation {@link Emulation} domain instance.
     */
    Emulate(Emulation emulation) {
        this.emulation = emulation;
    }

    /**
     * Sets web browser into a state as if it is run on a mobile device.
     *
     * @param deviceMetrics emulated device metrics parameters.
     */
    public void device(DeviceMetrics deviceMetrics) {
        final int width = 0;
        final int height = 0;
        final boolean mobile = true;
        final int positionX = 0;
        final int positionY = 0;
        final boolean dontSetVisibleSize = true;
        final DisplayFeature displayFeature = null;

        emulation.resetPageScaleFactor();
        emulation.setDeviceMetricsOverride(
                width,
                height,
                deviceMetrics.deviceScaleFactor(),
                mobile,
                deviceMetrics.scale(),
                deviceMetrics.screenWidth(),
                deviceMetrics.screenHeight(),
                positionX,
                positionY,
                dontSetVisibleSize,
                deviceMetrics.screenOrientation(),
                displayFeature);

        emulation.setUserAgentOverride(deviceMetrics.userAgentString(), null, null, null);
        emulation.setTouchEmulationEnabled(true, 1);
        emulation.setEmitTouchEventsForMouse(true, MouseTouchEventsConfig.mobile);
    }
}
