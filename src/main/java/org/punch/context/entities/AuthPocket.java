/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

/**
 * This is a holder for Basic Authentication credentials, plus, some config (currently, it's only one config field,
 * but, you know, it could be extended some day).
 */
public class AuthPocket {

    private static final long AWAIT_RESPONSE_TIMEOUT = 5000L;

    private final String user;
    private final String password;
    private final long awaitResponseTimeout;

    public static AuthPocket of(String user, String password) {
        return new AuthPocket(user, password, AWAIT_RESPONSE_TIMEOUT);
    }

    /**
     * You pass it to {@link org.punch.context.entities.Tab#authenticate(AuthPocket)}, if authentication is required.
     *
     * @param user                 Basic auth user
     * @param password             Basic auth password
     * @param awaitResponseTimeout This is await time between auth request and response. So, if you send a request to
     *                             a resource, which requires auth, and there is no response within given timeout,
     *                             auth task will be stopped, and you will receive warning message, and if this is
     *                             the case, you might want to increase this timeout.
     */
    public static AuthPocket of(String user, String password, long awaitResponseTimeout) {
        return new AuthPocket(user, password, awaitResponseTimeout);
    }

    /**
     * Same as method above, but with default {@param awaitResponseTimeout} set.
     */
    private AuthPocket(String user, String password, long awaitResponseTimeout) {
        this.user = user;
        this.password = password;
        this.awaitResponseTimeout = awaitResponseTimeout;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public long getAwaitResponseTimeout() {
        return awaitResponseTimeout;
    }
}
