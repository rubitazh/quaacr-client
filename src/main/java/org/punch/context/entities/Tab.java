/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.json.JSONObject;
import org.punch.context.ContextConfig;
import org.punch.context.SessionData;
import org.punch.context.enums.DeviceScreenSize;
import org.punch.context.enums.TargetType;
import org.punch.context.ex.BrowserSessionException;
import org.punch.context.ex.FailedRequestException;
import org.punch.context.ex.HttpResponseErrorException;
import org.punch.context.ex.ProtocolEventException;
import org.punch.context.interfaces.Scrollable;
import org.punch.devtools.enums.emulation.MouseTouchEventsConfig;
import org.punch.devtools.enums.network.HttpMethod;
import org.punch.devtools.enums.page.ImageFormat;
import org.punch.devtools.enums.page.ReferrerPolicy;
import org.punch.devtools.enums.page.TransitionType;
import org.punch.devtools.enums.runtime.JsDialogType;
import org.punch.devtools.enums.runtime.RemoteObjectType;
import org.punch.devtools.ex.PollEventException;
import org.punch.devtools.ex.ProtocolResponseErrorException;
import org.punch.devtools.impl.BasicEventConsumer;
import org.punch.devtools.impl.DomainFactory;
import org.punch.devtools.impl.singletons.BasicJsonConverter;
import org.punch.devtools.interfaces.BrowserContext;
import org.punch.devtools.interfaces.EventConsumer;
import org.punch.devtools.interfaces.JsonConverter;
import org.punch.devtools.protocol.animation.Animation;
import org.punch.devtools.protocol.browser.Browser;
import org.punch.devtools.protocol.dom.DOM;
import org.punch.devtools.protocol.emulation.Emulation;
import org.punch.devtools.protocol.io.IO;
import org.punch.devtools.protocol.log.Log;
import org.punch.devtools.protocol.network.Network;
import org.punch.devtools.protocol.overlay.Overlay;
import org.punch.devtools.protocol.page.Page;
import org.punch.devtools.protocol.runtime.Runtime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static org.punch.context.entities.DomainType.PAGE;
import static org.punch.context.entities.EventRules.*;
import static org.punch.context.entities.TabView.*;
import static org.punch.devtools.enums.browser.BrowserPermission.*;
import static org.punch.devtools.enums.browser.PermissionSetting.*;
import static org.punch.devtools.protocol.animation.Animation.Event.*;
import static org.punch.devtools.protocol.input.Input.MouseButton.none;
import static org.punch.devtools.protocol.input.Input.MouseType.mouseWheel;
import static org.punch.devtools.protocol.log.Log.Event.entryAdded;
import static org.punch.devtools.protocol.network.Network.Event.*;
import static org.punch.devtools.protocol.page.Page.Event.*;
import static org.punch.devtools.protocol.runtime.Runtime.*;
import static org.punch.devtools.protocol.runtime.Runtime.Event.consoleAPICalled;
import static org.punch.devtools.protocol.target.Target.Event.*;

/**
 * Represents a browser tab (a page in DevTools terminology).
 */
public class Tab extends Session implements Scrollable {

    private static final Logger logger = LoggerFactory.getLogger(Tab.class);

    private final JsonConverter jsonConverter = BasicJsonConverter.INSTANCE;
    private final Map<TabView, Object> tabViews = new HashMap<>();

    private final String frameId;
    private final EventConsumer eventConsumer;
    private final ExecutorService threadPool;

    private boolean firstNavPassed;
    private String loaderId;
    private boolean inEmulatedMode;
    private double ts;

    private Future<?> authTaskResult;

    public Tab(ContextConfig config,
               SessionData sessionData,
               DomainFactory domainFactory,
               EventConsumer eventConsumer,
               ExecutorService threadPool,
               boolean isNewTabNavigated) {

        super(config, sessionData, domainFactory, eventConsumer);
        this.frameId = sessionData.getTargetId();
        this.eventConsumer = eventConsumer;
        this.threadPool = threadPool;
        //Otherwise we might miss init events:
        getPage();
        getDOM();
        //if new tab is open via window.open(), link click,
        // form submission, etc, then we need to set loaderId:
        if (isNewTabNavigated) {
            setNewTabLoaderId();
        }
    }

    /**
     * This is for Basic authentication. It handles both simple auth request-response and auth request-redirect cases.
     * If you don't understand, what it is all about - nevermind, just use it this way:
     * <pre>
     *     Tab tab = cxt.emptyTab();
     *     tab.authenticate("user1", "12345");
     *     tab.open("https://page_with_basic_auth.org");
     * </pre>
     * Please make sure you place this method BEFORE {@link #open} (I know, there are always be exceptional cases,
     * but who cares about them).
     *
     * @param pocket holder for credentials and other auth-related settings.
     * @see AuthPocket
     */
    public void authenticate(AuthPocket pocket) {
        BasicAuth basicAuth = new BasicAuth(frameId, getNetwork(), getFetch(), eventConsumer, threadPool);
        authTaskResult = basicAuth.authenticate(pocket);
    }

    /* Switches to tab, if it's not currently active. */
    public void bringToFront() {
        getPage().bringToFront();
    }

    /**
     * Creates JavaScript dialog handler.
     *
     * @return dialog handler instance.
     * @see DialogHandler
     * @see JsDialogType
     */
    public DialogHandler createJsDialogHandler() {
        return new DialogHandler(getPage(), eventConsumer, threadPool);
    }

    /**
     * Verifies that specific node was inserted into this tab DOM. Returns inserted node id on success.
     * See <a href='https://chromedevtools.github.io/devtools-protocol/tot/DOM/#event-childNodeInserted'>
     * childNodeInserted</a> event description.
     * <p>
     * Example:
     * <pre>{@code
     *     BrowserContext cxt = Contexts.local();
     *     Tab tab = cxt.defaultTab();
     *     tab.open("http://example.com");
     *     tab.waitPageLoad();
     *     tab.locate()
     *              .htmlNode("#someId")
     *              .click();        //This click forces a DIV to be popped up
     *
     * int nodeId = tab.domNodeInserted("div", List.of("title", "popup-node"));
     * tab.clickZero();  //Remove popped up window
     * tab.domNodeRemoved(nodeId);  //Verify that popped up node was removed
     *                              // from DOM (DOM.childNodeRemoved event)
     * }</pre>
     * <p>
     * NB! If you execute a search in between {@code tab.domNodeInserted} and {@code domNodeRemoved} methods,
     * then you should use <b>the same</b> {@link Locate} instance to avoid any mess with node ids:
     * <pre>{@code
     *     Locate locate = tab.locate();
     *     locate.htmlNode(locator).click();
     *     int nodeId = tab.domNodeInserted("span", null);
     *     //Now if locate a new element and then check
     *     // for the node removal, this will work:
     *     locate.htmlNode(otherLocator).click(); //it makes 'span' to be removed
     *     tab.domNodeRemoved(nodeId);
     *     //And this will not:
     *     tab.locate().htmlNode(otherLocator).click(); //new Locate instance with the fresh DOM
     *     tab.domNodeRemoved(nodeId); //throws exception cause this nodeId not exist anymore, as
     *                                 // quaacr get fresh DOM document with each Locate instance.
     * }</pre>
     *
     * @param localName  inserted node local name value.
     * @param attributes inserted node attributes. You must list all the
     *                   attributes or pass {@code null} if you are lazy.
     * @return inserted node id.
     * @throws ProtocolEventException if DOM.childNodeInserted event wasn't fired.
     * @see Tab#domNodeRemoved
     */
    public int domNodeInserted(String localName, List<String> attributes) {
        final DOM.Event childNodeInserted = DOM.Event.childNodeInserted;
        try {
            JSONObject childNodeInsertedEvt = eventConsumer.getFiredEvent(
                    childNodeInserted, childNodeRule(localName, attributes));

            return (int) childNodeInsertedEvt.query(NODE_NODE_ID_Q);

        } catch (PollEventException e) {
            throw new ProtocolEventException(String.format("Event failure. [evt=%s, localName=%s, attributes=%s]",
                    childNodeInserted, localName, attributes), e);
        }
    }

    /**
     * Verifies that the node with provided {@code nodeId} is removed from DOM.
     *
     * @param nodeId nodeId expected to be removed.
     * @throws ProtocolEventException if DOM.childNodeRemoved event wasn't fired.
     * @see Tab#domNodeInserted
     */
    public void domNodeRemoved(int nodeId) {
        final DOM.Event childNodeRemoved = DOM.Event.childNodeRemoved;
        try {
            eventConsumer.waitFiredEvent(childNodeRemoved, j -> nodeId == (int) j.query(NODE_ID_Q));

        } catch (PollEventException e) {
            throw new ProtocolEventException(String.format("Event failure. Try to use a single " +
                            "org.punch.context.entities.Locate instance before Tab#domNodeRemoved call. " +
                            "See Tab#domNodeInserted documentation for details. [evt=%s, nodeId=%d]",
                    childNodeRemoved, nodeId));
        }
    }

    /**
     * Returns a service which emulates web browser behaviour like it's running on a mobile device.
     * <p>
     * NB! You must use {@link HtmlNode#touch()} method, if you are in an mobile device emulated mode,
     * as {@link HtmlNode#click()} might lead to some unexpected outcome.
     * <p>
     * I would also recommend to set a corresponding web browser window size via @link ContextBuilder#setWindowSize},
     * cause sometimes window size might affect web application behaviour. Couple of window size defaults are here:
     * {@link DeviceScreenSize})
     *
     * @return emulate mobile device service.
     */
    public Emulate emulate() {
        this.inEmulatedMode = true;
        return new Emulate(getEmulation());
    }

    /**
     * Resets all the emulated web browser page settings. I would suggest to reload the page once the emulation
     * state has been reset to get rid of mobile view related HTML templates, but this is up to you, dude.
     */
    public void emulateReset() {
        Emulation emulation = getEmulation();
        emulation.resetPageScaleFactor();
        emulation.clearGeolocationOverride();
        emulation.clearDeviceMetricsOverride();
        emulation.setTouchEmulationEnabled(false, 1);
        emulation.setEmitTouchEventsForMouse(false, MouseTouchEventsConfig.desktop);
        emulation.setUserAgentOverride("", null, null, null);
        getOverlay().setShowViewportSizeOnResize(true);
        this.inEmulatedMode = false;
    }

    /* Use it, if you need to download a file and you would like to monitor the whole process. */
    public Download expectDownload() {
        return new Download(frameId, eventConsumer);
    }

    /**
     * Waits for new tab to be opened within current context.
     * <p>
     * The thing is that from a technical perspective it doesn't matter whether it's a new tab or a new window.
     * So this method can be used for any new window scenario. I will implement 'expectNewWindowOpen' later to
     * avoid further confusion of a QA person (if they will use it in the future, of course).
     * <p>
     * Yes, one more thing: if you need to open one tab and then do some actions with it, go ahead and use
     * {@link Tab#expectNewTabOpen()}. But if you need to click on a number of links to open each in a new tab
     * one by one like an insane person, then I would recommend to specify the whole url or a part of it, which
     * precisely identifies the new tab address. Otherwise I cannot guarantee that you will get targetId values
     * in the same order you clicked tabs, cause DevTools doesn't guarantee that (as I see it from practice).
     * <p>
     * NB! This method forces new opened tab to be reloaded. So, if this behaviour (I mean, reloading page,
     * which was recently opened in a new tab via an external link, window.open() etc) is not acceptable,
     * then DO NOT use this method. Use {@link BrowserContext#getTargetsByType(TargetType)} with
     * {@link TargetType#TAB} as a parameter to get all target IDs of the 'page' type and then switch to
     * required one via {@link BrowserContext#switchToTarget(String, Class)} method.
     *
     * @param urlPart Url address or a part of url address to identify new tab address.
     * @return new tab targetId.
     */
    public String expectNewTabOpen(String urlPart) {
        try {
            var tgtCreatedEvt = eventConsumer.getFiredEvent(targetCreated, j -> "page".equals(j.query(TARGET_TYPE_Q)));
            var targetId = (String) tgtCreatedEvt.query(TARGET_ID_Q);
            eventConsumer.waitFiredEvent(targetInfoChanged, targetInfoChangedRule(targetId, urlPart));

            return targetId;

        } catch (PollEventException e) {
            throw new ProtocolEventException(String.format("Event failure. [evt=%s]", targetCreated.method()), e);
        }
    }

    /**
     * Same as above, but no url specified.
     *
     * @return new tab targetId.
     */
    public String expectNewTabOpen() {
        return expectNewTabOpen(null);
    }

    /**
     * This is Animation tab representation.
     *
     * @return enabled animation tab view.
     */
    public AnimationsView getAnimationView() {
        Class<AnimationsView> type = ANIMATION_VIEW.type();

        if (!tabViews.containsKey(ANIMATION_VIEW)) {
            tabViews.put(ANIMATION_VIEW, new AnimationsView(getAnimation(), eventConsumer, jsonConverter));
        }

        return type.cast(tabViews.get(ANIMATION_VIEW));
    }

    /**
     * This is Console tab representation in read-only mode. Everything which is printed into Console tab
     * can viewed via this method.
     *
     * @return enabled console tab view.
     */
    public LogView getLogView() {
        Class<LogView> type = LOG_VIEW.type();

        if (!tabViews.containsKey(LOG_VIEW)) {
            tabViews.putIfAbsent(LOG_VIEW, new LogView(getLog(), getRuntime(), eventConsumer, jsonConverter));
        }

        return type.cast(tabViews.get(LOG_VIEW));
    }

    /**
     * This is Network tab representation.
     *
     * @return enabled network tab view.
     */
    public NetworkView getNetworkView() {
        Class<NetworkView> type = NETWORK_VIEW.type();

        if (!tabViews.containsKey(NETWORK_VIEW)) {
            getNetwork();
            tabViews.putIfAbsent(NETWORK_VIEW, new NetworkView(this));
        }

        return type.cast(tabViews.get(NETWORK_VIEW));
    }

    /**
     * Opens given resource address. If resource requires authentication and {@link #authenticate} method is triggered,
     * then this method sends initial request, then waits until {@link #authenticate} does its job, and then proceeds.
     *
     * @param url            resource url.
     * @param referrer       referrer url.
     * @param transitionType intended {@link TransitionType}.
     * @param frameId        frameId to navigate.
     * @param referrerPolicy {@link ReferrerPolicy}.
     * @throws BrowserSessionException if tab fails to open (due to the auth or DevTools error messages).
     */
    public void open(String url,
                     String referrer,
                     TransitionType transitionType,
                     String frameId,
                     ReferrerPolicy referrerPolicy) {

        getPage().navigate(url, referrer, transitionType, frameId, referrerPolicy);

        if (authTaskResult != null) {
            try {
                authTaskResult.get();
                authTaskResult = null;

            } catch (InterruptedException | ExecutionException e) {
                Throwable cause = e.getCause();

                if (Objects.nonNull(cause) && cause.getClass().equals(HttpResponseErrorException.class)) {
                    throw new HttpResponseErrorException(cause.getMessage());
                }

                throw new BrowserSessionException("Authentication cannot be finished. [url=" + url + "]", e);
            }
        }

        updateLoaderId();
        firstNavPassed = true;
    }

    /* Opens URL with Devtools protocol defined default params. */
    public void open(String url) {
        open(url, null, null, null, null);
    }

    /**
     * Prints page to PDF. Works in headless mode only.
     *
     * @param path path to pdf.
     */
    public void printToPDF(Path path) {
        Browser browser = getBrowser();
        var args = browser.getBrowserCommandLine();

        if (!args.contains("--headless")) {
            throw new BrowserSessionException("Print to PDF works in headless mode only. And yes, I don't know " +
                    "the reason - this is how Google set it, so, just deal with it, dude, as I did.");
        }

        var handle = getPage().printToPDF(false, false, false, 1, null, null, null, null,
                null, null, "", false, null, null, null, "ReturnAsStream");

        try (FileOutputStream os = new FileOutputStream(path.toFile())) {
            IO.Read read;
            int offset = 0;
            final int size = 16384;

            for (;;) {
                read = getIO().read(handle, offset, size);
                var data = read.getData();
                byte[] curr = read.isBase64Encoded() ? decodeStrToBytes(data) : data.getBytes(StandardCharsets.UTF_8);
                os.write(curr);

                if (read.isEof()) {
                    os.flush();
                    break;
                }

                offset += curr.length;
            }
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot write to given path.", e);

        } finally {
            getIO().close(handle);
        }
    }

    /**
     * Retrieves text data from clipboard. Pay attention, that if you run several browser instances in parallel mode
     * within common workspace, then the clipboard will be common for all browser instances. I just want to say - don't
     * do it, cause you will be able to read only the latest clipboard record.
     *
     * @return clipboard content or {@code null}.
     */
    public String readFromClipboard() {
        Browser browser = getBrowser();
        Browser.PermissionDescriptor readPerm = Browser.PermissionDescriptor.of(CLIPBOARD_READ, true);
        Browser.PermissionDescriptor writePerm = Browser.PermissionDescriptor.of(CLIPBOARD_WRITE, true);

        browser.setPermission(readPerm, granted, null, null);
        browser.setPermission(writePerm, granted, null, null);

        FunctionResult functionResult = getRuntimeJS().evaluate(
                "await navigator.clipboard.readText()",
                "console",
                null, null,
                null, null,
                true, true,
                true);

        getRuntimeJS().checkFunctionResultErrors(functionResult);
        browser.setPermission(readPerm, denied, null, null);
        browser.setPermission(writePerm, denied, null, null);

        return (String) functionResult.getResult().getValue();
    }

    /* Reloads current page. */
    public void reload() {
        //Remove all saved events to reset page state:
        eventConsumer.clearAllQueues();
        //Do the reload:
        getPage().reload(false, null);
        //Set firstNavPassed to false, otherwise
        // we won't be able to update loaderId:
        resetFirstNavPassed();
        updateLoaderId();
        //Set tab back to loaded state:
        firstNavPassed = true;
    }

    /* Well, to make a screenshot of a page. */
    public Screenshot screenshot() {
        return new Screenshot(this);
    }

    /**
     * Executes scroll. Tab scroll is always absolute. So, tracking current scroll position is a client responsibility.
     * Yeah, I didn't promise it would be easy, but I trust your experience, dude.
     *
     * @param deltaY number of pixels to be scrolled (if value is negative, it scrolls up).
     */
    @Override
    public void scroll(int deltaY) {
        getInput().dispatchMouseEvent(mouseWheel, 0, 0, 0, none, 0, 0, deltaY);
    }

    /*I think it's self-speaking. */
    public void setBypassCSP(boolean enabled) {
        getPage().setBypassCSP(enabled);
    }

    /* Sets given HTML content for this tab. */
    public void setDocumentContent(String html) {
        getPage().setDocumentContent(frameId, html);
    }

    /**
     * Waits for a page to be navigated within a document. Use it when in-page navigation happens without requesting
     * new resource.
     *
     * @see ContextConfig#getAwaitEventTimeout() for default timeout value.
     */
    public void waitNavigatedWithinDocument() {
        try {
            eventConsumer.waitFiredEvent(navigatedWithinDocument, j -> frameId.equals(j.query(FRAME_ID_Q)));

        } catch (PollEventException e) {
            throw new ProtocolEventException("Event failure. [evt=" + navigatedWithinDocument.method() + "]", e);
        }
    }

    /**
     * Wait until Page.lifecycleEvent 'load' is fired. Works in most cases. By typing that I mean that it should be
     * enough for most of the cases. But if your web app depends hardly on AJAX, you should probably stick to
     * {@link Tab#waitPageLoadNetworkIdle()}.
     * <p>
     * Lifecycle Event Load (https://developer.mozilla.org/en-US/docs/Web/API/Window/load_event)
     */
    public void waitPageLoad() {
        updateLoaderId();
        resetFirstNavPassed();
        try {
            var loadEvt = eventConsumer.getFiredEvent(lifecycleEvent, loadRule(frameId, loaderId, ts));
            updateTimestamp(loadEvt);

            if (logger.isDebugEnabled()) {
                logger.debug("Page lifecycle event fired. [event={}, url={}, frameId={}, loaderId={}]",
                        loadEvt.query(NAME_Q), getUrl(), frameId, loaderId);
            }

        } catch (PollEventException e) {
            throw new ProtocolEventException(String.format("Event failure. [evt=%s, name=%s]",
                    lifecycleEvent.method(), "load"), e);
        }
    }

    /**
     * Wait until Page.lifecycleEvent 'load' and Page.lifecycleEvent 'networkIdle' are fired. Use it when you
     * need all the page resources to be loaded.
     * <p>
     * NB! Main thing to keep in mind: Page.networkIdle event is a tricky one. For instance, if a document doesn't have
     * any resources to be requested via network, then this event could be fired before the protocol starts to listen
     * to lifecycle events, and so it could be missed and the method fails with timeout exception. Or, as an opposite
     * example, if a document has a bunch of background jobs, that require network communication, it could take a lot
     * of time to wait for all these jobs to finish, and it doesn't make any sense to wait for them all, cause the
     * document view has already been loaded and ready to be tested.
     * <p>
     * So, as a rule of thumb:
     * <li>
     * if your document don't have any AJAX communication, use {@link #waitPageLoad()};
     * </li>
     * <li>
     * if your document has a number of background AJAX tasks, but they don't affect the document view,
     * use {@link #waitPageLoad()};
     * </li>
     * <li>
     * in all other cases this method is preferable (but, dude, please be reasonable).
     * </li>
     */
    public void waitPageLoadNetworkIdle() {
        updateLoaderId();
        resetFirstNavPassed();

        var evtNames = new ArrayList<>(LOAD_PAGE_EVENTS);
        int countdown = evtNames.size();
        double tsInit = ts;

        while (countdown > 0) {
            try {
                var evt = eventConsumer.getFiredEvent(lifecycleEvent, eventsFiredRule(evtNames, frameId, loaderId, tsInit));
                var evtName = (String) evt.query(NAME_Q);
                evtNames.remove(evtName);
                updateTimestamp(evt);
                countdown--;

                if (logger.isDebugEnabled()) {
                    logger.debug("Page lifecycle event fired. [event={}, url={}, frameId={}, loaderId={}, left={}]",
                            evtName, getUrl(), evt.query(FRAME_ID_Q), evt.query(LOADER_ID_Q), countdown);
                }
            } catch (PollEventException e) {
                //Remove brackets from list string representation:
                var eventsLeft = evtNames.toString().replace("[", "").replace("]", "");

                throw new ProtocolEventException(String.format("Event failure. [evt=%s, name(s)=%s]",
                        lifecycleEvent.method(), eventsLeft), e);
            }
        }
    }

    /* Package-private methods */

    /**
     * Returns {@code true} if the tab is in the emulated mode.
     * Used by {@link HtmlNode} to check allowed user gestures
     * (mouse or touch).
     *
     * @return whether the tab is in emulated mode.
     */
    boolean isInEmulatedMode() {
        return inEmulatedMode;
    }

    /* Private methods */

    private Page.NavigationHistory getHistory() {
        return getPage().getNavigationHistory();
    }

    private Page.Frame getNavigatedFrame() {
        try {
            var frameNavEvt = eventConsumer.getFiredEvent(frameNavigated, j -> frameId.equals(j.query(PAGE_FRAME_ID_Q)));
            var frameJson = frameNavEvt.query(PAGE_FRAME_Q);
            return jsonConverter.toBean(frameJson.toString(), Page.Frame.class);

        } catch (PollEventException e) {
            throw new ProtocolEventException(String.format("Event failure. [evt=%s]", frameNavigated.method()), e);
        }
    }

    private void resetFirstNavPassed() {
        if (firstNavPassed) {
            firstNavPassed = false;
        }
    }

    /**
     * Yes, this method FORCES THE DOCUMENT TO BE RELOADED. Why? I'll tell you why.
     * <p>
     * The thing is, that when a document is opened via window.open(), external link click, etc. - so, all the cases,
     * except the normal one, when you submit an url in a browser (here it's {@link #open(String)} method), then
     * there is a time gap (from the protocol perspective) between the new page to be created and forcing specific
     * domain methods to be fired (Page.frameNavigated, for instance). Problem is that these methods might be lost
     * within this time gap, but I really need these events to be caught, cause they have all the required information
     * about new page.
     * <p>
     * Workaround for this is to reload the page. In this case, all the domain methods, which are responsible for
     * firing page-load related events have already been executed, and no events can be missed.
     */
    private void setNewTabLoaderId() {
        //Disable page to stop events publishing:
        getPage().disable();
        //Clear Page.frameNavigated queue:
        eventConsumer.pollAllEvents(frameNavigated);
        //Re-enable page:
        getPage().enable();
        getPage().setLifecycleEventsEnabled(true);
        //do reload to get loaderId:
        getPage().reload(false, null);
        updateLoaderId();
        firstNavPassed = true;
    }

    private void updateLoaderId() {
        if (firstNavPassed) {
            return;
        }

        var ldrId = getNavigatedFrame().getLoaderId();

        if (Objects.isNull(loaderId)) {
            loaderId = ldrId;

            if (logger.isDebugEnabled()) {
                logger.debug("LoaderId set. [loaderId={}, frameId={}]]", loaderId, frameId);
            }

        } else {
            if (Objects.equals(loaderId, ldrId)) {
                return;
            }

            this.loaderId = ldrId;

            if (logger.isDebugEnabled()) {
                logger.debug("LoaderId updated. [loaderId={}, frameId={}]", loaderId, frameId);
            }
        }
    }

    private void updateTimestamp(JSONObject evt) {
        //It's excessive too, but, you know:
        double ts;
        if (this.ts < (ts = (double) evt.query(TIMESTAMP_Q))) {
            this.ts = ts;

            if (logger.isDebugEnabled()) {
                var method = evt.get("method");

                if (method.equals(lifecycleEvent.method())) {
                    logger.debug("Timestamp updated. [ts={}, method={}, name={}]",
                            ts, method, evt.query(NAME_Q));
                }

                if (method.equals(requestWillBeSent.method())) {
                    logger.debug("Timestamp updated. [ts={}, method={}, requestId={}]",
                            ts, method, evt.query(REQUEST_ID_Q));
                }
            }
        }
    }

    /* Inner classes */

    /**
     * Animations domain wrapper. Can be found in Chrome DevTools menu (three vertical dots in right top corner) ->
     * More tools -> Animations. For corresponding animation events check Protocol monitor tab (in the same tab).
     */
    public static class AnimationsView {

        private static final String ID_Q = "/params/id";

        private final Animation animation;
        private final EventConsumer eventConsumer;
        private final JsonConverter jsonConverter;

        public AnimationsView(Animation animation, EventConsumer eventConsumer, JsonConverter jsonConverter) {
            this.animation = animation;
            this.eventConsumer = eventConsumer;
            this.jsonConverter = jsonConverter;
        }

        /**
         * Turn Animations off.
         */
        public void disable() {
            animation.disable();
        }

        /**
         * Pauses animation if {@code paused} set to {@code true}. {@code false} un-pauses animation, if it's paused.
         *
         * @param animationObj single animation object to be paused (or un-paused);
         * @param paused       pauses animation if {@code true}.
         */
        public void pauseAnimation(Animation.AnimationObject animationObj, boolean paused) {
            animation.setPaused(getAnimationIds(List.of(animationObj)), paused);
        }

        /**
         * Pauses animation objects if {@code paused} set to {@code true}. {@code false} un-pauses animation,
         * if it's paused.
         *
         * @param animations list of animation objects be paused (or un-paused);
         * @param paused     pauses animation if {@code true}.
         */
        public void pauseAnimationList(List<Animation.AnimationObject> animations, boolean paused) {
            animation.setPaused(getAnimationIds(animations), paused);
        }

        /**
         * Releases animation object from memory, once animation is finished.
         *
         * @param animationObj single animation object.
         */
        public void releaseAnimation(Animation.AnimationObject animationObj) {
            animation.releaseAnimations(getAnimationIds(List.of(animationObj)));
        }

        /**
         * Releases animation objects from memory, once all animations are finished.
         *
         * @param animations list of animation objects.
         */
        public void releaseAnimationList(List<Animation.AnimationObject> animations) {
            animation.releaseAnimations(getAnimationIds(animations));
        }

        /**
         * Waits for {@code Animation.animationCanceled} event to be fired for given animation object.
         *
         * @param animationObj animation object expected to be canceled.
         * @throws ProtocolEventException if event wasn't fired.
         */
        public void waitAnimationCanceled(Animation.AnimationObject animationObj) {
            try {
                eventConsumer.waitFiredEvent(animationCanceled, j -> animationObj.getId().equals(j.query(ID_Q)));

            } catch (PollEventException e) {
                throw new ProtocolEventException(String.format("Event failure. [evt=%s]", animationCanceled.method()));
            }
        }

        /**
         * Waits for {@code Animation.animationCanceled} event to be fired for a list of animations.
         *
         * @param animations list of animation objects expected to be canceled.
         * @throws ProtocolEventException if event wasn't fired for any of given animations.
         */
        public void waitAnimationListCanceled(List<Animation.AnimationObject> animations) {
            List<String> animationIdsMutable = getAnimationIds(animations);

            while (!animationIdsMutable.isEmpty()) {
                try {
                    var animationCanceledEvt = eventConsumer.getFiredEvent(animationCanceled, null);
                    var animationId = animationCanceledEvt.query(ID_Q).toString();
                    animationIdsMutable.remove(animationId);

                } catch (PollEventException e) {
                    var animationsLeft = animations.stream()
                            .filter(animation -> animationIdsMutable.contains(animation.getId()))
                            .map(Animation.AnimationObject::getName)
                            .collect(Collectors.toList());

                    throw new ProtocolEventException(String.format("Event failure. [evt=%s, animationsLeft=%s]",
                            animationCanceled.method(), animationsLeft), e);
                }
            }
        }

        /**
         * Waits for {@code Animation.animationStarted} event to be fired for given animation name.
         *
         * @param animationName name of started animation.
         * @throws ProtocolEventException if event wasn't fired.
         */
        public Animation.AnimationObject waitAnimationStarted(String animationName) {
            try {
                var evt = eventConsumer.getFiredEvent(animationStarted, j -> animationName.equals(j.query(ANIMATION_NAME_Q)));
                var animationJson = evt.query(ANIMATION_Q).toString();
                return jsonConverter.toBean(animationJson, Animation.AnimationObject.class);

            } catch (PollEventException e) {
                throw new ProtocolEventException(String.format("Event failure. [evt=%s]", animationStarted.method()), e);
            }
        }

        /**
         * Waits for {@code Animation.animationStarted} event to be fired for a list of animation names.
         *
         * @param animationNames list of animation names expected to be started.
         * @throws ProtocolEventException if event wasn't fired for any of given animation names.
         */
        public List<Animation.AnimationObject> waitAnimationListStarted(List<String> animationNames) {
            List<String> animationNamesMutable = new ArrayList<>(animationNames);
            List<Animation.AnimationObject> animations = new ArrayList<>();

            while (!animationNamesMutable.isEmpty()) {
                try {
                    var evt = eventConsumer.getFiredEvent(animationStarted, null);
                    var animationName = evt.query(ANIMATION_NAME_Q).toString();

                    if (animationNamesMutable.contains(animationName)) {
                        var animationJson = evt.query(ANIMATION_Q).toString();
                        animations.add(jsonConverter.toBean(animationJson, Animation.AnimationObject.class));
                        animationNamesMutable.remove(animationName);
                    }

                } catch (PollEventException e) {
                    throw new ProtocolEventException(String.format("Event failure. [evt=%s, animationsLeft=%s]",
                            animationStarted.method(), animationNamesMutable), e);
                }
            }

            return animations;
        }

        private List<String> getAnimationIds(List<Animation.AnimationObject> animationObjects) {
            return animationObjects.stream()
                    .map(Animation.AnimationObject::getId)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Network domain wrapper. Represents DevTools Network tab.
     */
    public static class NetworkView {

        private static final double SKIP_TS_CHECK_VALUE = -1;

        /**
         * List of Network.loadingFinished events for active tab. Why do we need this?
         * <p>
         * Here is the problem: some events are fired in order they were triggered, and some events - don't.
         * <p>
         * Example: 'Network.requestWillBeSent' events happens in order they were triggered. And if we have
         * two network requests, 'requestWillBeSent' events will be fired in the same order. But it's not true
         * for 'loadingFinished' events, cause loading depends on how soon the response will arrive. So, if we
         * just read events one by one from queue, we will definitely read both 'requestWillBeSent' events
         * (for both requests), but if response for the second request will arrive earlier, we will loose it
         * in case we execute this method twice - one execution for each request.
         * <p>
         * So, we store all Network.loadingFinished events here just in case they arrive earlier than expected.
         * And first we check here, and if there are no matched events, we start polling from a corresponding queue.
         */
        private final List<String> lfReqIds = new ArrayList<>();

        private final Tab tab;
        private final Network network;

        private NetworkView(Tab tab) {
            this.tab = tab;
            this.network = tab.getNetwork();
        }

        /* Enables (activates) Network tab. You will need this only if you disable Network domain first. */
        public void enable() {
            network.enable(null, null, null);
        }

        /* Disables (de-activates) Network domain. No events will be fired once you execute this method. */
        public void disable() {
            network.disable();
            lfReqIds.clear();
        }

        /* Clears browser cache. */
        public void clearCache() {
            network.clearBrowserCache();
        }

        /* Clears browser cookies. */
        public void clearCookies() {
            network.clearBrowserCookies();
        }

        /* Returns a list of current tab cookies. */
        public List<Network.Cookie> getAllCookies() {
            return network.getAllCookies();
        }

        /**
         * Returns a list of current tab cookies for a list of URLs for which applicable cookies will be fetched.
         * If not specified, all the tab's cookies will be returned.
         *
         * @param urls list of URLs for which applicable cookies will be fetched.
         * @return list of cookies for this tab.
         */
        public List<Network.Cookie> getCookies(String... urls) {
            return network.getCookies(List.of(urls));
        }

        /* Returns a content for given request, specified by requestId. */
        public String getResponseBody(String requestId) {
            return network.getResponseBody(requestId);
        }

        /* Returns post data which is sent with given request specified by requestId. */
        public String getRequestPostData(String requestId) {
            return network.getRequestPostData(requestId);
        }

        /**
         * Sets a cookie for this tab.
         *
         * @param cookieParam cookie parameters.
         * @return {@code true} if cookie was set successfully.
         */
        public boolean setCookie(Network.CookieParam cookieParam) {
            return network.setCookie(
                    cookieParam.getName(),
                    cookieParam.getValue(),
                    cookieParam.getUrl(),
                    cookieParam.getDomain(),
                    cookieParam.getPath(),
                    cookieParam.getSecure(),
                    cookieParam.getHttpOnly(),
                    cookieParam.getSameSite(),
                    cookieParam.getExpires(),
                    cookieParam.getPriority());
        }

        /**
         * Sets a list of cookies for this tab.
         *
         * @param cookies list of cookies to be set.
         */
        public void setCookies(List<Network.CookieParam> cookies) {
            network.setCookies(cookies);
        }

        /**
         * Waits for the specified network request to be finished. By default, it doesn't check the timestamp of the
         * executed request. It means that if several requests was executed in a row, it just checks that these requests
         * were actually executed without verifying the execution order.
         *
         * @param url              request url. If 'checkFullAddress' set to {@code true}, then {@code url} value and
         *                         request's url must match, if you expect an execution to be successful.
         * @param method           HTTP request method.
         * @param checkFullAddress if set to true, then it checks for full url address in request.
         * @param checkTimestamp   if set to true, then timestamp of the request is also checked. And if the request
         *                         timestamp is less than the current {@link Tab#ts} value, then this request won't be
         *                         considered as the one that needs to be awaited.
         * @return request id.
         * @throws ProtocolEventException if await event failed for {@code Network.requestWillBeSent}
         *                                or {@code Network.loadingFinished}.
         * @throws FailedRequestException if {@code Network.loadingFailed} event fired.
         */
        public String waitRequestFinished(String url,
                                          HttpMethod method,
                                          boolean checkFullAddress,
                                          boolean checkTimestamp) {
            //Request Will Be Sent:
            JSONObject rwbsEvt;
            try {
                double ts = checkTimestamp ? tab.ts : SKIP_TS_CHECK_VALUE;
                rwbsEvt = tab.eventConsumer.getFiredEvent(requestWillBeSent,
                        rwbsRule(url, method, checkFullAddress, tab.frameId, tab.loaderId, ts));

            } catch (PollEventException e) {
                throw new ProtocolEventException(String.format("Event failure. [evt=%s]", requestWillBeSent.method()), e);
            }

            tab.updateTimestamp(rwbsEvt);
            var requestId = (String) rwbsEvt.query(REQUEST_ID_Q);

            if (logger.isDebugEnabled()) {
                logger.debug("'requestWillBeSent' fired. [requestId={}, url={}, method={}, ts={}]",
                        requestId, rwbsEvt.query(REQUEST_URL_Q), rwbsEvt.query(REQUEST_MTD_Q), tab.ts);
            }

            //Monitor for 'loadingFailed' event:
            var isLoadingFailed = new AtomicBoolean(false);
            var loadFailedMonitor = tab.threadPool.submit(() -> {
                try {
                    tab.eventConsumer.waitFiredEvent(loadingFailed, j -> requestId.equals(j.query(REQUEST_ID_Q)));

                } catch (PollEventException e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Poll event canceled. [evt={}]", loadingFailed.method());
                    }

                    return;
                }

                isLoadingFailed.compareAndSet(false, true);
            });

            //Loading Finished:
            if (lfReqIds.contains(requestId)) {

                if (logger.isDebugEnabled()) {
                    logger.debug("'loadingFinished' already fired. [requestId={}].", requestId);
                }

                loadFailedMonitor.cancel(true);
                lfReqIds.remove(requestId);

                return requestId;
            }

            while (true) {
                if (isLoadingFailed.get()) {
                    throw new FailedRequestException("Request failed to load. " +
                            "[requestId=" + requestId + "," +
                            " url=" + url + "]");
                }

                JSONObject lfEvt;
                try {
                    if (checkTimestamp) {
                        lfEvt = tab.eventConsumer.getFiredEvent(
                                loadingFinished,
                                j -> tab.ts <= Double.parseDouble(j.query(TIMESTAMP_Q).toString()));

                    } else {
                        lfEvt = ((BasicEventConsumer) tab.eventConsumer).getFiredEvent(loadingFinished);
                    }

                } catch (PollEventException e) {
                    throw new ProtocolEventException("Event failure. [evt=" + loadingFinished.method() + "]", e);
                }

                var lfReqId = (String) lfEvt.query(REQUEST_ID_Q);

                if (requestId.equals(lfReqId)) {

                    if (logger.isDebugEnabled()) {
                        logger.debug("'loadingFinished' fired. [requestId={}, ts={}].",
                                requestId, lfEvt.query(TIMESTAMP_Q));
                    }

                    loadFailedMonitor.cancel(true);

                    return requestId;
                }

                lfReqIds.add(lfReqId);

                if (logger.isDebugEnabled()) {
                    logger.debug("'LF' requestId saved. [requestId={}, ts={}]", lfReqId, lfEvt.query(TIMESTAMP_Q));
                }
            }
        }

        /**
         * Waits for a request to be finished. Doesn't check for full URL match and doesn't check the timestamp.
         * Can be considered as default 'waitUntilRequestFinished' method.
         *
         * @param url    request url. Only a part of the full url address can be specified
         *               (mail.yandex instead of https://mail.yandex.ru, for instance).
         * @param method HTTP request method.
         * @return requestId of awaited request.
         */
        public String waitRequestFinished(String url, HttpMethod method) {
            return waitRequestFinished(url, method, false, false);
        }
    }

    /* Screenshot wrapper */
    public static class Screenshot {

        private final Tab tab;

        Screenshot(Tab tab) {
            this.tab = tab;
        }

        public Captured capture(ImageFormat format, String query) {
            if (!tab.tabDomains.containsKey(PAGE)) {
                throw new NullPointerException("Tab is empty.");
            }

            //We don't need Emulation and Overlay steps for now, but it will be required in the future,
            // so I placed it here, cause I can forget to do this, when these are needed.
            Emulation emulation = tab.getEmulation();
            emulation.clearDeviceMetricsOverride();
            Overlay overlay = tab.getOverlay();
            overlay.enable();
            overlay.setShowViewportSizeOnResize(false);
            overlay.disable();

            //Tab must be activated, otherwise protocol won't send Page.captureScreenshot response
            // and a client will receive an exception:
            tab.bringToFront();
            String base64Img;

            if (Objects.nonNull(query)) {
                HtmlNode node = tab.locate().htmlNode(query);
                DOM.BoxModel boxModel;
                try {
                    boxModel = tab.getDOM().getBoxModel(node.getNodeId(), null, null);

                } catch (ProtocolResponseErrorException e1) {
                    try {
                        boxModel = tab.getDOM().getBoxModel(node.getFreshNodeId(), null, null);

                    } catch (ProtocolResponseErrorException e2) {
                        throw node.cannotInteractWithNodeEx(e2);
                    }
                }
                //Set screenshot area:
                int x = boxModel.getMargin().get(0);
                int y = boxModel.getMargin().get(1);
                int width = boxModel.getWidth();
                int height = boxModel.getHeight();
                Page.Viewport viewport = Page.Viewport.of(x, y, width, height, 1);
                //Scroll to area:
                node.scrollIntoView();

                base64Img = tab.getPage().captureScreenshot(format, null, viewport, true);

            } else {
                base64Img = tab.getPage().captureScreenshot(format, null, null, true);
            }

            if (Objects.isNull(base64Img)) {
                throw new NullPointerException("Unexpected screenshot capture error. Please submit a bug. Sorry.");
            }

            emulation.clearDeviceMetricsOverride();

            return new Captured(base64Img);
        }

        public Captured capture() {
            return capture(null, null);
        }

        public static class Captured {

            private final String base64Img;

            private Captured(String base64Img) {
                this.base64Img = base64Img;
            }

            public void asFile(Path path) {
                byte[] barr = decodeStrToBytes(base64Img);

                try (InputStream is = new ByteArrayInputStream(barr)) {
                    Files.copy(is, path, StandardCopyOption.REPLACE_EXISTING);

                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
            }

            public byte[] asBytes() {
                return decodeStrToBytes(base64Img);
            }
        }
    }

    /* DevTools Console tab domain wrapper */
    public static class LogView {

        private static final String PARAMS = "params";
        private static final String ENTRY_Q = "/params/entry";

        /* Browser log entries */
        private final Deque<Log.LogEntry> browserLogs = new ArrayDeque<>();

        /* Runtime console logs (simplified view) */
        private final Deque<ConsoleLog> consoleLogs = new ArrayDeque<>();

        private final Log log;
        private final Runtime runtime;
        private final EventConsumer eventConsumer;
        private final JsonConverter jsonConverter;

        public LogView(Log log, Runtime runtime, EventConsumer eventConsumer, JsonConverter jsonConverter) {
            this.log = log;
            this.runtime = runtime;
            this.eventConsumer = eventConsumer;
            this.jsonConverter = jsonConverter;
        }

        /**
         * Clears session log records. Similar to clicking on 'Clear console' button in DevTools Console tab.
         */
        public void clear() {
            log.clear();
            runtime.discardConsoleEntries();
            pullBrowserLogs();
            pullConsoleLogs();
            browserLogs.clear();
            consoleLogs.clear();
        }

        /**
         * Returns a list of console logs generated by a browser.
         *
         * @return list of browser log records.
         */
        public List<Log.LogEntry> getBrowserLogs() {
            pullBrowserLogs();
            return new ArrayList<>(browserLogs);
        }

        /**
         * Returns a list of console logs generated by a web application.
         *
         * @return list of web app logs.
         */
        public List<ConsoleLog> getConsoleLogs() {
            pullConsoleLogs();
            return new ArrayList<>(consoleLogs);
        }

        /**
         * Returns a list of all console records except the ones generated by a web browser
         * (browser logs can be fetched via {@link LogView#getBrowserLogs()} call).
         *
         * @return list of all console records except browser messages.
         */
        public List<ConsoleAPI> getAllConsoleAPICalls() {
            var consoleAPICalledEvents = eventConsumer.pollAllEvents(consoleAPICalled);

            if (Objects.nonNull(consoleAPICalledEvents)) {
                return consoleAPICalledEvents.stream()
                        .filter(e -> e.has(PARAMS))
                        .map(e -> jsonConverter.toBean(e.get(PARAMS).toString(), ConsoleAPI.class))
                        .collect(Collectors.toList());
            }

            return Collections.emptyList();
        }

        public Log.LogEntry getNewestBrowserLog() {
            pullBrowserLogs();
            return browserLogs.peekLast();
        }

        public ConsoleLog getNewestConsoleLog() {
            pullConsoleLogs();
            return consoleLogs.peekLast();
        }

        public Log.LogEntry getOldestBrowserLog() {
            pullBrowserLogs();
            return browserLogs.peekFirst();
        }

        public ConsoleLog getOldestConsoleLog() {
            pullConsoleLogs();
            return consoleLogs.peekFirst();
        }

        public void startViolationReport(List<Log.ViolationSetting> config) {
            log.startViolationsReport(config);
        }

        public void stopViolationReport() {
            log.stopViolationsReport();
        }

        private void pullBrowserLogs() {
            var entryAddedEvents = eventConsumer.pollAllEvents(entryAdded);

            if (Objects.isNull(entryAddedEvents)) {
                return;
            }

            entryAddedEvents.forEach(e -> {
                var entryStr = e.query(ENTRY_Q).toString();
                browserLogs.addLast(jsonConverter.toBean(entryStr, Log.LogEntry.class));
            });
        }

        private void pullConsoleLogs() {
            getAllConsoleAPICalls().forEach(api -> {
                //Collect messages:
                var messages = api.getArgs().stream()
                        .filter(o -> Objects.equals(RemoteObjectType.STRING.value(), o.getType()))
                        .map(o -> (String) o.getValue())
                        .collect(Collectors.toList());

                consoleLogs.addLast(ConsoleLog.of(api.getType(), messages, api.getTimestamp()));
            });
        }
    }

    private static byte[] decodeStrToBytes(String encoded) {
        return Base64.getDecoder().decode(encoded.getBytes(StandardCharsets.UTF_8));
    }
}
