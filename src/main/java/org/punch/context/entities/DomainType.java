/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.punch.devtools.protocol.animation.Animation;
import org.punch.devtools.protocol.browser.Browser;
import org.punch.devtools.protocol.domstorage.DOMStorage;
import org.punch.devtools.protocol.emulation.Emulation;
import org.punch.devtools.protocol.fetch.Fetch;
import org.punch.devtools.protocol.input.Input;
import org.punch.devtools.protocol.log.Log;
import org.punch.devtools.protocol.network.Network;
import org.punch.devtools.protocol.overlay.Overlay;
import org.punch.devtools.protocol.page.Page;
import org.punch.devtools.protocol.runtime.Runtime;

enum DomainType {
    ANIMATION(Animation.class),
    BROWSER(Browser.class),
    CSS(org.punch.devtools.protocol.css.CSS.class),
    DOM(org.punch.devtools.protocol.dom.DOM.class),
    DOM_STORAGE(DOMStorage.class),
    EMULATION(Emulation.class),
    FETCH(Fetch.class),
    INPUT(Input.class),
    IO(org.punch.devtools.protocol.io.IO.class),
    LOG(Log.class),
    NETWORK(Network.class),
    OVERLAY(Overlay.class),
    PAGE(Page.class),
    RUNTIME(Runtime.class);

    private final Class<?> type;

    DomainType(Class<?> type) {
        this.type = type;
    }

    @SuppressWarnings("unchecked")
    <T> Class<T> type() {
        return (Class<T>) type;
    }
}