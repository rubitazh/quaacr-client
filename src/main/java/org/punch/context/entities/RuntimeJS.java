/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.punch.context.ex.ScriptExecutionException;
import org.punch.devtools.protocol.runtime.Runtime;
import org.punch.devtools.protocol.runtime.ExceptionDetails;

import java.util.List;
import java.util.Objects;

class RuntimeJS {

    private final Runtime runtime;

    RuntimeJS(Runtime runtime) {
        this.runtime = runtime;
    }

    Runtime.FunctionResult callFunctionOn(String function,
                                          String objectId,
                                          List<Runtime.CallArgument> args,
                                          Boolean silent,
                                          Boolean returnByValue,
                                          Boolean generatePreview,
                                          Boolean userGesture,
                                          Boolean awaitPromise,
                                          Integer executionContextId,
                                          String objectGroup) {

        return runtime.callFunctionOn(
                function,
                objectId,
                args,
                silent,
                returnByValue,
                generatePreview,
                userGesture,
                awaitPromise,
                executionContextId,
                objectGroup);
    }

    void checkFunctionResultErrors(Runtime.FunctionResult functionResult) {
        ExceptionDetails ex = functionResult.getExceptionDetails();

        if (Objects.nonNull(ex)) {
            throw new ScriptExecutionException("Script threw exception. " +
                    "[ex=" + ex.getException() + ", msg=" + ex.getText() + "]");
        }

        String className;
        if ((className = functionResult.getResult().getClassName()) == null) {
            return;
        }

        if (className.equals("Error")) {

            throw new ScriptExecutionException("Cannot execute function. " +
                    "[error=" + functionResult.getResult().getDescription() + "]");
        }

        if (functionResult.getResult().getObjectId() == null) {
            throw new ScriptExecutionException("Script execution result is null.");
        }
    }

    Runtime.FunctionResult evaluate(String expression,
                                    String objectGroup,
                                    Integer contextId,
                                    Boolean includeCommandLineAPI,
                                    Boolean silent,
                                    Boolean generatePreview,
                                    Boolean userGesture,
                                    Boolean awaitPromise,
                                    Boolean replMode) {

        return runtime.evaluate(
                expression,
                objectGroup,
                contextId,
                includeCommandLineAPI,
                silent,
                generatePreview,
                userGesture,
                awaitPromise,
                replMode);
    }

    void releaseObject(String objectId) {
        runtime.releaseObject(objectId);
    }
}
