/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.json.JSONObject;
import org.punch.context.ex.HttpResponseErrorException;
import org.punch.devtools.ex.PollEventException;
import org.punch.devtools.impl.BasicEventConsumer;
import org.punch.devtools.interfaces.EventConsumer;
import org.punch.devtools.protocol.fetch.Fetch;
import org.punch.devtools.protocol.network.Network;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.punch.context.entities.EventRules.*;
import static org.punch.devtools.protocol.fetch.Fetch.Event.*;
import static org.punch.devtools.protocol.fetch.Fetch.Response.*;
import static org.punch.devtools.protocol.network.Network.Event.*;

/**
 * This class represents Basic authentication workflow for a given {@link Tab} instance. Actually, you can always pass
 * Basic Auth credentials as a part of URL address, but this is not secure (although who cares,  if these are tests),
 * so it's up to you, dude.
 */
public class BasicAuth {

    private static final Logger logger = LoggerFactory.getLogger(BasicAuth.class);

    private static final String REDIRECT_RESPONSE_Q = "/params/redirectResponse";
    private static final String RESPONSE_URL = "/params/response/url";
    private static final String RESPONSE_STATUS_Q = "/params/response/status";
    private static final String RESPONSE_STATUS_TXT_Q = "/params/response/statusText";

    /* Number of parallel response listeners (tasks) to be executed */
    private static final int RESPONSE_LISTENERS_NUM = 5;

    //Requests matching this pattern will produce fetchRequested event and will be paused until clients response.
    private final List<Fetch.RequestPattern> requestPattern = List.of(Fetch.RequestPattern.of("*"));

    //Several flags to select the way code must follow:
    private final AtomicBoolean needsAuth = new AtomicBoolean(false);
    private final AtomicBoolean loadFailed = new AtomicBoolean(false);
    private final AtomicBoolean respReceived = new AtomicBoolean(false);

    private final String frameId;
    private final Network network;
    private final Fetch fetch;
    private final EventConsumer consumer;
    private final ExecutorService threadPool;

    BasicAuth(String frameId, Network network, Fetch fetch, EventConsumer consumer, ExecutorService threadPool) {
        this.frameId = frameId;
        this.network = network;
        this.fetch = fetch;
        this.consumer = consumer;
        this.threadPool = threadPool;
    }

    /**
     * This method does all the basic authentication work. It's kind of classic JS style code (cause we use Fetch),
     * so it's a bit messy, but it has a bunch of comments to make it more friendly to anyone who needs to look into
     * it (obviously, this is me).
     *
     * @param pocket username/password pair.
     * @return authentication task future result.
     */
    Future<?> authenticate(AuthPocket pocket) {
        network.setCacheDisabled(true);
        fetch.enable(requestPattern, true);

        // This is the main auth task.
        Runnable doAuthTask = () -> {
            //First we wait for the request to resource to be paused:
            JSONObject reqPausedEvt;
            try {
                reqPausedEvt = consumer.getFiredEvent(requestPaused, j -> frameId.equals(j.query(FRAME_ID_Q)));

            } catch (PollEventException e) {
                logger.error("Auth task stopped.", e);
                return;
            }

            //Actually, this is initial resource requestId, but this is how it's named in Fetch,
            // so I left original name.
            var networkId = (String) reqPausedEvt.query(NETWORK_ID_Q);

            //This is to make sure that all listeners are started prior to process auth request.
            var barrier = new CyclicBarrier(RESPONSE_LISTENERS_NUM);

            var loadingFailedMonitorTask = new LoadingFailedMonitor(networkId, barrier, this);
            var loadingFailedMonitorResult = threadPool.submit(loadingFailedMonitorTask);

            //Now we need to submit redirect task, which is responsible
            // for tracking redirect response. And if it actually happens,
            // it does all the redirect related work to proceed further.
            // Otherwise it must be canceled by this (doAuthTask) task.
            var redirectMonitorTask = new RedirectMonitor(networkId, barrier, this);
            var redirectMonitorResult = threadPool.submit(redirectMonitorTask);

            //Here we submit task, which tracks for initial response from resource.
            // We need it for status checks and to send a signal to other tasks,
            // that response is received (or not).
            var responseMonitorTask = new ResponseMonitor(networkId, barrier, this);
            var responseMonitorResult = threadPool.submit(responseMonitorTask);

            //This task tracks whether resource is actually requests for the authentication.
            // If it does, then it sends credentials, and if these are not valid ones, it
            // cancels authentication, and 401 status must be received by a client.
            var authRequiredMonitorTask = new AuthRequiredMonitor(pocket.getUser(), pocket.getPassword(), barrier, this);
            var authRequiredMonitorResult = threadPool.submit(authRequiredMonitorTask);

            //Continue paused request:
            barrierAwait(barrier);
            var interceptionId = (String) reqPausedEvt.query(REQUEST_ID_Q);
            fetch.continueRequest(interceptionId, null, null, null, null);

            long awaitResponseStart = System.currentTimeMillis();
            long awaitResponseTimeout = pocket.getAwaitResponseTimeout();
            //We are not allowed to disable Fetch before responseReceived event, cause if we do, response might stuck,
            // so we wait for the response or, if response doesn't arrive, will break by awaitResponseTimeout:
            for (;;) {
                if ((System.currentTimeMillis() - awaitResponseStart) >= awaitResponseTimeout) {

                    if (logger.isDebugEnabled()) {
                        logger.warn("If there are no exceptions, and it's your AUTH Server which is too damn slow, " +
                                "then try to increase await response timeout. [timeout={}]", awaitResponseTimeout);
                    }

                    break;
                }

                if (loadFailed.get()) {
                    break;
                }

                if (respReceived.get()) {
                    try {
                        responseMonitorResult.get();

                    } catch (InterruptedException | ExecutionException e) {
                        var cause = e.getCause();

                        if (Objects.nonNull(cause) && cause.getClass().equals(HttpResponseErrorException.class)) {
                            throw (HttpResponseErrorException) cause;
                        }

                        throw new HttpResponseErrorException("Cannot get auth response result.", e);
                    }

                    break;
                }
            }

            //Cancel tasks, that still might not be canceled:
            loadingFailedMonitorResult.cancel(true);
            redirectMonitorResult.cancel(true);
            authRequiredMonitorResult.cancel(true);
            //Enable network cache back, otherwise it will
            // affect lifecycle network-related events:
            network.setCacheDisabled(false);
            //Stop Fetch:
            fetch.disable();
            //Clear Fetch queue:
            consumer.pollAllEvents(requestPaused);
            //Just in case, we interrupt responseMonitor task to avoid any
            // 'thread is in hang state' situation:
            if (!responseMonitorResult.isDone()) {
                responseMonitorResult.cancel(true);
            }
        };

        return threadPool.submit(doAuthTask);
    }

    private void barrierAwait(CyclicBarrier cyclicBarrier) {
        try {
            cyclicBarrier.await();

        } catch (InterruptedException | BrokenBarrierException e) {
            throw new RuntimeException("Basic auth unexpected error.", e);
        }
    }

    private static class LoadingFailedMonitor implements Runnable {

        private final String requestId;
        private final CyclicBarrier barrier;
        private final BasicAuth parent;

        private LoadingFailedMonitor(String requestId, CyclicBarrier barrier, BasicAuth parent) {
            this.requestId = requestId;
            this.barrier = barrier;
            this.parent = parent;
        }

        public void run() {
            try {
                parent.barrierAwait(barrier);
                parent.consumer.waitFiredEvent(loadingFailed, j -> requestId.equals(j.query(REQUEST_ID_Q)));

            } catch (PollEventException e) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Loading failed monitor task canceled.");
                }

                return;
            }

            parent.loadFailed.compareAndSet(false, true);
        }
    }

    private static class RedirectMonitor implements Runnable {

        private final String requestId;
        private final CyclicBarrier barrier;
        private final BasicAuth parent;

        private RedirectMonitor(String requestId, CyclicBarrier barrier, BasicAuth parent) {
            this.requestId = requestId;
            this.barrier = barrier;
            this.parent = parent;
        }

        @Override
        public void run() {
            try {
                parent.barrierAwait(barrier);
                parent.consumer.waitFiredEvent(requestWillBeSent,
                        j -> requestId.equals(j.query(REQUEST_ID_Q)) && Objects.nonNull(j.query(REDIRECT_RESPONSE_Q)));

            } catch (PollEventException e) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Redirect monitor task canceled.");
                }

                return;
            }

            JSONObject requestPausedEvt;
            try {
                requestPausedEvt = parent.consumer.getFiredEvent(
                        requestPaused, j -> parent.frameId.equals(j.query(FRAME_ID_Q)));

            } catch (PollEventException e) {
                logger.error("Auth redirect task cancelled. By design, it shouldn't happen on this step, " +
                        "so probably this is an issue.", e);

                return;
            }

            var interceptionId = (String) requestPausedEvt.query(REQUEST_ID_Q);
            parent.fetch.continueRequest(interceptionId, null, null, null, null);
        }
    }

    private static class ResponseMonitor implements Runnable {

        private final String requestId;
        private final CyclicBarrier barrier;
        private final BasicAuth parent;

        private ResponseMonitor(String requestId, CyclicBarrier barrier, BasicAuth parent) {
            this.requestId = requestId;
            this.barrier = barrier;
            this.parent = parent;
        }

        public void run() {
            JSONObject responseReceivedEvt;
            try {
                parent.barrierAwait(barrier);
                responseReceivedEvt = parent.consumer.getFiredEvent(responseReceived, j -> requestId.equals(j.query(REQUEST_ID_Q)));

            } catch (PollEventException e) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Response monitor task canceled.");
                }

                return;
            }

            parent.respReceived.compareAndSet(false, true);

            //Process response:
            int status = (int) responseReceivedEvt.query(RESPONSE_STATUS_Q);

            if (status >= 400) {
                var url = responseReceivedEvt.query(RESPONSE_URL);
                var statusTxt = responseReceivedEvt.query(RESPONSE_STATUS_TXT_Q);

                throw new HttpResponseErrorException(String.format(
                        "Authentication response error. [status=%d, url=%s, text=%s]", status, url, statusTxt));
            }

            if (logger.isDebugEnabled() && status >= 200) {
                var url = responseReceivedEvt.query(RESPONSE_URL);
                var statusTxt = responseReceivedEvt.query(RESPONSE_STATUS_TXT_Q);

                logger.debug("User authenticated. [status={}, url={}, text={}]", status, url, statusTxt);
            }
        }
    }

    private static class AuthRequiredMonitor implements Runnable {

        private final List<String> attemptedAuthRequests = new ArrayList<>();
        private final String user;
        private final String password;
        private final CyclicBarrier barrier;
        private final BasicAuth parent;

        private AuthRequiredMonitor(String user, String password, CyclicBarrier barrier, BasicAuth parent) {
            this.user = user;
            this.password = password;
            this.barrier = barrier;
            this.parent = parent;
        }

        public void run() {
            parent.barrierAwait(barrier);

            for (;;) {
                if (parent.loadFailed.get() || parent.respReceived.get()) {
                    return;
                }

                JSONObject authRequiredEvt;
                try {
                    authRequiredEvt = ((BasicEventConsumer) parent.consumer).getFiredEvent(authRequired);
                    parent.needsAuth.compareAndSet(false, true);

                } catch (PollEventException e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Auth required monitor task canceled.");
                    }

                    return;
                }

                var requestId = (String) authRequiredEvt.query(REQUEST_ID_Q);

                Fetch.Response response;
                if (attemptedAuthRequests.contains(requestId)) {
                    response = CancelAuth;

                } else {
                    response = ProvideCredentials;
                    attemptedAuthRequests.add(requestId);
                }

                parent.fetch.continueWithAuth(requestId, Fetch.AuthChallengeResponse.of(response, user, password));
            }
        }
    }
}
