/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.punch.context.enums.PseudoElement;
import org.punch.devtools.protocol.dom.DOM;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This class kind of mirrors
 * <a href="https://chromedevtools.github.io/devtools-protocol/tot/DOM/#type-Node">DevTools DOM.Node</a> object,
 * just a bit less fields. OK, most of the fields are excluded, but all the important ones are here.
 * Simply speaking, this class tends to mirror actual DOM nodes with the fields, that might require for
 * UI automation (from my point of view).
 */
public class NodeInfo {

    private static final int TEXT_NODE = 3;

    private final DOM.Node node;

    public NodeInfo(DOM.Node node) {
        this.node = node;
    }

    /**
     * Returns a list of all node's attributes in a form of 'attribute-name' followed by 'attribute-value'.
     *
     * @return list of node's attributes.
     */
    public List<String> getAttributes() {
        return node.getAttributes();
    }

    /* Returns a list of DOM Node objects for all node's child elements. */
    public List<NodeInfo> getChildNodeInfos() {
        return new ArrayList<>(getChildNodeInfoInternal(node, null));
    }

    /* Returns the node's children total count. */
    public int getChildNodeCount() {
        return node.getChildNodeCount();
    }

    /* Returns node's local name. */
    public String getLocalName() {
        return node.getLocalName();
    }

    /* Returns node's node name. */
    public String getNodeName() {
        return node.getNodeName();
    }

    /* Returns node type. */
    public int getNodeType() {
        return node.getNodeType();
    }

    /* Returns node value or null, if no value present. */
    public String getNodeValue() {
        return node.getNodeValue();
    }

    /**
     * Returns the node's text node value (simply speaking, the text within an HTML tag). It has boolean parameter,
     * which defines what exactly will be returned to a client:
     * <ul>
     *     <li>
     *         if you set {@code true} as a parameter, the method will return this node text, plus, the text values
     *         of all the node's children (if any). So, if you have an HTML like this:
     *         <pre>{@code
     *         <div>div_text
     *             <span>span_text</span>
     *         </div>}
     *        </pre>
     *        and the flag is {@code true}, then this XPath expression {@code //div} will return both 'div_text'
     *        and 'span_text' separated by spaces;
     *     </li><p>
     *     <li>
     *         if the flag is {@code false}, only this node's text will be returned, which is 'div_text' in our case.
     *     </li>
     * </ul>
     *
     * @param includeChildren makes the method to pull all the children's text node values, if set to {@code true}.
     * @return text node value (or values if the param is set to {@code true}).
     */
    public String getText(boolean includeChildren) {
        if (node.getChildNodeCount() == 0) {
            return "";
        }

        if (includeChildren) {
            return getTextNodesValues(node);
        }

        DOM.Node textNode = node.getChildren().stream()
                .filter(n -> n.getNodeType() == TEXT_NODE)
                .findFirst()
                .orElse(null);

        return Objects.isNull(textNode) ? "" : textNode.getNodeValue();
    }

    /**
     * Returns this node's text node value (simply speaking, the text within an HTML tag), plus, the text values
     * of all the node's children (if any). If you want the children's text values to be excluded from the result,
     * check {@link NodeInfo#getText(boolean)} method.
     *
     * @return text node values for given node and all its children.
     */
    public String getText() {
        return getText(true);
    }

    /**
     * Checks whether the node contains given {@link PseudoElement} as a child node.
     *
     * @param pe pseudo-element to check.
     * @return {@code true} on success.
     */
    public boolean hasPseudoElement(PseudoElement pe) {
        List<DOM.Node> pseudoElements = node.getPseudoElements();
        if (Objects.nonNull(pseudoElements) && !pseudoElements.isEmpty())
            for (DOM.Node pseudoElement : pseudoElements) {
                if (pseudoElement.getNodeName().contains(pe.value())) {
                    return true;
                }
            }

        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj.getClass() != this.getClass()) {
            return false;
        }

        return Objects.equals(this.node, ((NodeInfo) obj).node);
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash = 53 * hash + (this.node != null ? this.node.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "{" +
                "\"nodeType\":\"" + getNodeType() + "\"," +
                "\"nodeName\":\"" + getNodeName() + "\"," +
                "\"localName\":\"" + getLocalName() + "\"," +
                "\"nodeValue\":\"" + getNodeValue() + "\"," +
                "\"attributes\":" + getAttributes() + "\"," +
                "\"childNodeCount\":\"" + getChildNodeCount() +
                "}";
    }

    int getBackendNodeId() {
        return node.getBackendNodeId();
    }

    boolean isPseudoType() {
        return Objects.nonNull(node.getPseudoType());
    }

    private List<NodeInfo> getChildNodeInfoInternal(DOM.Node parent, List<NodeInfo> nodeInfos) {
        if (parent.getChildNodeCount() == 0) {
            return nodeInfos;
        }

        nodeInfos = Objects.isNull(nodeInfos) ? new ArrayList<>() : nodeInfos;

        for (DOM.Node child : parent.getChildren()) {
            nodeInfos.add(new NodeInfo(child));
            getChildNodeInfoInternal(child, nodeInfos);
        }

        return nodeInfos;
    }

    private String getTextNodesValues(DOM.Node nd) {
        if (nd.getNodeType() == TEXT_NODE) {
            return nd.getNodeValue();
        }

        if (nd.getChildNodeCount() == 0) {
            return "";
        }

        var txt = "";
        for (DOM.Node child : nd.getChildren()) {
            txt = txt.concat(getTextNodesValues(child));
        }

        return txt;
    }
}
