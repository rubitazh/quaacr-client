/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.punch.context.ContextConfig;
import org.punch.context.enums.Keys;
import org.punch.context.ex.NodeLocationException;
import org.punch.context.ex.ProtocolEventException;
import org.punch.context.interfaces.Node;
import org.punch.context.interfaces.Scrollable;
import org.punch.devtools.ex.PollEventException;
import org.punch.devtools.ex.ProtocolResponseErrorException;
import org.punch.devtools.interfaces.EventConsumer;
import org.punch.devtools.protocol.css.CSS;
import org.punch.devtools.protocol.dom.DOM;
import org.punch.devtools.protocol.domstorage.DOMStorage;
import org.punch.devtools.protocol.input.Input;
import org.punch.devtools.protocol.runtime.Runtime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.punch.context.entities.EventRules.*;
import static org.punch.devtools.protocol.dom.DOM.*;
import static org.punch.devtools.protocol.dom.DOM.Event.attributeModified;
import static org.punch.devtools.protocol.domstorage.DOMStorage.Event.domStorageItemRemoved;
import static org.punch.devtools.protocol.input.Input.MouseButton.none;
import static org.punch.devtools.protocol.input.Input.MouseType.*;
import static org.punch.devtools.protocol.input.Input.TouchType.*;
import static org.punch.devtools.protocol.page.Page.*;
import static org.punch.devtools.protocol.runtime.Runtime.*;

/**
 * Represents basic HTML DOM element to be interacted with via standard actions like mouse left button click,
 * mouseover etc. Class also provides element's type, attributes and values etc (if any) and its current state
 * to a client.
 * Any option listed above can be executed if and only if the element is still presented on the page.
 * Once it's not the true, any attempt of interaction from a client will cause an exception to be thrown.
 */
public class HtmlNode implements Node, Scrollable {

    private static final Logger logger = LoggerFactory.getLogger(HtmlNode.class);

    /**
     * JSONObject query to get Window.sessionStorage item key value from
     * DevTools DOMStorage.domStorageItemRemoved event.
     */
    private static final String KEY_Q = "/params/key";

    /* Html text node type value. See https://www.w3schools.com/Jsref/prop_node_nodetype.asp.*/
    private static final int TEXT_NODE = 3;

    /* Number of moving iterations towards to element before mouse pointer is actually placed to element's center. */
    private static final byte MOUSE_POINTER_MOVE_COUNT = 1 << 3;

    /* Number of attempts to re-calculate center of clickable node when located node is a parent of this node. */
    private static final byte ADJUST_COORDINATES_COUNT = 1 << 4;

    /* To pass non-null value to log, if NodeInfo wasn't defined for this node. */
    private static final NodeInfo NOT_DEFINED_NODE_INFO = new NotFoundNodeInfo();

    /* To interact with JS runtime. */
    protected final RuntimeJS runtimeJS;

    /* List of all nodeIds ever assigned to this instance */
    private final SortedSet<Integer> nodeIdSet = new TreeSet<>();

    private final Session session;
    private final EventConsumer consumer;
    private final String query;
    private final int index;
    private final int foundTotal;
    private final boolean includeShadowDOM;

    /* Holds the latest existing node info (for exceptions and logging) */
    private NodeInfo currentNodeInfo;

    /**
     * Default {@link Tab} {@link Node} constructor.
     *
     * @param session    tab the node is located on.
     * @param config     quaacr client {@link ContextConfig}.
     * @param consumer   DevTools {@link EventConsumer}.
     * @param query      locator for this note represented as XPath, Selector or basic string.
     * @param index      this node index for {@link Tab} DOM. For instance, if there are two nodes
     *                   with {@link HtmlNode#query}, index can have values {@code 0} or {@code 1}.
     * @param foundTotal total amount of nodes initially found for given query by {@link Locate} job.
     *                   We need this to make sure that total number of nodes found by given query is still
     *                   the same while the current node is being registered.
     *                   See {@link HtmlNode#registerNodeId(int nodeId)}.
     */
    HtmlNode(Session session, ContextConfig config, EventConsumer consumer, String query, int index, int foundTotal) {
        this.session = session;
        this.consumer = consumer;
        this.query = query;
        this.index = index;
        this.foundTotal = foundTotal;
        this.runtimeJS = new RuntimeJS(session.getRuntime());
        this.includeShadowDOM = config.getIncludeShadowDOM();
    }

    /**
     * Calls given JS function on this node. Simply speaking, the node can be referred in JS function as {@code this}.
     *
     * @param jsFunc        JavaScript function to be called on.
     * @param args          JavaScript function arguments. Pass {@code null} if not needed.
     * @param returnByValue whether the result is expected to be a JSON object which should be sent by value.
     * @return function execution positive result or an exception.
     */
    @Override
    public ExecutionResult applyJSFunction(String jsFunc, List<CallArgument> args, boolean returnByValue) {
        String objectId = getObjectId();
        FunctionResult result = runtimeJS.callFunctionOn(
                jsFunc,
                objectId,
                args,
                null,
                returnByValue,
                null, null,
                null, null,
                null);

        if (Objects.nonNull(objectId)) {
            runtimeJS.releaseObject(objectId);
        }

        return new ExecutionResult(result);
    }

    /**
     * Waits until 'aria-expanded' attribute is set to true of false for this node.
     * Client must obey following rules:
     * <li>
     * Node must belong to the type 'aria-expanded' attribute was modified on;
     * </li>
     * <li>
     * 'aria-expanded' attribute must be updated BEFORE calling this method.
     * So, valid execution would be:
     * <pre>{@code
     * BrowserContext cxt = Contexts.local();
     * Tab tab = cxt.defaultTab();
     * tab.open("http://example.com");
     * tab.waitPageLoad();
     * var anchorNode = tab.locate()
     *          .htmlNode("//a[@class='example']")
     *          .click() //here aria gets expanded
     *          .ariaExpanded(true); //now we can verify
     * } </pre>
     * </li>
     * <li>
     * If 'aria-expanded' attribute belongs to INPUT element, for instance, the located node
     * must be also of the input type. Simply speaking, this will work:
     * <p>
     * HTML:
     * <pre> {@code
     *  <html>
     *      <body>
     *          <input aria-expanded='false'>
     *              <span>My Input</span>
     *          </input>
     *      </body>
     *  </html>
     *  } </pre>
     * <p>
     * SRC:
     * <pre>{@code
     *  var node = tab.locate()
     *          .input("//body/input")
     *          .click()
     *          .ariaExpanded(true);
     *  }</pre>
     * <p>
     * This one won't work (for the same HTML):
     * <pre>{@code
     *  var node = tab.locate()
     *          .input("//body/input/span")
     *          .click()
     *          .ariaExpanded(true);
     *  }</pre>
     * </li>
     *
     * @param expanded expected boolean value of 'aria-expanded' attribute.
     */
    public void ariaExpanded(boolean expanded) {
        getFreshNodeId();
        boolean eventFired;
        do {
            try {
                var attrModifiedEvt = consumer.getFiredEvent(
                        attributeModified,
                        attrModifiedRule(ARIA_EXPANDED, Boolean.toString(expanded)));

                int nodeId = (int) attrModifiedEvt.query(NODE_ID_Q);
                eventFired = hasNodeId(nodeId);

            } catch (PollEventException e) {
                throw new ProtocolEventException(String.format("Event failure. " +
                        "[evt=%s]", attributeModified.method()), e);
            }

        } while (!eventFired);
    }

    /**
     * Mouse left button click. No keyboard keys are pressed for this click action.
     */
    public <T extends HtmlNode> T click() {
        return click(null);
    }

    /**
     * Mouse left button click with a {@link Keys} keyboard key pressed down.
     * There are several steps that needs to be passed:
     * <ul>
     *     <li>1) calculate this node's center point (in pixel coordinates);</li>
     *     <li>2) make sure the node we are going to click is exactly this node;</li>
     *     <li>3) click on this node and make sure that click is actually happened.</li>
     * </ul>
     *
     * @param keys keyboard key which must be pressed down during the click.
     * @return this node.
     */
    @SuppressWarnings("unchecked")
    public <T extends HtmlNode> T click(Keys keys) {
        if (session instanceof Tab && ((Tab) session).isInEmulatedMode()) {
            throw new IllegalStateException("Mouse click action is applied in emulated mode. " +
                    "Use HtmlNode#touch() instead or reset emulation via Tab#emulateReset().");
        }

        Supplier<Void> dispatchMouseClick = () -> {
            final byte steps = MOUSE_POINTER_MOVE_COUNT;
            final Input input = session.getInput();
            final Point p = getCenterPoint();
            final int modifier = Objects.nonNull(keys) ? Keyboard.getModifier(keys) : 0;

            for (byte i = 1; i <= steps; i++) {
                input.dispatchMouseEvent(mouseMoved, p.x * (i / steps), p.y * (i / steps), modifier, none, 0, 0, 0);
            }

            input.dispatchMouseEvent(mousePressed, p.x, p.y, modifier, Input.MouseButton.left, 1, 0, 0);
            input.dispatchMouseEvent(mouseReleased, p.x, p.y, modifier, Input.MouseButton.left, 1, 0, 0);

            return null;
        };

        String objectId = verifyNodeForLocation();

        if (sessionStorageAvailable()) {
            dispatchInputActionAndVerify(objectId, InputAction.CLICK, dispatchMouseClick);

        } else {
            logger.warn("Mouse click cannot be verified for queried node. [query={}, url={}]", query, session.getUrl());
            dispatchMouseClick.get();
        }

        return (T) this;
    }

    /**
     * Returns given attribute value or {@code null} if attribute not exist.
     *
     * @param name attribute name.
     * @return attribute value as string.
     */
    public String getAttributeValue(String name) {
        String objectId = getObjectId();
        FunctionResult functionResult = runtimeJS.callFunctionOn(
                "function (attr) {return this.getAttribute(attr);}",
                objectId,
                List.of(CallArgument.of(name)),
                null, null,
                null, null,
                null, null,
                null);

        runtimeJS.checkFunctionResultErrors(functionResult);
        runtimeJS.releaseObject(objectId);

        return (String) functionResult.getResult().getValue();
    }

    /**
     * Returns current node info represented via DOM.Node type.
     *
     * @return this node data.
     * @see Node
     */
    public NodeInfo getNodeInfo() {
        NodeInfo latestNodeInfo;
        try {
            latestNodeInfo = getNodeInfoById(getNodeId());

        } catch (ProtocolResponseErrorException e1) {
            try {
                latestNodeInfo = getNodeInfoById(getFreshNodeId());

            } catch (ProtocolResponseErrorException e2) {
                throw cannotInteractWithNodeEx(e2);
            }
        }

        if (!Objects.equals(currentNodeInfo, latestNodeInfo)) {
            currentNodeInfo = latestNodeInfo;
        }

        return latestNodeInfo;
    }

    /**
     * Returns HTML source of current node.
     *
     * @return this node outer HTML.
     */
    public String getSource() {
        try {
            return session.getDOM().getOuterHTML(getNodeId(), null, null);

        } catch (ProtocolResponseErrorException e1) {
            try {
                return session.getDOM().getOuterHTML(getFreshNodeId(), null, null);

            } catch (ProtocolResponseErrorException e2) {
                throw cannotInteractWithNodeEx(e2);
            }
        }
    }

    /**
     * Mouse pointer mouseover action. Same as method below, but no keyboard keys are involved.
     *
     * @return this node.
     */
    public HtmlNode pointTo() {
        return pointTo(null);
    }

    /**
     * Mouse pointer mouseover action. There are several steps that needs to be passed:
     * <ul>
     *     <li>1) calculate this node's center point (in pixel coordinates);</li>
     *     <li>2) make sure the node we are going to point on is exactly this node;</li>
     *     <li>3) point on this node and make sure that mouseover action is actually happened.</li>
     * </ul>
     *
     * @param keys keyboard keys pressed on mouseover.
     * @return this node.
     */
    public HtmlNode pointTo(Keys keys) {
        if (session instanceof Tab && ((Tab) session).isInEmulatedMode()) {
            throw new IllegalStateException("Mouseover action is applied in emulated mode. " +
                    "Use HtmlNode#touch() instead or reset emulation via Tab#emulateReset().");
        }

        Supplier<Void> dispatchMouseOver = () -> {
            final byte steps = MOUSE_POINTER_MOVE_COUNT;
            final Point p = getCenterPoint();
            final int modifier = Objects.nonNull(keys) ? Keyboard.getModifier(keys) : 0;

            for (byte i = 1; i <= steps; i++) {
                session.getInput().dispatchMouseEvent(mouseMoved, p.x * (i / steps), p.y * (i / steps), modifier, none, 0, 0, 0);
            }

            return null;
        };

        String objectId = verifyNodeForLocation();

        if (sessionStorageAvailable()) {
            dispatchInputActionAndVerify(objectId, InputAction.MOUSEOVER, dispatchMouseOver);

        } else {
            logger.warn("Mouseover cannot be verified for queried node. [query={}, url={}]", query, session.getUrl());
            dispatchMouseOver.get();
        }

        return this;
    }

    /**
     * Node scroll is relative, so client should always have expected scroll position.
     * If it didn't work - sorry, I tried really hard. Please submit an issue and sorry once again.
     *
     * @param deltaY number in pixels to be scrolled down. If number is negative, it scrolls up.
     */
    @Override
    public void scroll(int deltaY) {
        int deltaFin = deltaY + getCenterPoint().y;
        session.getInput().dispatchMouseEvent(mouseWheel, 0, 0, 0, none, 0, 0, deltaFin);
    }

    /**
     * Scrolls the current node into viewport.
     *
     * @param rect rectangle coordinates in pixels. If not presented, the center of current node is used.
     * @return this node.
     */
    public HtmlNode scrollIntoView(DOM.Rect... rect) {
        return scrollIntoView(0L, rect);
    }

    /**
     * Scrolls the current node into viewport.
     *
     * @param timeout timeout after scroll is triggered. Useful when web page is a kinda Y coordinates long.
     * @param rect    rectangle coordinates in pixels. If not presented, the center of current node is used.
     * @return this node.
     */
    public HtmlNode scrollIntoView(long timeout, DOM.Rect... rect) {
        Rect rct = rect.length == 0 ? null : rect[0];
        try {
            session.getDOM().scrollIntoViewIfNeeded(getNodeId(), null, null, rct);

        } catch (ProtocolResponseErrorException e1) {
            try {
                session.getDOM().scrollIntoViewIfNeeded(getFreshNodeId(), null, null, rct);

            } catch (ProtocolResponseErrorException e2) {
                throw cannotInteractWithNodeEx(e2);
            }
        }

        session.waitFor(timeout);
        return this;
    }

    /**
     * Sets new attribute and its value for this node. If attribute already exists, value will ve overwritten.
     *
     * @param name  attribute name.
     * @param value attribute value.
     */
    public void setAttributeValue(String name, String value) {
        try {
            session.getDOM().setAttributeValue(getNodeId(), name, value);

        } catch (ProtocolResponseErrorException e1) {
            try {
                session.getDOM().setAttributeValue(getFreshNodeId(), name, value);

            } catch (ProtocolResponseErrorException e2) {
                throw cannotInteractWithNodeEx(e2);
            }
        }
    }

    /**
     * Emulates a single touchscreen tap action. That's it.
     * <p>
     * NB! {@link Tab#emulate()} must be evoked prior to using this method, otherwise
     * you might get an unexpected behaviour you don't want. So, be reasonable.
     *
     * @return this node.
     */
    public <T extends HtmlNode> T touch() {
        return touch(null);
    }

    /**
     * Emulates a touchscreen tap action with a {@link Keys} virtual keyboard key pressed down.
     * There are several steps that needs to be passed:
     * <ul>
     *     <li>1) calculate this node's center point (in pixel coordinates);</li>
     *     <li>2) make sure the node we are going to tap is exactly this node;</li>
     *     <li>3) tap on this node and make sure that the tap is actually happened.</li>
     * </ul>
     * <p>
     * NB! {@link Tab#emulate()} must be evoked prior to using this method, otherwise
     * you might get an unexpected behaviour you don't want. So, be reasonable.
     *
     * @param keys virtual keyboard key which must be pressed down during the tap.
     * @return this node.
     */
    @SuppressWarnings("unchecked")
    public <T extends HtmlNode> T touch(Keys keys) {
        Supplier<Void> dispatchTouch = () -> {
            final Input input = session.getInput();
            final Point p = getCenterPoint();
            final int modifier = Objects.nonNull(keys) ? Keyboard.getModifier(keys) : 0;
            input.dispatchTouchEvent(touchStart, List.of(Input.TouchPoint.of(p.x, p.y)), modifier, null);
            input.dispatchTouchEvent(touchEnd, List.of(), modifier, null);
            return null;
        };

        String objectId = verifyNodeForLocation();

        if (sessionStorageAvailable()) {
            dispatchInputActionAndVerify(objectId, InputAction.TOUCHSTART, dispatchTouch);

        } else {
            logger.warn("Touch tap cannot be verified for queried node. [query={}, url={}]", query, session.getUrl());
            dispatchTouch.get();
        }

        return (T) this;
    }

    /* Returns generic NodeLocationException if a node cannot be interacted. */
    NodeLocationException cannotInteractWithNodeEx(Throwable e) {
        return new NodeLocationException("Node interaction error. " +
                "[reason=NOT_INTERACTED," +
                " url=" + session.getUrl() + "," +
                " query=" + query + "," +
                " queriedNode=" + getCurrentNodeInfo() + "]", e);
    }

    /* Calculates center coordinates in pixels for this element. */
    Point getCenterPoint() {
        //Scroll to center:
        scrollIntoView();
        // Calc center:
        BoxModel boxModel;
        try {
            boxModel = session.getDOM().getBoxModel(getNodeId(), null, null);

        } catch (ProtocolResponseErrorException e1) {
            try {
                boxModel = session.getDOM().getBoxModel(getFreshNodeId(), null, null);

            } catch (ProtocolResponseErrorException e2) {
                throw cannotInteractWithNodeEx(e2);
            }
        }

        List<Integer> margin = boxModel.getMargin();

        int[][] points = new int[4][2];
        points[0] = new int[]{margin.get(0), margin.get(1)};
        points[1] = new int[]{margin.get(2), margin.get(3)};
        points[2] = new int[]{margin.get(4), margin.get(5)};
        points[3] = new int[]{margin.get(6), margin.get(7)};

        LayoutMetrics layoutMetrics = session.getPage().getLayoutMetrics();
        LayoutViewport layoutViewport = layoutMetrics.getLayoutViewport();
        int clientWidth = layoutViewport.getClientWidth();
        int clientHeight = layoutViewport.getClientHeight();

        for (int i = 0; i < points.length; i++) {
            points[i] = new int[]{
                    Math.min(Math.max(points[i][0], 0), clientWidth),
                    Math.min(Math.max(points[i][1], 0), clientHeight)
            };
        }

        int area = 0;
        for (int j = 0; j < points.length; ++j) {
            int[] point1 = points[j];
            int[] point2 = points[(j + 1) % points.length];
            area += (point1[0] * point2[1] - point2[0] * point1[1]) / 2;
        }

        area = Math.abs(area);

        if (area <= 1) {
            throw new NodeLocationException(String.format("Node not visible. Could be hidden or out of view port. " +
                    "[query=%s, url=%s, nodeInfo=%s]", query, session.getUrl(), getCurrentNodeInfo()));
        }

        int xc = 0;
        int yc = 0;
        for (int[] point : points) {
            xc += point[0];
            yc += point[1];
        }

        return Point.of(xc / 4, yc / 4);
    }

    /* Returns the latest nodeId for the current Node. */
    Integer getNodeId() {
        return nodeIdSet.isEmpty() ? getFreshNodeId() : nodeIdSet.last();
    }

    /* Verifies the node still belongs to its DOM and returns the latest nodeId on success. */
    Integer getFreshNodeId() {
        DOM dom = session.getDOM();
        PerformSearch search = dom.performSearch(query, includeShadowDOM);

        List<Integer> nodeIds;
        int resultCount;
        nodeIds = ((resultCount = search.getResultCount()) > 0)
                ? dom.getSearchResults(search.getSearchId(), 0, resultCount)
                : null;

        if (Objects.isNull(nodeIds)) {
            throw new NodeLocationException("Node location error. " +
                    "[reason=DETACHED," +
                    " url=" + session.getUrl() + "," +
                    " query=" + query + "," +
                    " queriedNode=" + getCurrentNodeInfo() + "]");
        }

        if (nodeIds.size() != foundTotal) {
            throw new NodeLocationException("Node location error. " +
                    "[reason=AMOUNT_CHANGED," +
                    " url=" + session.getUrl() + "," +
                    " query=" + query + "," +
                    " queriedNode=" + getCurrentNodeInfo() + "," +
                    " nodesFoundInitially=" + foundTotal + "," +
                    " nodesFoundOnUpdate=" + nodeIds.size() + "]");
        }

        int nodeId = nodeIds.get(index);

        //If nodeId is 0, it means that the node location action was executed
        // while DOM was in a kind of half-baked state (from DevTools perspective).
        // So, as we clear tab's events state (see org.punch.context.entities.Tab)
        // we also need to make sure that any events related to 'before reload' state
        // (which still could present even after page reload, as there is a chance, that
        // old events could arrive after a tab reloading from any action which happened
        //right before reload) won't be associated with this node after tab is reloaded.
        // That's why we need to clear all previous node Ids:
        if (nodeId == 0) {
            //If the node is a part of a list, quaacr throws exception, cause there is
            // no guarantee that tne node's index would be the same after fresh search:
            if (foundTotal > 1) {
                throw new NodeLocationException("Node location error." +
                        "[reason=NOT_FULLY_CONSTRUCTED_DOM," +
                        " url=" + session.getUrl() + "," +
                        " query=" + query + "," +
                        " queriedNode=" + getCurrentNodeInfo() + "," +
                        " failedNodeIndex=" + index + "," +
                        " queriedNodesTotal=" + nodeIds.size() + "," +
                        " description=\"Node was located for a half-constructed DOM." +
                        " As this node is a part of the nodes list, quaacr cannot" +
                        " guarantee, that exactly the same node will be found after" +
                        " a fresh search.\"");
            }

            logger.info("DOM was reset, updating... [query={}]", query);

            nodeIdSet.clear();
            dom.getDocument(null, includeShadowDOM);
            nodeId = getFreshNodeId();
        }

        registerNodeId(nodeId);

        return nodeId;
    }

    /* Returns Runtime objectId for this node. */
    String getObjectId() {
        return resolveNode().getObjectId();
    }

    /* Checks if given nodeId belongs to this node. */
    boolean hasNodeId(int nodeId) {
        return nodeIdSet.contains(nodeId);
    }

    /* Sets new nodeId for this node after DOM is updated for current tab. */
    void registerNodeId(int nodeId) {
        if (nodeId == 0) {
            //Here we just need to re-register nodeId:
            getFreshNodeId();

        } else {
            boolean isAdded = nodeIdSet.add(nodeId);

            if (isAdded) {
                try {
                    currentNodeInfo = getNodeInfoById(nodeId);

                } catch (ProtocolResponseErrorException e) {
                    logger.warn("Node not exist anymore - let's search one more time. [query={}", query);
                    //remove nodeId and re-register:
                    nodeIdSet.remove(nodeId);
                    getFreshNodeId();
                }
            }
        }
    }

    /**
     * This method assigns JS function to a node which is about to be clicked or tapped. If successful, the function
     * puts key-value to session storage, method checks that session storage actually has the stored key-value pair,
     * removes it from session storage and finishes normally. If session storage doesn't have given pair for the
     * existing origin, it means, that input action (mouse or tap) didn't happen, and the exception will be thrown.
     * If on session storage content request the {@link ProtocolResponseErrorException} saying that nothing is found
     * for given security origin, quaacr considers that input action was actually applied to a node, as the resource
     * was updated, which in turn forced the origin to be updated.
     *
     * @param objectId            the node runtime objectId.
     * @param inputAction         click, mouseover or touch (tap).
     * @param dispatchInputAction method that executes input action (mouse or tap) via DevTools protocol.
     * @throws NodeLocationException if mouse pointer action fails.
     */
    private void dispatchInputActionAndVerify(String objectId,
                                              InputAction inputAction,
                                              Supplier<Void> dispatchInputAction) {

        //Enable DOM Storage to get registered events from storage
        DOMStorage domStorage = session.getDOMStorage();

        //Prepares JS script data for the execution in runtime. It actually sets JS event listener
        // for a given mouse action (click or mouseover). On success it will be saved to browser's
        // session storage as a key-value pair with key equals 'Runtime.ObjectId value + Node#nodeId value'
        // and value equals to mouseAction:
        String storageKey = "{\"nodeId\":" + nodeIdSet.last() + "," + "\"query\":\"" + query + "\"}";
        List<CallArgument> funcArgs = List.of(CallArgument.of(storageKey), CallArgument.of(inputAction.value));
        FunctionResult functionResult = runtimeJS.callFunctionOn(
                setSessionStorageItemOnMousePointerAction(),
                objectId,
                funcArgs,
                null, null,
                null, null,
                null, null,
                null);

        runtimeJS.checkFunctionResultErrors(functionResult);
        runtimeJS.releaseObject(objectId);

        //Security origin value needs to be retrieved before mouse action:
        String securityOrigin = session.getPage().getFrameTree().getFrame().getSecurityOrigin();

        //Execute mouse action via DevTools protocol:
        dispatchInputAction.get();

        //Once input event is dispatched, we retrieve all the session storage items for given Security Origin
        // to check whether event actually is fired. If nothing was saved to session storage then we know that
        // input event didn't actually happen:
        final String ORIGIN_FRAME_NOT_FOUND_MSG = "Frame not found for the given security origin";
        final DOMStorage.StorageId storageId = DOMStorage.StorageId.of(securityOrigin, false);
        try {
            List<List<String>> domStorageItems = domStorage.getDOMStorageItems(storageId);
            List<String> domStorageItem = domStorageItems.stream()
                    .filter(itm -> itm.get(0).equals(storageKey) && itm.get(1).equals(inputAction.value))
                    .findFirst().orElse(null);

            if (Objects.isNull(domStorageItem)) {
                throw new NodeLocationException("Node interaction error. " +
                        "[reason=INPUT_ACTION_NOT_APPLIED," +
                        " action=" + inputAction.value + "," +
                        " url=" + session.getUrl() + "," +
                        " query=" + query + "," +
                        " queriedNode=" + getCurrentNodeInfo() + "]");
            }

        } catch (ProtocolResponseErrorException e) {
            //If storage items cannot be fetched cause the given origin not exist anymore,
            // quaacr treats this case as the click/touch event was dispatched, printing
            // the debug message below for the sake of awareness:
            if (e.getMessage().contains(ORIGIN_FRAME_NOT_FOUND_MSG)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Cannot get storage items: frame not exist anymore. " +
                            "[origin={}, key={}]", securityOrigin, storageKey, e);
                }

            } else throw e;
        }

        try {
            //Remove saved key-value pair from session storage and make sure it was deleted:
            domStorage.removeDOMStorageItem(storageId, storageKey);
            consumer.waitFiredEvent(domStorageItemRemoved, j -> j.query(KEY_Q).equals(storageKey));

        } catch (ProtocolResponseErrorException e) {
            //If storage item cannot be removed cause the given origin not exist anymore,
            // quaacr just treats this case as the item has already been removed, printing
            // the debug message below for the sake of awareness:
            if (e.getMessage().contains(ORIGIN_FRAME_NOT_FOUND_MSG)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Cannot remove storage item: frame not exist anymore. " +
                            "[origin={}, key={}]", securityOrigin, storageKey, e);
                }

            } else throw e;

        } catch (PollEventException e) {
            throw new ProtocolEventException("DOM Storage item cannot be removed or not exist. " +
                    "[storageId=" + storageId + "," +
                    " key=" + storageKey + "," +
                    " url=" + session.getUrl() + "]", e);
        }

        domStorage.disable();
    }

    /**
     * Locates a center point of the queried node and gathers the node's data by that coordinates. Ideally,
     * target node equals to this {@link HtmlNode} node, but it might not be the case. For instance, we use
     * XPath '//button' expression to locate required button, but in HTML document this button tag looks like
     * {@code <button><span>click me</span></button>}.
     * So as a result, the center of the {@code <button>} node returns {@code <span>} node, and the mouse click
     * or mouseover will be applied to {@code <span>} node.
     * <p>
     * Why do we need this information at all? Well, just to make sure that the node we are about to click
     * is actually belongs to the searchable node. For instance, check {@link #getLocatedObjectIdForChild} method.
     *
     * @return node's objectId, nodeId and node description.
     */
    private NodeObject elementFromPointNode() {
        Point p = getCenterPoint();

        FunctionResult evaluateResult = runtimeJS.evaluate(
                String.format("document.elementFromPoint(%d, %d)", p.x, p.y),
                "console",
                null, null,
                null, null,
                true, null,
                null);

        runtimeJS.checkFunctionResultErrors(evaluateResult);
        RemoteObject remoteObject = evaluateResult.getResult();
        String objectId = remoteObject.getObjectId();
        int nodeId = session.getDOM().requestNode(objectId);

        return NodeObject.of(objectId, nodeId, remoteObject.getDescription());
    }

    /* Returns all unique child local names for given parent node. */
    private Set<String> getChildLocalNames(DOM.Node parent, Set<String> localNames) {
        if (parent.getChildNodeCount() == 0) {
            return Objects.isNull(localNames) ? Collections.emptySet() : localNames;
        }

        localNames = Objects.isNull(localNames) ? new HashSet<>() : localNames;

        for (DOM.Node child : parent.getChildren()) {
            if (child.getNodeType() != TEXT_NODE) {
                String localName = child.getLocalName();

                if (!localName.isEmpty()) {
                    localNames.add(child.getLocalName());
                }

                getChildLocalNames(child, localNames);
            }
        }

        return localNames;
    }

    /* Returns a list of IDs of all child nodes for given parent node id (if any). */
    private List<Integer> getChildNodeIds(int nodeId) {
        DOM dom = session.getDOM();
        var node = dom.describeNode(nodeId, null, null, -1, includeShadowDOM);

        //Collect all the child nodes' local names (except text);
        Set<String> localNames = getChildLocalNames(node, null);

        //Find all child node IDs for this node:
        List<Integer> childNodeIds = new ArrayList<>();
        localNames.forEach(n -> {
            var ids = dom.querySelectorAll(nodeId, "* " + n);
            childNodeIds.addAll(ids);
        });

        return childNodeIds;
    }

    /* Returns the latest NodeInfo instance or NotDefinedNodeInfo on the node's search failure. */
    private NodeInfo getCurrentNodeInfo() {
        return Objects.isNull(currentNodeInfo) ? NOT_DEFINED_NODE_INFO : currentNodeInfo;
    }

    /* Returns IFrame nodeId for given IFrame remote (runtime) objectId. */
    private int getIFrameDocumentNodeId(String iFrameNodeObjectId) {
        //Call iFrame document:
        FunctionResult contentWindowResult = runtimeJS.callFunctionOn(
                "function(){return this.contentWindow.document}",
                iFrameNodeObjectId,
                null, null,
                null, null,
                null, null,
                null, null);

        runtimeJS.checkFunctionResultErrors(contentWindowResult);
        String iframeDocObjectId = contentWindowResult.getResult().getObjectId();
        int iframeDocNodeId = session.getDOM().requestNode(iframeDocObjectId);
        runtimeJS.releaseObject(iframeDocObjectId);

        return iframeDocNodeId;
    }

    /**
     * Returns objectId of a child node for this {@link HtmlNode} the {@link InputAction} is going to be applied on.
     * Node's interaction point is defined by mouse pointer coordinates.
     * <p>
     * This method checks (at least this is the goal) all the cases, when the interacted node is a child node of this
     * {@link HtmlNode}. If this is true, then the objectId of the child node is returned, otherwise an exception will
     * be thrown.
     *
     * @param locatedNode objectId, nodeId and description which intercepts with the mouse pointer coordinates.
     * @return child node objectId.
     */
    private String getLocatedObjectIdForChild(NodeObject locatedNode) {
        int currentNodeId = getNodeId();
        List<Integer> currentNodeChildrenNodeIds;
        try {
            currentNodeChildrenNodeIds = getChildNodeIds(currentNodeId);

            if (currentNodeChildrenNodeIds.contains(locatedNode.nodeId)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Clickable node is a child element of queried node. [query={}]", query);
                }

                return locatedNode.objectId;
            }

        } catch (ProtocolResponseErrorException e) {
            throw cannotInteractWithNodeEx(e);
        }

        //If located node is not a child of the current node, then we have options:
        //
        // 1) located child node is a pseudo element. Here we need to get all the node's pseudo elements
        //    and check whether the located node belongs to this list via backendNodeId comparison;
        //
        // 2) located node is some random node with higher nodeId (could happen during a scrolling).
        //    In this case we try to re-locate the node 16 times and throw an exception on failure:

        //1. Pseudo element:
        NodeInfo locatedNodeInfo;
        boolean locatedNodeUpdated = false;
        try {
            locatedNodeInfo = getNodeInfoById(locatedNode.nodeId);

        } catch (ProtocolResponseErrorException e1) {
            //If node doesn't exist anymore, then try to determine the node
            // the mouse pointer action will be applied to once again:
            locatedNode = elementFromPointNode();
            locatedNodeUpdated = true;

            try {
                locatedNodeInfo = getNodeInfoById(locatedNode.nodeId);

            } catch (ProtocolResponseErrorException e2) {
                throw cannotInteractWithNodeEx(e2);
            }
        }

        if (locatedNodeInfo.isPseudoType()) {
            DOM.Node currentNode;
            try {
                currentNode = session.getDOM().describeNode(currentNodeId, null, null, -1, includeShadowDOM);

            } catch (ProtocolResponseErrorException e) {
                throw cannotInteractWithNodeEx(e);
            }

            List<DOM.Node> currentNodePseudoElements = getNodePseudoElements(currentNode, null);
            List<Integer> pseudoElementsBackendNodeIds = currentNodePseudoElements.stream()
                    .map(DOM.Node::getBackendNodeId)
                    .collect(Collectors.toList());

            if (pseudoElementsBackendNodeIds.contains(locatedNodeInfo.getBackendNodeId())) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Clickable node is a child pseudo element of queried node. " +
                            "[clickableNode={}]", locatedNodeInfo);
                }

                //Re-locate clickable node once again and return its objectId:
                return elementFromPointNode().objectId;

            } else throw nodeLocationExceptionDetails("pseudo.child", locatedNode.nodeId);
        }

        //2. Some random node with higher nodeId:
        byte adjustCoordinatesCountdown = ADJUST_COORDINATES_COUNT;

        do {
            if (adjustCoordinatesCountdown == 0) {
                throw nodeLocationExceptionDetails("child", locatedNode.nodeId);
            }

            if (!locatedNodeUpdated) {
                //Just to save some time on loop's first run
                locatedNode = elementFromPointNode();
            }

            if (nodeIdSet.contains(locatedNode.nodeId)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Clickable node is a self element of queried node. [query={}]", query);
                }

                return locatedNode.objectId;
            }

            if (currentNodeChildrenNodeIds.contains(locatedNode.nodeId)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Clickable node is a child element of queried node. [query={}]", query);
                }

                return locatedNode.objectId;
            }

            locatedNodeUpdated = false;
            adjustCoordinatesCountdown--;

        } while (true);
    }

    /**
     * Returns objectId of an IFrame node for this {@link HtmlNode} the {@link InputAction} is going to be applied on.
     * Node's interaction point is defined by mouse pointer coordinates.
     * <p>
     * This method checks (at least this is the goal) all the cases, when the interacted node is an IFrame node of this
     * {@link HtmlNode}. If this is true, then the objectId of the IFrame node is returned, otherwise an exception will
     * be thrown.
     *
     * @param locatedNode objectId, nodeId and description which intercepts with the mouse pointer coordinates.
     * @return IFrame node objectId.
     */
    private String getLocatedObjectIdForIFrame(NodeObject locatedNode) {
        int currentNodeId = getNodeId();
        String locatedNodeType = "iframe";

        //1. IFrame is a child of the current node:
        if (currentNodeId < locatedNode.nodeId) {
            //First check that IFrame belongs to current node:
            List<Integer> currentNodeChildren;
            try {
                currentNodeChildren = getChildNodeIds(currentNodeId);

            } catch (ProtocolResponseErrorException e) {
                throw cannotInteractWithNodeEx(e);
            }

            //If true, locate node to be clicked within IFrame:
            if (currentNodeChildren.contains(locatedNode.nodeId)) {
                int iframeDocNodeId = getIFrameDocumentNodeId(locatedNode.objectId);
                //Get nodeId of element to be clicked within IFramme:
                DOM dom = session.getDOM();
                Point p = getCenterPoint();
                NodeForLocation iframeClickableNode = dom.getNodeForLocation(p.x, p.y, includeShadowDOM, false);
                int iframeClickableNodeId = iframeClickableNode.getNodeId();

                try {
                    //Locate all the IFrame children nodeIds and if clickable nodeId belongs
                    // to these children, return its objectId:
                    List<Integer> iframeChildrenNodeIds = getChildNodeIds(iframeDocNodeId);

                    if (iframeChildrenNodeIds.contains(iframeClickableNodeId)) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("Clickable node is a child iframe element of queried node. [query={}]", query);
                        }

                        return dom.resolveNode(iframeClickableNodeId, null, null, null).getObjectId();
                    }
                } catch (ProtocolResponseErrorException e) {
                    throw cannotInteractWithNodeEx(e);
                }
            }

            locatedNodeType += ".child";
        }

        //2. IFrame is a ancestor of the current node:
        if (currentNodeId > locatedNode.nodeId) {
            //Check that current node is child of IFrame and if true -
            // re-locate clickable node and return its objectId:
            int iframeDocNodeId = getIFrameDocumentNodeId(locatedNode.objectId);
            List<Integer> iframeChildrenNodeIds = getChildNodeIds(iframeDocNodeId);

            if (iframeChildrenNodeIds.contains(currentNodeId)) {
                DOM dom = session.getDOM();
                Point p = getCenterPoint();
                NodeForLocation iframeClickableNode = dom.getNodeForLocation(p.x, p.y, includeShadowDOM, false);
                int iframeClickableNodeId = iframeClickableNode.getNodeId();

                if (iframeClickableNodeId >= currentNodeId) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Clickable node is a parent iframe element of queried node. [query={}]", query);
                    }

                    return dom.resolveNode(iframeClickableNodeId, null, null, null).getObjectId();
                }
            }

            locatedNodeType += ".parent";
        }

        throw nodeLocationExceptionDetails(locatedNodeType, locatedNode.nodeId);
    }

    /**
     * Returns objectId of a parent node for this {@link HtmlNode} the {@link InputAction} is going to be applied on.
     * Node's interaction point is defined by mouse pointer coordinates.
     * <p>
     * This method checks (at least this is the goal) all the cases, when the interacted node is a parent node of this
     * {@link HtmlNode}. If this is true, then the objectId of the parent node is returned, otherwise an exception will
     * be thrown.
     *
     * @param locatedNode objectId, nodeId and description which intercepts with mouse the pointer coordinates.
     * @return parent node objectId.
     */
    private String getLocatedObjectIdForParent(NodeObject locatedNode) {
        List<Integer> locatedNodeChildrenNodeIds;
        try {
            locatedNodeChildrenNodeIds = getChildNodeIds(locatedNode.nodeId);

        } catch (ProtocolResponseErrorException e) {
            throw cannotInteractWithNodeEx(e);
        }

        int currentNodeId = getNodeId();

        //Check that this node belongs to located parent one. If it's not true - throw exception, otherwise -
        // there are options:
        //
        // 1) located node is a parent cause mouse pointer coordinates are not accurate (due to scroll, for instance).
        //    In this case we need to re-locate this node until the coordinates are coordinates of this node;
        //
        // 2) located node is a parent, cause this node has 'pointer-events:none' style property, which means
        //    it ignores all mouse pointer actions. In this case we need to find the nearest ancestor of the node
        //    which can be clicked (does not have 'pointer-events:none' property).

        if (!locatedNodeChildrenNodeIds.contains(currentNodeId)) {
            throw nodeLocationExceptionDetails("parent", locatedNode.nodeId);
        }

        CSS.CSSComputedStyleProperty pointerEventsNoneProp = getPointerEventsNoneProperty(currentNodeId);

        //1. Handle regular parent node:
        if (Objects.isNull(pointerEventsNoneProp)) {
            //16 coordinates adjustment attempts will be executed before an exception will be thrown:
            byte adjustCoordinatesCountdown = ADJUST_COORDINATES_COUNT;

            do {
                if (adjustCoordinatesCountdown == 0) {
                    throw nodeLocationExceptionDetails("parent", locatedNode.nodeId);
                }

                //Re-locate previously located node to adjust
                // pointer coordinates to point to this node:
                locatedNode = elementFromPointNode();

                if (nodeIdSet.contains(locatedNode.nodeId)) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Clickable node is a self element of queried node.. [query={}]", query);
                    }

                    return locatedNode.objectId;
                }

                adjustCoordinatesCountdown--;

            } while (true);
        }

        // 2. handle pointer-events:none case:
        do {
            DOM dom = session.getDOM();
            String currentObjectId;
            try {
                currentObjectId = dom.resolveNode(currentNodeId, null, null, null).getObjectId();

            } catch (ProtocolResponseErrorException e) {
                throw cannotInteractWithNodeEx(e);
            }

            FunctionResult parentNodeResult = runtimeJS.callFunctionOn(
                    "function(){return this.parentNode;}",
                    currentObjectId,
                    null, null,
                    null, null,
                    null, null,
                    null, null);

            runtimeJS.checkFunctionResultErrors(parentNodeResult);

            String parentObjectId = parentNodeResult.getResult().getObjectId();
            currentNodeId = dom.requestNode(parentObjectId); //Assign parent node id as current node.
            pointerEventsNoneProp = getPointerEventsNoneProperty(currentNodeId);

            if (Objects.isNull(pointerEventsNoneProp)) {
                locatedNode = elementFromPointNode();

                if (locatedNode.nodeId == currentNodeId) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Clickable node is a parent element of queried node. [query={}]", query);
                    }

                    return locatedNode.objectId;
                }
            }

        } while (true);
    }

    /**
     * Returns DOM node data as {@link NodeInfo} object for given nodeId.
     *
     * @param nodeId nodeId.
     * @return wrapped {@link DOM.Node} object.
     * @throws ProtocolResponseErrorException for not existing nodeId.
     * @see <a href='https://chromedevtools.github.io/devtools-protocol/tot/DOM/#type-Node'>DevTools DOM.Node</a>
     */
    private NodeInfo getNodeInfoById(int nodeId) {
        DOM.Node node = session.getDOM().describeNode(nodeId, null, null, -1, includeShadowDOM);
        return new NodeInfo(node);
    }

    /* Returns a list of pseudo elements for given root node. */
    private List<DOM.Node> getNodePseudoElements(DOM.Node root, List<DOM.Node> pseudoElements) {
        if (Objects.isNull(root.getPseudoElements()) || root.getPseudoElements().isEmpty()) {
            return pseudoElements;
        }

        pseudoElements = Objects.isNull(pseudoElements) ? new ArrayList<>() : pseudoElements;

        for (DOM.Node pseudo : root.getPseudoElements()) {
            pseudoElements.add(pseudo);

            getNodePseudoElements(pseudo, pseudoElements);
        }

        return pseudoElements;
    }

    /* Returns CSS property which ignores mouse pointer events. */
    private CSS.CSSComputedStyleProperty getPointerEventsNoneProperty(int nodeId) {
        List<CSS.CSSComputedStyleProperty> props = session.getCSS().getComputedStyleForNode(nodeId);
        return props.stream()
                .filter(p -> p.getName().equals("pointer-events") && p.getValue().equals("none"))
                .findFirst()
                .orElse(null);
    }

    /* Returns NodeLocationException with details if a node belonging to given coordinates is not expected one. */
    private NodeLocationException nodeLocationExceptionDetails(String relative, int locatedNodeId) {
        NodeInfo locatedNodeInfo;
        try {
            locatedNodeInfo = getNodeInfoById(locatedNodeId);

        } catch (Exception e1) {
            return new NodeLocationException("Node location error. " +
                    "[reason=DETACHED," +
                    " url=" + session.getUrl() +
                    " query=" + query + "," +
                    " locatedAs=" + relative + " element of queried node," +
                    " queriedNode=" + getCurrentNodeInfo() + "," +
                    " interactedNode=NOT_FOUND]");
        }

        return new NodeLocationException("Node interaction error. " +
                "[reason=NOT_INTERACTED," +
                " url=" + session.getUrl() +
                " query=" + query + "," +
                " locatedAs=" + relative + " element of queried node," +
                " queriedNode=" + getCurrentNodeInfo() + "," +
                " interactedNode=" + locatedNodeInfo + "]");
    }

    /* Returns Remote Object instance for this node. */
    private Runtime.RemoteObject resolveNode() {
        try {
            return session.getDOM().resolveNode(getNodeId(), null, null, null);

        } catch (ProtocolResponseErrorException e1) {
            try {
                return session.getDOM().resolveNode(getFreshNodeId(), null, null, null);

            } catch (ProtocolResponseErrorException e2) {
                throw cannotInteractWithNodeEx(e2);
            }
        }
    }

    /* Checks if session storage is available for the current session this node belongs to. */
    private boolean sessionStorageAvailable() {
        final String IS_STORAGE_AVAIL_FUNC =
                "(function() {" +
                        "    var quaacr = 'quaacr';" +
                        "    try {" +
                        "        sessionStorage.setItem(quaacr, quaacr);" +
                        "        sessionStorage.removeItem(quaacr);" +
                        "        return true;" +
                        "    } catch(e) {" +
                        "        return false;" +
                        "    }" +
                        "})()";

        FunctionResult evaluateResult = runtimeJS.evaluate(
                IS_STORAGE_AVAIL_FUNC,
                "console",
                null, null,
                null, null,
                null, null,
                null);

        runtimeJS.checkFunctionResultErrors(evaluateResult);
        RemoteObject remoteObject = evaluateResult.getResult();
        String objectId = remoteObject.getObjectId();

        if (Objects.nonNull(objectId)) {
            runtimeJS.releaseObject(remoteObject.getObjectId());
        }

        return (boolean) remoteObject.getValue();
    }

    /* Returns objectId of the node which is located by the mouse pointer coordinates. */
    private String verifyNodeForLocation() {
        NodeObject locatedNode = elementFromPointNode();

        //If this is the same node, return located objectId:
        if (nodeIdSet.contains(locatedNode.nodeId)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Clickable node is a self element of queried node. [query={}]", query);
            }

            return locatedNode.objectId;
        }

        // If located node is an IFrame:
        if (locatedNode.description.contains("iframe")) {
            return getLocatedObjectIdForIFrame(locatedNode);
        }

        //If located node is a child of the current node:
        if (locatedNode.nodeId > nodeIdSet.last()) {
            //First check whether located node equals to the
            // current node. It can be true for form/input cases:
            NodeInfo locatedNodeInfo;
            try {
                locatedNodeInfo = getNodeInfoById(locatedNode.nodeId);

            } catch (ProtocolResponseErrorException e1) {
                locatedNode = elementFromPointNode();
                try {
                    locatedNodeInfo = getNodeInfoById(locatedNode.nodeId);

                } catch (ProtocolResponseErrorException e2) {
                    throw cannotInteractWithNodeEx(e2);
                }
            }

            if (locatedNodeInfo.equals(getNodeInfo())) {
                return locatedNode.objectId;
            }

            //If located not differs from the current node
            // then go to the step below:
            return getLocatedObjectIdForChild(locatedNode);
        }

        //If located node is a parent of the current node:
        if (locatedNode.nodeId < nodeIdSet.last()) {
            return getLocatedObjectIdForParent(locatedNode);
        }

        throw nodeLocationExceptionDetails("self", locatedNode.nodeId);
    }

    /* JavaScript function that sets mouse pointer action data to session storage. */
    private static String setSessionStorageItemOnMousePointerAction() {
        return "function(...args) {" +
                "    function mousePointerHandler() {" +
                "        sessionStorage.setItem(args[0], args[1]);" +
                "    }" +
                "    this.addEventListener(args[1], mousePointerHandler);" +
                "}";
    }

    /* Inner classes */

    private static class NodeObject {

        private final String objectId;
        private final int nodeId;
        private final String description;

        private NodeObject(String objectId, int nodeId, String description) {
            this.objectId = objectId;
            this.nodeId = nodeId;
            this.description = description;
        }

        private static NodeObject of(String objectId, int nodeId, String description) {
            return new NodeObject(objectId, nodeId, description);
        }
    }

    private static class NotFoundNodeInfo extends NodeInfo {

        public NotFoundNodeInfo() {
            super(null);
        }

        @Override
        public String toString() {
            return "NOT_FOUND";
        }
    }

    private static class Point {

        private final int x;
        private final int y;

        private Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        private static Point of(int x, int y) {
            return new Point(x, y);
        }

        @Override
        public String toString() {
            return "(" + x + ", " + y + ")";
        }
    }

    private enum InputAction {
        CLICK("click"), MOUSEOVER("mouseover"), TOUCHSTART("touchstart");

        private final String value;

        InputAction(String value) {
            this.value = value;
        }
    }
}
