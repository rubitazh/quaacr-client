package org.punch.context.entities;

import org.punch.context.interfaces.Node;
import org.punch.devtools.protocol.dom.DOM;

import java.util.List;

import static org.punch.devtools.protocol.runtime.Runtime.*;

public class ShadowRootNode implements Node {

    private final DOM.Node node;
    private final DOM dom;

    ShadowRootNode(DOM.Node node, DOM dom) {
        this.node = node;
        this.dom = dom;
    }

    /**
     * Currently not implemented.
     */
    @Override
    public ExecutionResult applyJSFunction(String jsFunc, List<CallArgument> args, boolean returnByValue) {
        throw new NullPointerException("Not implemented.");
    }

    @Override
    public NodeInfo getNodeInfo() {
        return new NodeInfo(node);
    }

    @Override
    public String getSource() {
        return dom.getOuterHTML(node.getNodeId(), null, null);
    }
}
