/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.punch.devtools.protocol.runtime.Runtime;
import org.punch.devtools.protocol.runtime.ExceptionDetails;

/**
 * Represents JavaScript function of code execution result. Result returns to a client as a {@link JSObject} or as a
 * {@link JSException} instances. If JavaScript executes normally, then no {@link JSException} will be returned.
 */
public class ExecutionResult {

    private final JSObject jsObject;
    private final JSException jsException;

    ExecutionResult(Runtime.FunctionResult functionResult) {
        this.jsObject = functionResult.getResult() != null
                ? new JSObject(functionResult.getResult())
                : null;

        this.jsException = functionResult.getExceptionDetails() != null
                ? new JSException(functionResult.getExceptionDetails())
                : null;
    }

    /* Returns JavaScript object representation for an executed JavaScript code. */
    public JSObject getJSObject() {
        return jsObject;
    }

    /* Returns JavaScript error representation for an executed JavaScript code or null, if error wasn't thrown. */
    public JSException getJSException() {
        return jsException;
    }

    /**
     * Represents JavaScript object wrapper as a result of an executed JavaScript code.
     *
     * @see <a href="https://chromedevtools.github.io/devtools-protocol/tot/Runtime/#type-RemoteObject">JSObject</a>
     */
    public static class JSObject {

        private String type;
        private String subtype;
        private String className;
        private String description;
        private Object value;
        private String unserializableValue;

        private JSObject(Runtime.RemoteObject remoteObject) {
            this.type = remoteObject.getType();
            this.subtype = remoteObject.getSubtype();
            this.className = remoteObject.getClassName();
            this.description = remoteObject.getDescription();
            this.value = remoteObject.getValue();
            this.unserializableValue = remoteObject.getUnserializableValue();
        }

        public String getType() {
            return type;
        }

        public String getSubtype() {
            return subtype;
        }

        public String getClassName() {
            return className;
        }

        public String getDescription() {
            return description;
        }

        public Object getValue() {
            return value;
        }

        public String getUnserializableValue() {
            return unserializableValue;
        }
    }

    /**
     * Represents JavaScript error wrapper as a result of an executed JavaScript code.
     *
     * @see <a href="https://chromedevtools.github.io/devtools-protocol/tot/Runtime/#type-ExceptionDetails">JSException</a>
     */
    public static class JSException {

        private String text;
        private int lineNumber;
        private int columnNumber;
        private String url;
        private JSObject exception;

        private JSException(ExceptionDetails exceptionDetails) {
            this.text = exceptionDetails.getText();
            this.lineNumber = exceptionDetails.getLineNumber();
            this.columnNumber = exceptionDetails.getColumnNumber();
            this.url = exceptionDetails.getUrl();
            this.exception = new JSObject(exceptionDetails.getException());
        }

        public String getText() {
            return text;
        }

        public int getLineNumber() {
            return lineNumber;
        }

        public int getColumnNumber() {
            return columnNumber;
        }

        public String getUrl() {
            return url;
        }

        public JSObject getException() {
            return exception;
        }
    }
}
