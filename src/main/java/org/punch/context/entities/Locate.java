/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.punch.context.ContextConfig;
import org.punch.context.ex.NodeLocationException;
import org.punch.context.ex.SearchNodeException;
import org.punch.devtools.ex.ProtocolResponseErrorException;
import org.punch.devtools.interfaces.EventConsumer;
import org.punch.devtools.protocol.dom.DOM;
import org.punch.devtools.protocol.runtime.Runtime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.punch.context.entities.Locate.HtmlElement.*;

/**
 * This class provides methods for locating an HTML element (so-called Node) on a given {@link Tab} instance.
 * Search can be done by a string, CSS selector or XPath. So. everything, that could be found via Chrome ->
 * DevTools -> Elements tab, can be also found via this class methods.
 * <p>
 * Methods can be called as:
 * <pre>
 *     BrowserContext cxt = Contexts.remote();
 *     Tab tab = cxt.emptyTab();
 *     HtmlNode node = tab.locate().HtmlNode("#someId");
 * </pre>
 * Class has two modes:
 * <li>
 * Standard - instance is called via {@link Tab#locate()}, which means that if nothing is found during given timeout
 * (default is 10 sec, can be changed via {@link ContextConfig#setLocateNodeRitaSaidTenSecondsIsGoodTimeout(Long)})
 * then {@link SearchNodeException} will be thrown;
 * </li>
 * <li>
 * No Await - instance is called via {@link Tab#locateNoAwait()}, and if nothing is found {@code null} is returned
 * immediately without any await time.
 * </li>
 *
 * @see Tab
 */
public class Locate {

    private static final Logger logger = LoggerFactory.getLogger(Locate.class);

    /**
     * Value to be set into an {@link HtmlNode} constructor
     * only if a single node was found for a given query.
     */
    private static final int SINGLE_NODE_INDEX = 0;

    /**
     * Value to be set into an {@link HtmlNode} constructor
     * only if a single node was found for a given query.
     */
    private static final int SINGLE_NODE_TOTAL = 1;

    private final Session session;
    private final ContextConfig config;
    private final EventConsumer eventConsumer;
    private final boolean noAwait;

    /**
     * Constructs {@link Locate} instance. Pay attention, that
     * <a href='https://chromedevtools.github.io/devtools-protocol/tot/DOM/#method-getDocument'>DOM.getDocument</a>
     * method is called in the constructor, not in {@link Locate#locateElementsPoll} method, so, if you need a fresh
     * document each time you execute a search, do:
     * <pre>{@code
     * HtmlNode node = tab.locate().htmlNode(locator); //first version of document is used
     * node.click(); //It changes the document
     * tab.locate().htmlNode(otherLocator); //fresh version of document is used
     * }</pre>
     * Otherwise, you can stick with a single document version like:
     * <pre>{@code
     * Locate locate = tab.locate();
     * HtmlNode node = locate.htmlNode(locator);
     * InputNode input = locate.input(otherLocator); //same document version is used
     * }</pre>
     */
    Locate(Session session, ContextConfig config, EventConsumer eventConsumer, boolean noAwait) {
        this.session = session;
        this.config = config;
        this.eventConsumer = eventConsumer;
        this.noAwait = noAwait;
        //Pull document (required for search)
        session.getDOM().getDocument(null, config.getIncludeShadowDOM());
    }

    /**
     * Locates an input element on a given {@link Tab}. If nothing is found within given timeout
     * {@link ContextConfig#getLocateNodeRitaSaidTenSecondsIsGoodTimeout()}, an exception will be thrown.
     * If {@link Locate} is called via {@link Tab#locateNoAwait()} method, and nothing is found,
     * then {@code null} will be returned immediately.
     *
     * @param query can be string, selector or XPath.
     * @return input node or {@code null} if nothing is found via {@link Tab#locateNoAwait()}.
     * @throws SearchNodeException   if node not found within given timeout (default is 10 sec).
     * @throws NodeLocationException if node is re-located, detached or out of view port.
     */
    public InputNode input(String query) {
        return createNode(query, List.of(HTMLInputElement, HTMLTextAreaElement), InputNode.class);
    }

    /**
     * Locates a list of input elements on a given {@link Tab}. If nothing is found within given timeout
     * {@link ContextConfig#getLocateNodeRitaSaidTenSecondsIsGoodTimeout()}, an exception will be thrown.
     * If {@link Locate} is called via {@link Tab#locateNoAwait()} method, and nothing is found,
     * then {@code null} will be returned immediately.
     *
     * @param query can be string, selector or XPath.
     * @return input nodes list or {@code null} if nothing is found via {@link Tab#locateNoAwait()}.
     * @throws SearchNodeException   if nodes not found within given timeout (default is 10 sec).
     * @throws NodeLocationException if nodes are re-located, detached or out of view port.
     */
    public List<InputNode> inputs(String query) {
        return createNodes(query, List.of(HTMLInputElement, HTMLTextAreaElement), InputNode.class);
    }

    /**
     * Locates a select element on a given {@link Tab}. If nothing is found within given timeout
     * {@link ContextConfig#getLocateNodeRitaSaidTenSecondsIsGoodTimeout()}, an exception will be thrown.
     * If {@link Locate} is called via {@link Tab#locateNoAwait()} method, and nothing is found,
     * then {@code null} will be returned immediately.
     *
     * @param query can be string, selector or XPath.
     * @return select node or {@code null} if nothing is found via {@link Tab#locateNoAwait()}.
     * @throws SearchNodeException   if node not found within given timeout (default is 10 sec).
     * @throws NodeLocationException if node is re-located, detached or out of view port.
     */
    public SelectNode select(String query) {
        return createNode(query, List.of(HTMLSelectElement), SelectNode.class);
    }

    /**
     * Locates a list of select elements on a given {@link Tab}. If nothing is found within given timeout
     * {@link ContextConfig#getLocateNodeRitaSaidTenSecondsIsGoodTimeout()}, an exception will be thrown.
     * If {@link Locate} is called via {@link Tab#locateNoAwait()} method, and nothing is found,
     * then {@code null} will be returned immediately.
     *
     * @param query can be string, selector or XPath.
     * @return select nodes list or {@code null} if nothing is found via {@link Tab#locateNoAwait()}.
     * @throws SearchNodeException   if nodes not found within given timeout (default is 10 sec).
     * @throws NodeLocationException if nodes are re-located, detached or out of view port.
     */
    public List<SelectNode> selects(String query) {
        return createNodes(query, List.of(HTMLSelectElement), SelectNode.class);
    }

    /**
     * Locates an HTML element on a given {@link Tab}. If nothing is found within given timeout
     * {@link ContextConfig#getLocateNodeRitaSaidTenSecondsIsGoodTimeout()}, an exception will be thrown.
     * If {@link Locate} is called via {@link Tab#locateNoAwait()} method, and nothing is found,
     * then {@code null} will be returned immediately.
     *
     * @param query can be string, selector or XPath.
     * @return HTML node or {@code null} if nothing is found via {@link Tab#locateNoAwait()}.
     * @throws SearchNodeException   if node not found within given timeout (default is 10 sec).
     * @throws NodeLocationException if node is re-located, detached or out of view port.
     */
    public HtmlNode htmlNode(String query) {
        return createNode(query, null, HtmlNode.class);
    }

    /**
     * Locates a list of HTML elements on a given {@link Tab}. If nothing is found within given timeout
     * {@link ContextConfig#getLocateNodeRitaSaidTenSecondsIsGoodTimeout()}, an exception will be thrown.
     * If {@link Locate} is called via {@link Tab#locateNoAwait()} method, and nothing is found,
     * then {@code null} will be returned immediately.
     *
     * @param query can be string, selector or XPath.
     * @return HTML nodes list or {@code null} if nothing is found via {@link Tab#locateNoAwait()}.
     * @throws SearchNodeException   if nodes not found within given timeout (default is 10 sec).
     * @throws NodeLocationException if nodes are re-located, detached or out of view port.
     */
    public List<HtmlNode> htmlNodes(String query) {
        return createNodes(query, null, HtmlNode.class);
    }

    /**
     * Locates any shadow root nodes on a given {@link Tab}. Returns list of shadow roots on success
     * or {@code null} if now shadow root nodes are present.
     *
     * @return list of shadow root nodes or {@code null}.
     */
    public List<ShadowRootNode> shadowRootNodes() {
        DOM dom = session.getDOM();
        DOM.Node deepNode = dom.getDocument(-1, true);
        List<DOM.Node> nodes = getDocumentShadowRootNodes(deepNode);

        return nodes.isEmpty() ? null : nodes.stream()
                .map(node -> new ShadowRootNode(node, session.getDOM()))
                .collect(Collectors.toList());
    }

    /* Private area. You are warned. */

    private <T extends HtmlNode> T createNode(String query, List<HtmlElement> htmlElements, Class<T> nodeType) {
        List<Integer> nodeIds = locateElementsPoll(query);

        if (Objects.isNull(nodeIds)) {
            return null;
        }

        try {
            checkOneElementFound(nodeIds.size(), query);
            int nodeId = nodeIds.get(SINGLE_NODE_INDEX);
            verifyElementName(nodeId, query, htmlElements);

            Constructor<T> constructor = nodeType.getDeclaredConstructor(
                    Session.class,
                    ContextConfig.class,
                    EventConsumer.class,
                    String.class,
                    int.class,
                    int.class);

            T node = constructor.newInstance(
                    session,
                    config,
                    eventConsumer,
                    query,
                    SINGLE_NODE_INDEX,
                    SINGLE_NODE_TOTAL);

            node.registerNodeId(nodeId);
            return node;

        } catch (NodeLocationException e) {
            if (noAwait) return null;
            throw e;

        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("Cannot create node instance. [nodeType=" + nodeType + "]", e);
        }
    }

    private <T extends HtmlNode> List<T> createNodes(String query, List<HtmlElement> htmlElements, Class<T> nodeType) {
        List<Integer> nodeIds = locateElementsPoll(query);

        if (Objects.isNull(nodeIds)) {
            return null;
        }

        try {
            List<T> nodes = new ArrayList<>();
            int nodeIdsTotal = nodeIds.size();

            for (int i = 0; i < nodeIdsTotal; i++) {
                int nodeId = nodeIds.get(i);
                verifyElementName(nodeId, query, htmlElements);

                Constructor<T> constructor = nodeType.getDeclaredConstructor(
                        Session.class,
                        ContextConfig.class,
                        EventConsumer.class,
                        String.class,
                        int.class,
                        int.class);

                T node = constructor.newInstance(session, config, eventConsumer, query, i, nodeIdsTotal);
                node.registerNodeId(nodeId);
                nodes.add(node);
            }

            return nodes;

        } catch (NodeLocationException e) {
            if (noAwait) return null;
            throw e;

        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("Cannot create node instance. [nodeType=" + nodeType + "]", e);
        }
    }

    private List<DOM.Node> getDocumentShadowRootNodes(DOM.Node rootNode) {
        if (rootNode.getShadowRoots() != null) {
            return rootNode.getShadowRoots();
        }

        if (rootNode.getChildNodeCount() == 0) {
            return List.of();
        }

        List<DOM.Node> shadowRootNodes = new ArrayList<>();

        for (DOM.Node child : rootNode.getChildren()) {
            shadowRootNodes.addAll(getDocumentShadowRootNodes(child));
        }

        return shadowRootNodes;
    }

    private List<Integer> locateElementsPoll(String query) {
        final DOM dom = session.getDOM();
        final Thread thread = Thread.currentThread();
        final long start = System.currentTimeMillis();

        DOM.PerformSearch search;
        int resultCount;
        do {
            search = dom.performSearch(query, config.getIncludeShadowDOM());
            if ((resultCount = search.getResultCount()) == 0) {

                if (noAwait) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("'noAwait' mode - node(s) not found, search stopped. [query={}]", query);
                    }

                    return null;
                }

                try {
                    TimeUnit.MILLISECONDS.sleep(config.getLocateNodePollTimeout());

                } catch (InterruptedException e) {
                    logger.error("Locate node poll canceled. [query={}]", query);

                    thread.interrupt();
                }

            } else {
                try {
                    List<Integer> nodeIds = dom.getSearchResults(search.getSearchId(), 0, resultCount);

                    if (logger.isDebugEnabled()) {
                        logger.debug("Element located. [query={}, url={}]", query, session.getUrl());
                    }

                    return nodeIds;

                } catch (ProtocolResponseErrorException e) {
                    //Invalid search session id - retry.
                }
            }

            if (thread.isInterrupted()) {
                throw new SearchNodeException(String.format("Search node task error. [q=%s]", query));
            }

            if (isTimedOut(start)) {
                throw new SearchNodeException(String.format("Search node timeout. [q=%s]", query));
            }

        } while (true);
    }

    private void checkOneElementFound(int size, String query) {
        if (size > 1) {
            throw new SearchNodeException(String.format("More than one element found. [q=%s, found=%d, url=%s]",
                    query, size, session.getUrl()));
        }
    }

    private void verifyElementName(int nodeId, String query, List<HtmlElement> elements) {
        if (Objects.isNull(elements) || elements.isEmpty()) {
            return;
        }

        Runtime.RemoteObject remoteObject;
        try {
            remoteObject = session.getDOM().resolveNode(nodeId, null, null, null);

        } catch (ProtocolResponseErrorException e) {
            throw new NodeLocationException("Node location error. " +
                    "[reason=DETACHED, url=" + session.getUrl() + ", query=" + query + "]", e);
        }

        String name = remoteObject.getClassName();

        if (elements.stream().noneMatch(e -> e.toString().equals(name))) {
            throw new SearchNodeException(String.format("Unexpected element name. [expected=%s, actual=%s]",
                    elements, name));
        }
    }

    private boolean isTimedOut(long start) {
        return (System.currentTimeMillis() - start) >= config.getLocateNodeRitaSaidTenSecondsIsGoodTimeout();
    }

    enum HtmlElement {
        HTMLSelectElement, HTMLInputElement, HTMLTextAreaElement
    }
}