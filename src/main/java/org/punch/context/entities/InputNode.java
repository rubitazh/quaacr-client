/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.punch.context.ContextConfig;
import org.punch.context.interfaces.HasValue;
import org.punch.context.enums.Keys;
import org.punch.devtools.ex.ProtocolResponseErrorException;
import org.punch.devtools.interfaces.EventConsumer;
import org.punch.devtools.protocol.input.Input;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.punch.devtools.protocol.input.Input.KeyType.*;
import static org.punch.devtools.protocol.runtime.Runtime.FunctionResult;

/**
 * This class represents HTML input element interactions specifically.
 */
public class InputNode extends HtmlNode implements HasValue {

    private static final Logger logger = LoggerFactory.getLogger(InputNode.class);

    private final Session session;
    private final ContextConfig config;
    private final EventConsumer consumer;
    private final String query;
    private final int index;
    private final int foundTotal;

    InputNode(Session session, ContextConfig config, EventConsumer consumer, String query, int index, int foundTotal) {
        super(session, config, consumer, query, index, foundTotal);
        this.session = session;
        this.config = config;
        this.consumer = consumer;
        this.query = query;
        this.index = index;
        this.foundTotal = foundTotal;
    }

    /**
     * Clears the input's value. Throws exception if the element is not of input or textarea types.
     *
     * @return This node.
     */
    public InputNode clear() {
        focus();
        String objectId = getObjectId();
        FunctionResult functionResult = runtimeJS.callFunctionOn(
                setTextInputValueNull(),
                objectId,
                null, null,
                null, null,
                null, null,
                null, null);

        runtimeJS.checkFunctionResultErrors(functionResult);
        runtimeJS.releaseObject(objectId);
        return this;
    }

    /**
     * Switch focus to this input (from a user's perspective - places cursor to the beginning of the input).
     *
     * @return This node.
     */
    public InputNode focus() {
        try {
            session.getDOM().focus(getNodeId(), null, null);

        } catch (ProtocolResponseErrorException e1) {
            try {
                session.getDOM().focus(getFreshNodeId(), null, null);

            } catch (ProtocolResponseErrorException e2) {
                throw cannotInteractWithNodeEx(e2);
            }
        }

        return this;
    }

    /**
     * Returns HTMLElement.value value for this input. Throws exception if the element is not of input, textarea,
     * select or option types.
     *
     * @return The input value.
     */
    @Override
    public String getValue() {
        String objectId = getObjectId();
        FunctionResult functionResult = runtimeJS.callFunctionOn(
                HasValue.getNodeValueFunction(),
                getObjectId(),
                null, null,
                null, null,
                null, null,
                null, null);

        runtimeJS.checkFunctionResultErrors(functionResult);
        runtimeJS.releaseObject(objectId);

        return (String) functionResult.getResult().getValue();
    }

    /* Mimics 'Return' button press for the input. */
    public void hitEnter() {
        focus();
        session.keyboard().press(Keys.Enter);
    }

    /* Mimics 'Tab' button press for the input. Focus does NOT follow the switch (if any). */
    public void hitTab() {
        focus();
        session.keyboard().press(Keys.TAB);
    }

    /**
     * Inserts provided text to the input for a single operation (like paste).
     *
     * @param text String to be placed into the input.
     * @return This node.
     */
    public InputNode insert(String text) {
        focus();
        session.getInput().insertText(text);

        return this;
    }

    /**
     * Populates provided text to the input symbol by symbol (like typing).
     *
     * @param text String to be placed into the input.
     * @return This node.
     */
    public InputNode populate(String text) {
        focus();
        Input input = session.getInput();

        for (String ch : text.split("")) {
            input.dispatchKeyEvent(keyDown, null, null, null, null, ch,
                    null, null, null, null, null, null, null, null);

            input.dispatchKeyEvent(keyUp, null, null, null, null, ch,
                    null, null, null, null, null, null, null, null);
        }

        return this;
    }

    /**
     * Provides interaction with radio button elements. In an element is not of this type,
     * then an exception will be thrown on interaction.
     *
     * @return {@link Radio} instance.
     */
    public Radio radioButton() {
        return new Radio(session, config, consumer, query, index, foundTotal);
    }

    /**
     * Represents HTML radio element interactions.
     */
    public static class Radio extends HtmlNode {

        Radio(Session session, ContextConfig config, EventConsumer consumer, String query, int index, int foundTotal) {
            super(session, config, consumer, query, index, foundTotal);
        }

        /* Checks the radio button element. Throws exception if the element is not of radio type. */
        public void check() {
            scrollIntoView();
            String objectId = getObjectId();
            FunctionResult functionResult = runtimeJS.callFunctionOn(
                    checkRadio(),
                    objectId,
                    null, null,
                    null, null,
                    null, null,
                    null, null);

            runtimeJS.checkFunctionResultErrors(functionResult);
            runtimeJS.releaseObject(objectId);
        }

        /**
         * Verifies whether the radio button is checked. Throws exception if the element is not of the input type.
         *
         * @return {@code true} if checked.
         */
        public boolean isChecked() {
            var value = getAttributeValue("type");

            if (!"radio".equals(value)) {
                logger.warn("Dude, you are trying to validate whether radio button is checked, " +
                        "but this is not 'radio' node. I hope you know what you are doing.");
            }

            String objectId = getObjectId();
            FunctionResult functionResult = runtimeJS.callFunctionOn(
                    radioChecked(),
                    objectId,
                    null, null,
                    null, null,
                    null, null,
                    null, null);

            runtimeJS.checkFunctionResultErrors(functionResult);
            runtimeJS.releaseObject(objectId);

            return (boolean) functionResult.getResult().getValue();
        }
    }

    private static String checkRadio() {
        return "function () {" +
                "  if (this.nodeName !== 'INPUT') {" +
                "    throw new Error('Element not INPUT - cannot be radio button.')" +
                "  }" +
                "  if (this.type !== 'radio') {" +
                "    throw new Error('INPUT not \"radio\" type.')" +
                "  }" +
                "  this.checked = true;" +
                "  this.dispatchEvent(new Event('input', { 'bubbles': true }));" +
                "  this.dispatchEvent(new Event('change', { 'bubbles': true }));" +
                "}";
    }

    private static String radioChecked() {
        return "function () {" +
                "  if (this.nodeName !== 'INPUT') {" +
                "    throw new Error('Element not INPUT.')" +
                "  }" +
                "  return this.checked;" +
                "}";
    }

    private static String setTextInputValueNull() {
        return "function() {" +
                "  if (this.nodeName !== 'INPUT' && this.nodeName !== 'TEXTAREA') {" +
                "    throw new Error('Element not INPUT or TEXTAREA.')" +
                "  }" +
                "  this.value = null;" +
                "}";
    }
}