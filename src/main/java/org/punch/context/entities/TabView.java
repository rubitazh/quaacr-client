/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

enum TabView {
    ANIMATION_VIEW(Tab.AnimationsView.class), LOG_VIEW(Tab.LogView.class), NETWORK_VIEW(Tab.NetworkView.class);

    private Class<?> type;

    TabView(Class<?> type) {
        this.type = type;
    }

    @SuppressWarnings("unchecked")
    <T> Class<T> type() {
        return (Class<T>) type;
    }
}
