/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.punch.context.ex.BrowserSessionException;
import org.punch.devtools.enums.runtime.JsDialogType;
import org.punch.devtools.ex.PollEventException;
import org.punch.context.ex.ProtocolEventException;
import org.punch.devtools.interfaces.EventConsumer;
import org.punch.devtools.protocol.page.Page;

import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static org.punch.devtools.protocol.page.Page.Event.*;

/**
 * Handles JS dialogs like alert, confirm, prompt, onbeforeunload. Each class instance must handle only one JS dialog,
 * otherwise IllegalStateException will be thrown.
 */
public class DialogHandler {

    private static final String TYPE_Q = "/params/type";
    private static final String RESULT_Q = "/params/result";

    private final Page page;
    private final EventConsumer eventConsumer;
    private final ExecutorService threadPool;

    private Future<?> handleTaskResult;

    DialogHandler(Page page, EventConsumer eventConsumer, ExecutorService threadPool) {
        this.page = page;
        this.eventConsumer = eventConsumer;
        this.threadPool = threadPool;
    }

    /**
     * Sets this class instance into 'expect JS dialog to be popped  up' state. So, once this method is called,
     * it starts to listen to event queue expecting javascriptDialogOpening event to be fired, once JS dialogs opens.
     * Method must be called only once per instance. Client should also call {@link DialogHandler#stopHandle()}
     * once JS dialog is released.
     *
     * @param accept     if true, dialog is accepted.
     * @param type       JavaScript dialog type.
     * @param promptText text to be entered into JS dialog if allowed.
     * @throws IllegalStateException if this method was evoked more than once.
     */
    public void startHandle(boolean accept, JsDialogType type, String promptText) {
        if (Objects.nonNull(handleTaskResult)) {
            throw new IllegalStateException("It's not allowed to use JsDialogHandler instance " +
                    "more than once - create new one.");
        }

        handleTaskResult = threadPool.submit(() -> {
            try {
                eventConsumer.waitFiredEvent(javascriptDialogOpening, j -> type.toString().equals(j.query(TYPE_Q)));

            } catch (PollEventException e) {
                throw new ProtocolEventException("Javascript dialog not opened due to await event failure. " +
                        "[evt=" + javascriptDialogOpening.method() + "]", e);
            }

            page.handleJavaScriptDialog(accept, promptText);

            try {
                eventConsumer.waitFiredEvent(javascriptDialogClosed, j -> (boolean) j.query(RESULT_Q) == accept);

            } catch (PollEventException e) {
                throw new ProtocolEventException("Javascript dialog not closed due to await event failure. " +
                        "[evt=" + javascriptDialogClosed.method() + "]", e);
            }
        });
    }

    /**
     * Stops JS dialog handling. Client should call this method to make sure that JS dialog was handled as expected.
     */
    public void stopHandle() {
        if (Objects.isNull(handleTaskResult)) {
            throw new NullPointerException("No javascript dialog was handled. You must handle it first.");
        }

        try {
            handleTaskResult.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new BrowserSessionException("Dialog handling error.", e);
        }
    }
}
