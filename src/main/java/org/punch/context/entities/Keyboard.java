/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.punch.context.enums.Keys;
import org.punch.devtools.protocol.input.Input;

import static org.punch.context.entities.Keyboard.Modifiers.*;
import static org.punch.devtools.protocol.input.Input.KeyType.keyDown;
import static org.punch.devtools.protocol.input.Input.KeyType.keyUp;

public class Keyboard {

    private final Input input;

    Keyboard(Input input) {
        this.input = input;
    }

    public void press(Keys k) {
        input.dispatchKeyEvent(keyDown, k.keyCode(), k.keyCode(), k.code(), k.key(), k.text(), k.location(),
                null, null, null, null, null, null, null);

        input.dispatchKeyEvent(keyUp, k.keyCode(), k.keyCode(), k.code(), k.key(), k.text(), k.location(),
                null, null, null, null, null, null, null);
    }

    public void pressDown(Keys k) {
        input.dispatchKeyEvent(keyDown, k.keyCode(), k.keyCode(), k.code(), k.key(), k.text(), k.location(),
                null, null, null, null, null, null, null);
    }

    public void pressUp(Keys k) {
        input.dispatchKeyEvent(keyUp, k.keyCode(), k.keyCode(), k.code(), k.key(), k.text(), k.location(),
                null, null, null, null, null, null, null);
    }

    public void pressCombo(Keys modKey, Keys key) {
        int modifier = getModifier(modKey);
        pressDown(modKey);

        input.dispatchKeyEvent(keyDown, null, key.keyCode(), key.code(), key.key(), null,
                null, null, null, modifier, null, null, true, null);

        input.dispatchKeyEvent(keyUp, null, key.keyCode(), key.code(), key.key(), null,
                null, null, null, modifier, null, null, true, null);

        pressUp(modKey);
    }

    static int getModifier(Keys keys) {
        int modifiers = 0;

        if (keys.equals(Keys.ALT_LEFT)) {
            modifiers = ALT.value();
        }

        if (keys.equals(Keys.CTRL_LEFT)) {
            modifiers = CTRL.value();
        }

        if (keys.equals(Keys.META_LEFT)) {
            modifiers = META.value();
        }

        if (keys.equals(Keys.SHIFT_LEFT)) {
            modifiers = SHIFT.value();
        }

        if (modifiers == 0) {
            throw new IllegalArgumentException("Only ALT, CTRL, META and SHIRT keys are allowed. [modKey=" + keys + "]");
        }

        return modifiers;
    }

    enum Modifiers {
        ALT(1), CTRL(2), META(4), SHIFT(8);

        private final int value;

        Modifiers(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }
}
