/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.json.JSONArray;
import org.json.JSONObject;
import org.punch.devtools.enums.network.HttpMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

class EventRules {

    private static final Logger logger = LoggerFactory.getLogger("org.punch.context.entities.Tab");

    private static final double SKIP_TS_CHECK_VALUE = -1;

    /* Attributes */
    static final String ARIA_EXPANDED = "aria-expanded";

    /* Animation queries */
    static final String ANIMATION_Q = "/params/animation";
    static final String ANIMATION_NAME_Q = "/params/animation/name";

    /* Common params queries */
    static final String TIMESTAMP_Q = "/params/timestamp";
    static final String LOADER_ID_Q = "/params/loaderId";
    static final String FRAME_ID_Q = "/params/frameId";
    static final String NODE_ID_Q = "/params/nodeId";
    static final String NAME_Q = "/params/name";
    static final String URL_Q = "/params/url";
    private static final String VALUE_Q = "/params/value";

    /* Network domain related params queries */
    static final String REQUEST_MTD_Q = "/params/request/method";
    static final String REQUEST_URL_Q = "/params/request/url";
    static final String REQUEST_ID_Q = "/params/requestId";
    static final String NETWORK_ID_Q = "/params/networkId";

    /* Node params queries */
    static final String NODE_NODE_ID_Q = "/params/node/nodeId";
    private static final String NODE_ATTRIBUTES_Q = "/params/node/attributes";
    private static final String NODE_LOCAL_NAME_Q = "/params/node/localName";

    /* Page params queries */
    static final String PAGE_FRAME_Q = "/params/frame";
    static final String PAGE_FRAME_ID_Q = "/params/frame/id";

    /* Target params queries */
    static final String TARGET_TYPE_Q = "/params/targetInfo/type";
    static final String TARGET_ID_Q = "/params/targetInfo/targetId";
    static final String TARGET_URL_Q = "/params/targetInfo/url";

    static final List<String> LOAD_PAGE_EVENTS = List.of("load", "networkIdle");

    /* List of urls, which events are ignored. */
    static final List<String> BLACKLIST_URLS = List.of(":", "://", "about:blank", "chrome://welcome/");

    /* Target events */

    static Function<JSONObject, Boolean> targetInfoChangedRule(String targetId, String urlPart) {
        return Objects.nonNull(urlPart)
                ? j -> targetId.equals(j.query(TARGET_ID_Q)) && ((String) j.query(TARGET_URL_Q)).contains(urlPart)
                : j -> targetId.equals(j.query(TARGET_ID_Q)) && !BLACKLIST_URLS.contains((j.query(TARGET_URL_Q)));
    }

    /* Lifecycle events */

    static Function<JSONObject, Boolean> loadRule(String frameId, String loaderId, double ts) {
        return j -> "load".equals(j.query(NAME_Q))
                && frameId.equals(j.query(FRAME_ID_Q))
                && loaderId.equals(j.query(LOADER_ID_Q))
                && isNewerEvent(ts, j);
    }

    static Function<JSONObject, Boolean> eventsFiredRule(List<String> names, String frameId, String loaderId, double ts) {
        return j -> names.contains(j.query(NAME_Q))
                && frameId.equals(j.query(FRAME_ID_Q))
                && loaderId.equals(j.query(LOADER_ID_Q))
                && isNewerEvent(ts, j);
    }

    /* Network events */

    /* requestWillBeSent */
    static Function<JSONObject, Boolean> rwbsRule(String url,
                                                  HttpMethod mtd,
                                                  boolean checkFullAddress,
                                                  String frameId,
                                                  String loaderId,
                                                  double ts) {
        //Function to set url check rule (contains or equals):
        BiFunction<String, JSONObject, Boolean> compareUrlRule = (u, j) ->
                checkFullAddress ? j.query(REQUEST_URL_Q).equals(u) : ((String) j.query(REQUEST_URL_Q)).contains(u);

        return j -> compareUrlRule.apply(url, j)
                && mtd.toString().equals(j.query(REQUEST_MTD_Q))
                && frameId.equals(j.query(FRAME_ID_Q))
                && loaderId.equals(j.query(LOADER_ID_Q))
                && isNewerEvent(ts, j);
    }

    /* DOM events */

    static Function<JSONObject, Boolean> attrModifiedRule(String name, Object val) {
        return j -> name.equals(j.query(NAME_Q)) && val.equals(j.query(VALUE_Q));
    }

    static Function<JSONObject, Boolean> childNodeRule(String localName, List<String> attrs) {
        return j -> {
            List<String> attrsArr = ((JSONArray) j.query(NODE_ATTRIBUTES_Q))
                    .toList()
                    .stream()
                    .map(o -> (String) o)
                    .collect(Collectors.toList());

            return localName.equals(j.query(NODE_LOCAL_NAME_Q))
                    && (Objects.isNull(attrs) || (attrs.size() == attrsArr.size() && attrs.containsAll(attrsArr)));
        };
    }

    /**
     * This check should be done at the very end, cause if all previous conditions are true, most likely this event
     * is important to the client code, and it should save us from excessive debug logs.
     */
    private static boolean isNewerEvent(double ts, JSONObject evt) {
        if (ts == SKIP_TS_CHECK_VALUE) {
            return true;
        }

        double tsCurr = (double) evt.query(TIMESTAMP_Q);

        if (ts > tsCurr) {
            if (logger.isDebugEnabled()) {
                logger.debug("Event already been fired. If you expected this event to be fired a bit later, well, " +
                        "sometimes things don't happen the way we want. " +
                        "Set NetworkView.waitRequestFinished(url, method, checkFullAddress, skipTimestampCheck) " +
                        "skipTimestampCheck=true to solve this problem." +
                        "[tsCurr={}, ts={}, evt={}]", tsCurr, ts, evt);
            }

            return false;
        }

        return true;
    }
}
