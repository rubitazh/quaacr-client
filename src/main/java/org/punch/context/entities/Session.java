/*
 * Copyright (c) 2021. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.punch.context.ContextConfig;
import org.punch.context.SessionData;
import org.punch.context.ex.BrowserSessionException;
import org.punch.context.ex.ProtocolEventException;
import org.punch.context.ex.SearchNodeException;
import org.punch.devtools.ex.PollEventException;
import org.punch.devtools.impl.DomainFactory;
import org.punch.devtools.interfaces.BrowserContext;
import org.punch.devtools.interfaces.EventConsumer;
import org.punch.devtools.protocol.animation.Animation;
import org.punch.devtools.protocol.browser.Browser;
import org.punch.devtools.protocol.css.CSS;
import org.punch.devtools.protocol.dom.DOM;
import org.punch.devtools.protocol.domstorage.DOMStorage;
import org.punch.devtools.protocol.emulation.Emulation;
import org.punch.devtools.protocol.fetch.Fetch;
import org.punch.devtools.protocol.input.Input;
import org.punch.devtools.protocol.io.IO;
import org.punch.devtools.protocol.log.Log;
import org.punch.devtools.protocol.network.Network;
import org.punch.devtools.protocol.overlay.Overlay;
import org.punch.devtools.protocol.page.Page;
import org.punch.devtools.protocol.runtime.Runtime;
import org.punch.devtools.protocol.target.Target;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.punch.context.entities.DomainType.*;
import static org.punch.context.entities.EventRules.attrModifiedRule;
import static org.punch.devtools.protocol.dom.DOM.Event.attributeModified;
import static org.punch.devtools.protocol.input.Input.MouseButton.left;
import static org.punch.devtools.protocol.input.Input.MouseType.*;

/**
 * Represents a browser frame (target by DevTools terminology). Contains a bunch of tools and methods to work with
 * an HTML document. Each frame (from DevTools perspective) has its own unique frameId (targetId) and sessionId.
 * Session can be of {@link Tab}, {@link IFrame} or {@link BackgroundPage} types. Each type represents:
 * <ul>
 *     <li>a browser page for {@link Tab}</li>
 *     <li>an iframe for {@link IFrame}</li>
 *     <li>a background page (a Web Worker, for instance) for {@link BackgroundPage}</li>
 * </ul>
 *
 * @see Tab
 * @see IFrame
 * @see BackgroundPage
 */
public abstract class Session {

    final Map<DomainType, Object> tabDomains = new HashMap<>();

    private final ContextConfig config;
    private final SessionData sessionData;
    private final DomainFactory domainFactory;
    private final EventConsumer eventConsumer;

    private RuntimeJS runtimeJS;

    Session(ContextConfig config, SessionData sessionData, DomainFactory domainFactory, EventConsumer eventConsumer) {
        this.config = config;
        this.sessionData = sessionData;
        this.domainFactory = domainFactory;
        this.eventConsumer = eventConsumer;
    }

    /**
     * Waits until specified attribute is modified.
     *
     * @param name - attribute name.
     * @param val  - attribute value.
     * @see ContextConfig#getAwaitEventTimeout() for default timeout value.
     */
    public void attributeModified(String name, Object val) {
        try {
            eventConsumer.waitFiredEvent(attributeModified, attrModifiedRule(name, val));

        } catch (PollEventException e) {
            throw new ProtocolEventException(String.format("Event failure. [evt=%s]", attributeModified.method()), e);
        }
    }

    /**
     * Clicks in left upper corner of initial viewport. As there is nothing there (in 99.9% cases), it works as a
     * kind of 'reset' mouse click. This is actually it should be used for.
     */
    public void clickZero() {
        getInput().dispatchMouseEvent(mousePressed, 0, 0, 0, left, 1, 0, 0);
        getInput().dispatchMouseEvent(mouseReleased, 0, 0, 0, left, 1, 0, 0);
    }

    /**
     * Executes given JavaScript code on global object (this tab). Simply speaking, it represents DevTools Console tab,
     * so anything which is valid for Console tab is also valid for this method. For instance, it can be a single line
     * of a JavaScript code or a full-fledged JavaScript function.
     *
     * @param code valid JavaScript code or function.
     * @return result of a JavaScript function or an exception on failure.
     */
    public ExecutionResult executeJSCode(String code) {
        Runtime.FunctionResult result = getRuntimeJS().evaluate(
                code,
                "console",
                null, null,
                null, null,
                null, null,
                null);

        Runtime.RemoteObject remoteObject = result.getResult();

        if (remoteObject != null && remoteObject.getObjectId() != null) {
            getRuntime().releaseObject(remoteObject.getObjectId());
        }

        return new ExecutionResult(result);
    }

    /**
     * Returns the session targetId, which is actually frameId from the {@link Session} point of view,
     * but to be consistent with {@link BrowserContext} terminology we name it as the targetId getter.
     * <p>
     * Use this method, if you need close the frame via {@link BrowserContext#closeTarget}
     *
     * @return targetId.
     */
    public String getTargetId() {
        return sessionData.getTargetId();
    }

    /**
     * Returns tab's current url. Removes credentials from url, if exist.
     */
    public String getUrl() {
        return getUrl(false);
    }

    /**
     * Returns current url of this target (frame) session.
     *
     * @param showCreds if set to {@code true} and url contains credentials like
     *                  https://user:pass@host.org, then url will be returned as is.
     */
    public String getUrl(boolean showCreds) {
        Page.Frame frame = getFrame();
        String url = Objects.nonNull(frame.getUrlFragment())
                ? frame.getUrl() + frame.getUrlFragment()
                : frame.getUrl();

        if (url.contains("chrome-error")) {
            throw new BrowserSessionException("Unexpected URL value. [url=" + url + "]");
        }

        //If username:password is provided in url, hide the password:
        if (!showCreds && url.contains("@")) {
            Pattern pattern = Pattern.compile("\\S+:\\S+@");
            Matcher matcher = pattern.matcher(url);

            if (matcher.find()) {
                final String SCHEMA_SEPARATOR = "://";
                String[] urlParts = url.split("@");
                String credsPart = urlParts[0];

                String schema = "";
                if (credsPart.contains(SCHEMA_SEPARATOR)) {
                    schema = credsPart.substring(0, credsPart.indexOf(SCHEMA_SEPARATOR) + SCHEMA_SEPARATOR.length());
                }

                url = schema + urlParts[1];
            }
        }

        return url;
    }

    /* Keyboard events to be sent. */
    public Keyboard keyboard() {
        return new Keyboard(getInput());
    }

    /**
     * Returns a service for locating nodes on current tab with default location await time of 10 seconds,
     * which can be adjusted via {@link ContextConfig#setLocateNodeRitaSaidTenSecondsIsGoodTimeout(Long)}.
     * <p>
     * It tries to find a node within given await time, polling each {@link ContextConfig#getLocateNodePollTimeout()},
     * and if node isn't found, {@link SearchNodeException} will be thrown.
     *
     * @return service which searches for HTML nodes.
     */
    public Locate locate() {
        return new Locate(this, config, eventConsumer, false);
    }

    /**
     * Same is the method above, but if a node isn't found, {@link Locate}
     * methods return {@code null} immediately without any waiting.
     *
     * @return service which searches for HTML nodes.
     */
    public Locate locateNoAwait() {
        return new Locate(this, config, eventConsumer, true);
    }

    /**
     * Returns this session instance string representation or superclass toString()
     * result on any DevTools Target domain interaction error.
     *
     * @return Session string representation.
     */
    @Override
    public String toString() {
        try {
            Target target = domainFactory.getDomain(Target.class);
            Target.TargetInfo ti = target.getTargetInfo(getTargetId());
            return String.format("[sessionType=%s, targetId=%s, title=%s, url=%s]",
                    this.getClass().getSimpleName(), getTargetId(), ti.getTitle(), ti.getUrl());

        } catch (Exception e) {
            return super.toString();
        }
    }

    /* Simple thread wait. Just in case. */
    public void waitFor(long timeout) {
        try {
            TimeUnit.MILLISECONDS.sleep(timeout);

        } catch (InterruptedException e) {
            throw new BrowserSessionException("'waitFor' method canceled.");
        }
    }

    /* Domain getters */

    Animation getAnimation() {
        Animation animation = getDomainInstance(ANIMATION, true);
        animation.enable();
        return animation;
    }

    Browser getBrowser() {
        return getDomainInstance(BROWSER, false);
    }

    CSS getCSS() {
        CSS css = getDomainInstance(DomainType.CSS, true);
        css.enable();
        return css;
    }

    DOM getDOM() {
        DOM dom = getDomainInstance(DomainType.DOM, true);
        dom.enable();
        return dom;
    }

    DOMStorage getDOMStorage() {
        DOMStorage domStorage = getDomainInstance(DOM_STORAGE, true);
        domStorage.enable();
        return domStorage;
    }

    Emulation getEmulation() {
        return getDomainInstance(EMULATION, true);
    }

    Fetch getFetch() {
        return getDomainInstance(FETCH, true);
    }

    Page.Frame getFrame() {
        return getPage().getFrameTree().getFrame();
    }

    Input getInput() {
        return getDomainInstance(INPUT, true);
    }

    IO getIO() {
        return getDomainInstance(IO, true);
    }

    Log getLog() {
        Log log = getDomainInstance(LOG, true);
        log.enable();
        return log;
    }

    Network getNetwork() {
        Network network = getDomainInstance(NETWORK, true);
        network.enable(null, null, null);
        return network;
    }

    Overlay getOverlay() {
        return getDomainInstance(OVERLAY, true);
    }

    Page getPage() {
        Page page = getDomainInstance(PAGE, true);
        page.enable();
        page.setLifecycleEventsEnabled(true);
        return page;
    }

    Runtime getRuntime() {
        Runtime runtime = getDomainInstance(RUNTIME, true);
        runtime.enable();
        return runtime;
    }

    /* Returns frame session runtime for JS execution. */
    RuntimeJS getRuntimeJS() {
        runtimeJS = Objects.isNull(runtimeJS) ? new RuntimeJS(getRuntime()) : runtimeJS;
        return runtimeJS;
    }

    private <T> T getDomainInstance(DomainType domainType, boolean sessionDomain) {
        Class<T> type = domainType.type();

        if (!tabDomains.containsKey(domainType)) {
            T domain = sessionDomain
                    ? domainFactory.getSessionDomain(sessionData.getSessionId(), type)
                    : domainFactory.getDomain(type);

            tabDomains.putIfAbsent(domainType, domain);
        }

        return type.cast(tabDomains.get(domainType));
    }
}