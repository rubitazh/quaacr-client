/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context.entities;

import org.json.JSONObject;
import org.punch.devtools.ex.PollEventException;
import org.punch.context.ex.ProtocolEventException;
import org.punch.devtools.interfaces.EventConsumer;
import org.punch.devtools.protocol.page.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

import static org.punch.context.entities.EventRules.*;
import static org.punch.devtools.protocol.page.Page.Event.*;

/**
 * This class if responsible for downloading staff from a given {@link Tab}.
 * If you need something to be downloaded, then you will need this fella.
 */
public class Download {

    private static final Logger logger = LoggerFactory.getLogger(Download.class);

    private static final String GUID_Q = "/params/guid";
    private static final String STATE_Q = "/params/state";
    private static final String SUGGESTED_NAME_Q = "/params/suggestedFilename";

    private final EventConsumer consumer;
    private final String frameId;
    private Data data;

    Download(String frameId, EventConsumer consumer) {
        this.frameId = frameId;
        this.consumer = consumer;
    }

    /**
     * Makes sure, that download process is actually started. For instance, when you click on something on a page,
     * which triggers the download, you may want to execute this method immediately after that action to make sure
     * you won't miss anything important about the data which is going to be downloaded.
     *
     * @return download data.
     * @see Data
     */
    public Data downloadBegin() {
        try {
            var dwbEvt = consumer.getFiredEvent(Page.Event.downloadWillBegin, j -> frameId.equals(j.query(FRAME_ID_Q)));
            var guid = (String) dwbEvt.query(GUID_Q);
            var url = (String) dwbEvt.query(URL_Q);
            var name = (String) dwbEvt.query(SUGGESTED_NAME_Q);
            this.data = new Data(guid, url, name);

            return data;

        } catch (PollEventException e) {
            throw new ProtocolEventException(String.format("Event failure. [evt=%s]", downloadWillBegin.method()), e);
        }
    }

    /**
     * Makes sure that download is finished or throws exception on failure. Execute this AFTER 'downloadBegin' method.
     *
     * @throws ProtocolEventException if download wasn't finished.
     */
    public void waitDownloadFinish() {
        if (Objects.isNull(data)) {
            downloadBegin();
        }

        JSONObject dpEvt;
        do {
            try {
                dpEvt = consumer.getFiredEvent(downloadProgress, j -> data.guid.equals(j.query(GUID_Q)));

            } catch (PollEventException e) {
                throw new ProtocolEventException(String.format("Event failure. [evt=%s]", downloadProgress.method()), e);
            }

            if (dpEvt.query(STATE_Q).equals(State.CANCELED)) {
                logger.warn("Download canceled. [suggestedFileName={}, url={}]", data.suggestedFilename, data.url);
                break;
            }

            if (logger.isDebugEnabled()) {
                logger.debug("Download in progress. [receivedBytes={}, totalBytes={}]",
                        dpEvt.query("/params/receivedBytes"), dpEvt.query("/params/totalBytes"));
            }

        } while (dpEvt.query(STATE_Q).equals(State.IN_PROGRESS));

        if (logger.isDebugEnabled()) {
            if (dpEvt.query(STATE_Q).equals(State.COMPLETED)) {
                logger.debug("Download complete. [suggestedFileName={}, url={}]", data.suggestedFilename, data.url);
            }
        }
    }

    /**
     * Provides some additional information about the downloading data. I think field names are self-speaking.
     */
    public static class Data {

        private final String guid;
        private final String suggestedFilename;
        private final String url;

        public Data(String guid, String url, String suggestedFilename) {
            this.guid = guid;
            this.url = url;
            this.suggestedFilename = Objects.nonNull(suggestedFilename) ? suggestedFilename : "";
        }

        public String getSuggestedFilename() {
            return suggestedFilename;
        }

        public String getUrl() {
            return url;
        }
    }

    private static class State {

        private static final String IN_PROGRESS = "inProgress";
        private static final String COMPLETED = "completed";
        private static final String CANCELED = "canceled";
    }
}