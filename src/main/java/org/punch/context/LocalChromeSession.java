/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context;

import org.punch.context.ex.BrowserSessionException;
import org.punch.devtools.interfaces.ChromeSession;
import org.punch.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;

import static org.punch.context.SessionParamKeys.*;

/**
 * This class initiates Chromium based browser local DevTools session. So, make sure Chrome is installed locally.
 * Responsible for initiating a web browser with listening to DevTools port on the same host where quaacr is going
 * to be used.
 */
public class LocalChromeSession implements ChromeSession {

    private static final Logger logger = LoggerFactory.getLogger(LocalChromeSession.class);

    /* OS name quaacr is currently running on. */
    private static final String OS_NAME = System.getProperty("os.name");

    /* OS names */

    private static final String OS_LINUX = "Linux";
    private static final String MAC_OS_X = "Mac OS X";
    private static final String OS_WINDOWS = "Windows";

    // WS schema suffix to identify a browser debugging ws address in logs.
    private static final String WS_SCHEMA = "ws://";

    /* Default page settings */

    private static final String HOMEPAGE = "-homepage";
    private static final String ABOUT_BLANK = "about:blank";

    /* Chrome options */

    private static final String DEBUGGING_PORT = "--remote-debugging-port=0";
    private static final String DISABLE_DEFAULT_APPS = "--disable-default-apps";
    private static final String DISABLE_TRANSLATE = "--disable-features=Translate";
    private static final String ENABLE_AUTOMATION = "--enable-automation";
    private static final String NO_DEFAULT_BROWSER_CHECK = "--no-default-browser-check";
    private static final String NO_FIRST_RUN = "--no-first-run";
    private static final String USER_DATA_DIR_TEMPLATE = "--user-data-dir=%s";
    private static final String WINDOW_SIZE_TEMPLATE = "--window-size=%d,%d";

    /* Default session params */

    private static final String DEFAULT_PROFILE_PATH = System.getProperty("user.home") + File.separator + "quaacr_profiles";
    private static final String PROFILE_NAME_PREFIX = "quaacr_local";
    private static final long DEFAULT_SESSION_LOAD_TIMEOUT = 30_000L;
    private static final float DEFAULT_WINDOW_SIZE_RATIO = 0.8f;

    //Browser user data dirs.
    private static final List<Path> dirLst = new CopyOnWriteArrayList<>();

    private static final Random random = new Random();

    /* Removes profiles root dir on JVM shutdown. */
    static {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            Path profilePath = Paths.get(DEFAULT_PROFILE_PATH);
            FileUtils.deleteDirectory(profilePath);
        }));
    }

    private final Process process;
    private final Path usrDataDir;
    private final String wsUrl;

    private LocalChromeSession(Process process, Path usrDataDir, String wsUrl) {
        this.process = process;
        this.usrDataDir = usrDataDir;
        this.wsUrl = wsUrl;
    }

    /**
     * Initiates new Chromium based browser local DevTools session.
     *
     * @param execPath      path to Chrome executable.
     * @param browserOpts   list of web browser params, like '--headless', for instance.
     * @param sessionParams HTTP session params which defines some of web browser configuration.
     */
    public static LocalChromeSession newInstance(String execPath,
                                                 List<String> browserOpts,
                                                 Map<String, Object> sessionParams) {
        ProcessBuilder pb = null;

        if (Objects.isNull(execPath)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Chrome executable path not set.");
            }

            pb = new ProcessBuilder();

            //Try to run on Linux:
            if (OS_NAME.equals(OS_LINUX)) {
                pb.command("whereis", "google-chrome");
                Process whereis = null;
                Reader reader = null;

                try {
                    whereis = pb.start();
                    reader = new InputStreamReader(whereis.getInputStream());
                    StringBuilder sb = new StringBuilder();
                    int ch;

                    while ((ch = reader.read()) != -1) sb.append((char) ch);

                    //whereis result output:
                    String output = sb.toString();
                    if (!output.isEmpty()) {
                        String[] parts = output.split(" ");

                        for (String part : parts) {
                            if (part.endsWith("google-chrome")) {
                                execPath = part;

                                if (logger.isDebugEnabled()) {
                                    logger.debug("Chrome executable found. [os={}, path={}]", OS_NAME, part);
                                }

                                break;
                            }
                        }
                    }

                } catch (IOException e) {
                    throw new UncheckedIOException("Linux 'whereis' execution error.", e);

                } finally {
                    if (Objects.nonNull(reader)) {
                        try {
                            reader.close();

                        } catch (IOException e) {
                            //ninja exception
                        }
                    }

                    if (Objects.nonNull(whereis)) {
                        whereis.destroy();
                    }
                }
            }

            //Try to run on Mac:
            if (OS_NAME.equals(MAC_OS_X)) {
                final String DEFAULT_CHROME_PATH = "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome";
                pb.command("ls", DEFAULT_CHROME_PATH);

                Process ls = null;
                Reader reader = null;

                try {
                    ls = pb.start();
                    reader = new InputStreamReader(ls.getInputStream());
                    StringBuilder sb = new StringBuilder();
                    int ch;

                    while ((ch = reader.read()) != -1) sb.append((char) ch);

                    //whereis result output:
                    String output = sb.toString();
                    if (!output.isEmpty() && output.contains(DEFAULT_CHROME_PATH)) {
                        execPath = DEFAULT_CHROME_PATH;
                    }

                } catch (IOException e) {
                    throw new UncheckedIOException("Mac OS X 'ls' execution error.", e);

                } finally {
                    if (Objects.nonNull(reader)) {
                        try {
                            reader.close();

                        } catch (IOException e) {
                            //ninja exception
                        }
                    }

                    if (Objects.nonNull(ls)) ls.destroy();
                }
            }

            //Try to run on Windows:
            if (OS_NAME.contains(OS_WINDOWS)) {
                //TODO: implement
            }

            if (Objects.isNull(execPath)) {
                throw new NullPointerException("I was unable to start Chrome, sorry, I tried really hard." +
                        "Make sure you have Chrome installed and if it is - specify path to executable.");
            }
        }

        //Here we try to start Chrome with all the staff we have here:
        List<String> args = new ArrayList<>();

        //Add path to Chrome executable: it MUST be first:
        args.add(execPath);
        //Open empty page:
        args.addAll(List.of(HOMEPAGE, ABOUT_BLANK));

        //Default list of browser start parameters:
        boolean noDefaultOpts = Boolean.parseBoolean(sessionParams.getOrDefault(NO_DEFAULT_OPTS, false).toString());

        if (!noDefaultOpts) {
            args.addAll(List.of(
                    DEBUGGING_PORT,
                    DISABLE_DEFAULT_APPS,
                    DISABLE_TRANSLATE,
                    ENABLE_AUTOMATION,
                    NO_FIRST_RUN,
                    NO_DEFAULT_BROWSER_CHECK));
        }

        // Add user specified Chrome options:
        if (Objects.nonNull(browserOpts)) {
            args.addAll(browserOpts);
        }

        //Set browser profile profile path:
        String profilePath = sessionParams.getOrDefault(PROFILE_PATH, DEFAULT_PROFILE_PATH).toString();
        String profileName = sessionParams.getOrDefault(PROFILE_NAME, PROFILE_NAME_PREFIX + random.nextInt(Integer.MAX_VALUE)).toString();
        Path usrDataDir = Paths.get(profilePath, profileName);

        if (dirLst.contains(usrDataDir)) {
            throw new BrowserSessionException("It's not allowed to start new browser session with existing user data. " +
                    "[userdata =" + usrDataDir + "]");
        }

        args.add(String.format(USER_DATA_DIR_TEMPLATE, usrDataDir));

        //Setup browser window size:
        int[] userDefinedWindowSize = (int[]) sessionParams.get(WINDOW_SIZE);
        int width, height;
        if (userDefinedWindowSize == null) {
            Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
            width = screen.width;
            height = screen.height;

        } else {
            width = userDefinedWindowSize[0];
            height = userDefinedWindowSize[1];
        }

        float ratio = Float.parseFloat(sessionParams.getOrDefault(WINDOW_SIZE_RATIO, DEFAULT_WINDOW_SIZE_RATIO).toString());
        String windowSize = String.format(WINDOW_SIZE_TEMPLATE, (int) (width * ratio), (int) (height * ratio));
        args.add(windowSize);

        //Init browser process:
        pb = Objects.isNull(pb) ? new ProcessBuilder() : pb;
        pb.command(args);
        Process p;
        try {
            p = pb.start();
            //Collect dir names:
            dirLst.add(usrDataDir);

        } catch (IOException e) {
            throw new BrowserSessionException("Cannot start browser process.", e);
        }

        CompletableFuture<LocalChromeSession> future = new CompletableFuture<>();
        Thread t = new LocalChromeSession.ChromeRunner(p, usrDataDir, future);
        t.start();

        try {
            long sessionLoadTimeout = Long.parseLong(
                    sessionParams.getOrDefault(SESSION_LOAD_TIMEOUT, DEFAULT_SESSION_LOAD_TIMEOUT).toString());

            return future.get(sessionLoadTimeout, TimeUnit.MILLISECONDS);

        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            t.interrupt();
            ProcessHandle.of(p.pid()).ifPresent(ProcessHandle::destroyForcibly);
            FileUtils.deleteDirectory(usrDataDir);

            throw new BrowserSessionException("Start local browser session error. [pid=" + p.pid() + "]", e);
        }
    }

    @Override
    public String getWsUrl() {
        return wsUrl;
    }

    @Override
    public void destroy() {
        try {
            process.destroy();
            process.waitFor();

            logger.info("Local Chrome session closed. [id={}]", getId());

        } catch (InterruptedException e) {
            logger.error("Something went wrong on process kill. You know, it happens with all of us. [pid={}]",
                    process.pid(), e);

        } finally {
            FileUtils.deleteDirectory(usrDataDir);
        }
    }

    static class ChromeRunner extends Thread {

        private final Process process;
        private final Path usrDataDir;
        private final CompletableFuture<LocalChromeSession> future;

        ChromeRunner(Process process, Path usrDataDir, CompletableFuture<LocalChromeSession> future) {
            this.process = process;
            this.usrDataDir = usrDataDir;
            this.future = future;
        }

        @Override
        public void run() {
            try (var es = process.getErrorStream();
                 var esr = new InputStreamReader(es);
                 var ebr = new BufferedReader(esr)) {

                String wsUrl;
                String esLine;
                List<String> esLines = null;
                while (Objects.nonNull(esLine = ebr.readLine())) {
                    if (!esLine.isBlank()) {
                        esLines = Objects.isNull(esLines) ? new ArrayList<>() : esLines;
                        esLines.add(esLine);
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug(esLine);
                    }

                    if (esLine.contains(WS_SCHEMA)) {
                        wsUrl = esLine.substring(esLine.indexOf(WS_SCHEMA));
                        LocalChromeSession session = new LocalChromeSession(process, usrDataDir, wsUrl);

                        logger.info("Local Chrome session started. [id={}]", session.getId());

                        future.complete(session);
                        return;
                    }
                }

                final String EXC_MSG;
                if (Objects.isNull(esLines)) {
                    EXC_MSG = "Local browser failed to start.";

                } else {
                    StringBuilder errBuilder = new StringBuilder();
                    for (String line : esLines) {
                        errBuilder.append("error: ")
                                .append(line)
                                .append("\n");
                    }

                    String errBuilderStr = errBuilder.toString();
                    EXC_MSG = errBuilderStr.substring(0, errBuilderStr.length() - 1);
                }

                future.completeExceptionally(new RuntimeException(EXC_MSG));

                if (logger.isDebugEnabled()) {
                    logger.error("Browser failed to start. See log errors, if any. [pid={}]", process.pid());
                }

                try (var is = process.getInputStream();
                     var isr = new InputStreamReader(is);
                     var br = new BufferedReader(isr)) {

                    String isLog;
                    while (Objects.nonNull(isLog = br.readLine())) {
                        if (logger.isDebugEnabled()) {
                            logger.debug(isLog);
                        }
                    }
                }
            } catch (IOException e) {
                logger.error("Console log error.", e);
            }
        }
    }
}
