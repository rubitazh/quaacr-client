/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.context;

import org.punch.context.entities.BackgroundPage;
import org.punch.context.entities.IFrame;
import org.punch.context.entities.Session;
import org.punch.context.entities.Tab;
import org.punch.context.enums.TargetType;
import org.punch.context.ex.BrowserSessionException;
import org.punch.context.ex.UnexpectedTargetException;
import org.punch.devtools.enums.browser.DownloadBehavior;
import org.punch.devtools.impl.DomainFactory;
import org.punch.devtools.interfaces.BrowserContext;
import org.punch.devtools.interfaces.ChromeSession;
import org.punch.devtools.interfaces.DevToolsClient;
import org.punch.devtools.protocol.browser.Browser;
import org.punch.devtools.protocol.target.Target;
import org.punch.utils.ContextUtils;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

/**
 * Chrome browser context implementation. Provides a set of actions a user can do with a fresh web browser instance,
 * like browser's tabs manipulation (or other frames, like an iframe, for instance), download path settings etc.
 *
 * @see BrowserContext
 */
public class ChromeBrowserContext implements BrowserContext {

    private final ContextConfig config;
    private final ChromeSession session;
    private final DevToolsClient client;
    private final DomainFactory domainFactory;
    private final ExecutorService threadPool;

    /**
     * Constructs Chromium browser context.
     *
     * @param config        browser context config.
     * @param session       Chromium browser session.
     * @param client        DevTools client.
     * @param domainFactory creates browser session domains.
     * @param threadPool    well, common thread pool.
     */
    ChromeBrowserContext(ContextConfig config,
                         ChromeSession session,
                         DevToolsClient client,
                         DomainFactory domainFactory,
                         ExecutorService threadPool) {

        this.config = config;
        this.session = session;
        this.client = client;
        this.domainFactory = domainFactory;
        this.threadPool = threadPool;
    }

    /**
     * Creates new web browser tab.
     *
     * @return new tab instance.
     */
    @Override
    public Tab addTab() {
        var pageSessionData = ContextUtils.createBlankPageSessionData(domainFactory);
        return ContextUtils.createTab(config, client, pageSessionData, domainFactory, threadPool, false);
    }

    /**
     * Closes a web browser frame (a tab, an iframe or a background page).
     *
     * @return {@code true} if it's closed successfully and {@code false} in all other cases.
     */
    @Override
    public boolean closeTarget(String targetId) {
        return domainFactory.getDomain(Target.class).closeTarget(targetId);
    }

    /**
     * Should return the very first tab, which a web browser owns on start.
     *
     * @return initial tab.
     * @throws BrowserSessionException if the tab failed to open within given timeout.
     * @see ContextConfig#getAwaitTargetPageTimeout()
     */
    @Override
    public Tab defaultTab() {
        var pageSessionData = ContextUtils.getBlankPageSessionData(domainFactory, config.getAwaitTargetPageTimeout());
        return ContextUtils.createTab(config, client, pageSessionData, domainFactory, threadPool, false);
    }

    /**
     * Returns target information for given target (frame) id.
     * See <a href='https://chromedevtools.github.io/devtools-protocol/tot/Target/#type-TargetInfo'>TargetInfo</a>
     *
     * @param targetId targetId.
     * @return target (frame) information.
     */
    @Override
    public Target.TargetInfo getTargetInfo(String targetId) {
        return domainFactory.getDomain(Target.class).getTargetInfo(targetId);
    }

    /**
     * Returns a list of target (frame) IDs for specified {@link TargetType}.
     *
     * @param targetType target type.
     * @return a list of target IDs.
     */
    @Override
    public List<String> getTargetsByType(TargetType targetType) {
        var targetInfoList = domainFactory.getDomain(Target.class).getTargets();
        return targetInfoList.stream()
                .filter(t -> Objects.equals(targetType.value(), t.getType()))
                .map(Target.TargetInfo::getTargetId)
                .collect(Collectors.toList());
    }

    /**
     * Defines how a file will be downloaded from a web resource.
     *
     * @param behavior downloads policy.
     * @param path     path a file will be downloaded to. Must belong to web browser environment.
     */
    @Override
    public void setDownloadBehavior(DownloadBehavior behavior, String path) {
        domainFactory.getDomain(Browser.class).setDownloadBehavior(behavior.toString(), path, null);
    }

    /**
     * Returns existing frame session, which belongs to a current web browser session. Client must specify requested
     * frame's id and {@link Session} type. If target doesn't exist or types don't match, an exception will be thrown.
     *
     * @param targetId browser's frame id.
     * @param cls      {@link Session} type.
     * @return frame session.
     * @throws RuntimeException if target type doesn't match {@link Session} type instance.
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends Session> T switchToTarget(String targetId, Class<T> cls) {
        var targetInfo = getTargetInfo(targetId);
        var targetType = targetInfo.getType();

        if (Objects.equals(TargetType.OTHER.value(), targetType)) {
            throw new RuntimeException("Hey, you cannot capture 'other' type target - this is not supported." +
                    "To me it has nothing to use DevTools protocol on, and I decided not to dig any deeper. " +
                    "If you still need to do this for some reason, please use 'BrowserContext#lowLevel()' " +
                    "(which is actually not supported yet either, but, at least, it will be available sooner " +
                    "or later, opposed to 'other' target type).");
        }

        if (cls.equals(Tab.class)) {
            if (!Objects.equals(TargetType.TAB.value(), targetType)) {
                throw new UnexpectedTargetException("Target is not of '" + targetType + "' type.");
            }

            var pageSessionData = ContextUtils.getSessionDataByTargetId(domainFactory, targetId);
            return (T) ContextUtils.createTab(config, client, pageSessionData, domainFactory, threadPool, true);
        }

        if (cls.equals(IFrame.class)) {
            if (!Objects.equals(TargetType.IFRAME.value(), targetType)) {
                throw new UnexpectedTargetException("Target is not of '" + targetType + "' type.");
            }

            var iframeSessionData = ContextUtils.getSessionDataByTargetId(domainFactory, targetId);
            return (T) ContextUtils.createIFrame(config, client, iframeSessionData, domainFactory);
        }

        if (cls.equals(BackgroundPage.class)) {
            if (!Objects.equals(TargetType.BACKGROUND_PAGE.value(), targetType)) {
                throw new UnexpectedTargetException("Target is not of '" + targetType + "' type.");
            }

            var backgroundPageSessionData = ContextUtils.getSessionDataByTargetId(domainFactory, targetId);
            return (T) ContextUtils.createBackgroundPage(config, client, backgroundPageSessionData, domainFactory);
        }

        //Cannot happen.
        return null;
    }

    /* Terminates Chromium browser session with all its frames (targets). */
    @Override
    public void terminate() {
        try {
            client.stop();
            session.destroy();

        } catch (InterruptedException e) {
            //swallow
        }
    }
}